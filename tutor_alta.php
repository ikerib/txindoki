<?php

if ($_SERVER['REQUEST_METHOD']=="GET") {
	header('Content-Type: application/json');
	$arr = array('ErrorGA' => "Has enviado get, tiene que ser POST");
	echo json_encode($arr);
	return false;
}
//var_dump($_POST);

$user_name ="";
if(!isset($_POST['user_name']) || ($_POST['user_name']=="")) { 
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_name')); 
	return false;
} else{
	$user_name = $_POST['user_name'];
}

$first_name ="";
if(!isset($_POST['first_name']) || ($_POST['first_name']=="")) { 
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta first_name')); 
	return false;
} else{
	$first_name = $_POST['first_name'];
}

$last_name_1 ="";
if(!isset($_POST['last_name_1']) || ($_POST['last_name_1']=="")) { 
	header('Content-Type: application/json'); 
	echo json_encode(array('ErrorGA'=>'Falta last_name_1')); 
	return false;
} else {
	$last_name_1 =$_POST['last_name_1'];
}

$last_name_2 =""; // PUEDE SER FALSE
if(!isset($_POST['last_name_2'])) { 
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta last_name_2')); 
	return false;
} else {
	$last_name_2 =$_POST['last_name_2'];
}

$email ="";
if(!isset($_POST['user_email']) || ($_POST['user_email']=="")) {  
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_email')); 
	return false;
} else {
	$email = $_POST['user_email'];
}

$lang ="es"; // Por defecto es
if(!isset($_POST['user_lang']) || ($_POST['user_lang']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_lang')); 
	return false;
*/
	$lang="es";
} else {
	$lang = $_POST['user_lang'];
}

$npass ="";
if(!isset($_POST['npass']) || ($_POST['npass']=="")) {  
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta npass')); 
	return false;
} else {
	$npass = $_POST['npass'];
	$cpass = $npass;
}


$card_id =""; // NAN
if(!isset($_POST['card_id'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta card_id')); 
	return false;
*/
	$card_id = "";
} else {
	$card_id = $_POST['card_id'];
}

$telf ="";
if(!isset($_POST['phone'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta phone')); 
	return false;
*/
	$telf="";
} else {
	$telf = $_POST['phone'];
}

$telf2 ="";
if(!isset($_POST['mobile'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta mobile')); 
	return false;
*/
	$telf2="";
} else {
	$telf2 = $_POST['mobile'];
}

$city ="";
if(!isset($_POST['city']) || ($_POST['city']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta city')); 
	return false;
*/
	$city ="";
} else {
	$city = $_POST['city'];
}




require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Tutor.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 

	$obj = new Tutor();
	$obj->loadData("new");

	$existe = $obj->existsUserID($user_name);
	if ($existe == "1") {
		header('Content-Type: application/json');
		$arr = array('ErrorGA' => "El usuario ya existe");
		echo json_encode($arr);
		return false;
	}

    $id = $obj->getID(); //Último insertado

	$datuak['id']='new';
	$datuak['user_id']=$id;
	$datuak['first_name']=$first_name;
	$datuak['last_name_1'] = $last_name_1;
	$datuak["last_name_2"] = $last_name_2;
	$datuak["registration_date"] = date(CFG_FORMATO_FECHA);
	$datuak["user_valido"] = "NO EXISTE";
	
	$datuak["user_name"] =  $user_name;
	$datuak["user_email"] = $email;
	$datuak["user_lang"] = $lang; // 'eu'
	$datuak["npass"]=$npass;
	$datuak["cpass"]=$npass;
/* 	$datuak["level"] = '1'; */
/* 	$datuak["course_particular_id"]='0'; */
	$datuak["card_id"]=$card_id; // NAN
	$datuak["phone"]=$telf;
	$datuak["mobile"]=$telf2;
	$datuak["city"]=$city;
/* 	$datuak["internal_id"]=$internal_id; */
	$datuak['bupload_image']="no";
	$datuak['picture_file_old']="";
	$datuak["company_id"]="1";
	$datuak["remarks"]="";



	$resultado = $obj->init($datuak);
	if ($resultado==""){ //NO hay error.
	    $error = $obj->createNew($datuak);
        $id = $obj->getID(); //Último insertado
        
        $obj->setPasswordUserID($npass,$user_name);
        
        
        header('Content-Type: application/json');
		$arr = array('resultado' => "1");
		echo json_encode($arr);
		return false;		
    } else {
	    header('Content-Type: application/json');
		$arr = array('resultado' => "0");
		echo json_encode($arr);
		return false;		
    }




/*
  // 1-. Buscamos el usuario para obtener su ID y lo guardamos en $usuid
  $sql = "SELECT * FROM ic_student WHERE user_id = ? ";
 
  $usu = $mysqli->query($sql);
  
  if (!$usu) {
    header('HTTP/1.1 204 No Content');

  } else {
    header('HTTP/1.1 200 OK');
  }

   echo "Usurario: $usuario <br />";
   echo "Usurario id: $usuid <br />";
   echo "Curso: $cursoid <br />";
*/

/*
  // Comprobamos que el curso existe
  $sql = "SELECT COUNT(course_id) as kontatu FROM ic_course WHERE course_id = ?";
  $cursoexiste = $app['dbs']['mysql_srmgureak']->fetchAssoc($sql, array($cursoid));
  $cursokont = (int) $cursoexiste['kontatu'];
  if ($cursokont == 0) {
    return new Response("El curso no existe.", 200);
  }


  // Comprobamos que NO este matriculado
  $sql = "SELECT count(course_id) as kontatu FROM ic_student_course WHERE course_id = ? AND student_id = ?";
  $matriculado = $app['dbs']['mysql_srmgureak']->fetchAssoc($sql, array($cursoid, $usuid));
  $kont = (int) $matriculado['kontatu'];
  // echo "<br>----<br>";
  // print_r("Matriculado : $kont");
  // echo "<br>----<br>";

  if ($kont > 0 ) {
    return new Response("Ya matriculado.", 200);
  } else {
    try {
      if ($cursoid){
        $usu = $app['dbs']['mysql_srmgureak']->insert("ic_student_course", array('course_id' => $cursoid, 'student_id' => $usuid));
      }
    } catch (Exception $e) {
      return new Response("Error", 400);
      echo("Caught exception: ". $e->getMessage());
    }

    // 4-. Guardar y response
    return new Response("Matriculado", 201);

  }

*/


?>
<?php
// -----------------------------------------
// CourseSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/AbstractCRMObjectSummary.php');

class CourseSummary extends AbstractCRMObjectSummary
{
	
	
   public function CourseSummary()  
   {
   }	

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();
      
      if (null == $sOrderBy)
         return "ic_course.course_name_en";
		 
      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_course.course_desc"; break;
			default: $sOrderBy = "ic_course.course_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END
   
   
   
   
   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si la petición especificada es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function  load()
   {
    
     $nCompanyID = $_SESSION["company_id"];
	 $sLanguage = $_SESSION["language"];
     
     global $mysqli;
	 global $errorLog;
	  
      try
      {

      $sFrom = " FROM ic_course LEFT JOIN ic_level ON ic_course.level_id = ic_level.level_id AND ic_level.language = '".$_SESSION["language"]."'";
            
      $sWhere = " WHERE ic_course.active_flag = 1 ";
      
      $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "course_name", "ic_course", $this->STRING_TYPE, "", "");

	  // Param description
	  $sWhere.=$this->getWhereClause("description", "course_desc", "ic_course", $this->STRING_TYPE, "", "");

	  // Param course_category_id
	  $sWhere.=$this->getWhereClause("level_id", "level_id", "ic_level", $this->LOGICAL_TYPE, "", "");
	  
	  // param internal_id
	  $sWhere.=$this->getWhereClause("internal_id", "internal_id", "ic_course", $this->STRING_TYPE, "", "");
			
	  
	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;
	  
	  //echo $query_select;
	  
	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }
		 
       $query_select="SELECT ic_course.course_id,
				  COALESCE(ic_course.course_name,'') as course_name,
				  COALESCE(ic_course.course_desc,'') as course_desc,
				  ic_course.level_id,
				  ic_level.level_name";
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
       $query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();
		
       echo $query_select;
	   
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
   }//end load
   
   
   
   
   
   
   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  sType    tipo de filtrado
    * @param  sID      clave de filtrado
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si alguno de los parámetros es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
   public function loadFilter($sType, $sID)
   {
   
		$nCompanyID = $_SESSION["company_id"];
		$sLanguage = $_SESSION["language"];
     
		global $mysqli;
		global $errorLog;
	  
		try
		{

			$sFrom = sprintf(" FROM ic_course LEFT JOIN ic_level ON ic_course.level_id = ic_level.level_id AND ic_level.language='%s'",$sLanguage);
            
			$sWhere = " WHERE ic_course.active_flag = 1 ";
      
			$sOperator = " = ";
			$sCondition = "";

			// Param name
			$sWhere.=$this->getWhereClause("name", "course_name", "ic_course", $this->STRING_TYPE, "", "");

			if ($sType=="GROUP")
			{ 
				$sFrom.=" LEFT JOIN ic_center_course ON ic_course.course_id = ic_center_course.course_id ";
				$sWhere.=" AND ic_center_course.center_id = " . $sID ;
			}else{
				$sWhere.=" AND 1 = 2 " ;
			}
			
			$query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;
	  
			//echo $query_select;
	  
			if ($result = $mysqli->query($query_select)){
				$row = $result->fetch_array();
				$this->init($row[0]);
				$result->close();
			}else{
				$this->init(0);
			}
		 
			$query_select="SELECT ic_course.course_id,
				  COALESCE(ic_course.course_name,'') as course_name,
				  COALESCE(ic_course.course_desc,'') as course_desc,
				  COALESCE(ic_level.level_name,'') as level_name ";
			
			if ($sType=="GROUP")
			{
				//$query_select.=", ic_certification_course.nota_minima";
			}
			
			$query_select.= $sFrom . $sWhere;
			$query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
			$query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();
		
			//echo $query_select;
	   
			$result = $mysqli->query($query_select);
	   
			return $result;
		
		}
		catch (Exception $ex)
		{
        }
   
   
   }
   
   
}

?>
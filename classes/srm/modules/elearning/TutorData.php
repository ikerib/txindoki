<?php
// -----------------------------------------
// TutorData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/AbstractCRMPersonData.php');

class TutorData extends AbstractCRMPersonData 
{
	
	public $isUser;
	public $userID;
	public $userPass;
	public $userLang;
	public $userEmail;
	public $moRegistrationDate = "";
	
	public $oAssignedCourses = "";
	public $oNoAssignedCourses = "";
	
	public $oMoodleCourses = "";
	
	public $msInternalID;
	
	public function getIsUser(){
		return $this->isUser;
	}
	
	public function setIsUser($value){
		$this->isUser = $value;
	}
	
	public function getUserID(){
		return $this->userID;
	}
	
	public function setUserID($value){
		$this->userID = $value;
	}	
	
	public function getUserLang(){
		return $this->userLang;
	}
	
	public function setUserLang($value){
		$this->userLang = $value;
	}	
		
	public function getUserEmail(){
		return $this->userEmail;
	}
	
	public function setUserEmail($value){
		$this->userEmail = $value;
	}	
	
	public function getRegistrationDate(){
		return $this->moRegistrationDate;
	}
	public function setRegistrationDate($value){
		$this->moRegistrationDate = $value;
	}	
	
	public function getAssignedCourses(){
		return $this->oAssignedCourses;
	}
	public function setAssignedCourses($value){
		$this->oAssignedCourses = $value;
	}	

	public function getNoAssignedCourses(){
		return $this->oNoAssignedCourses;
	}
	public function setNoAssignedCourses($value){
		$this->oNoAssignedCourses = $value;
	}
	
	
	public function getMoodleCourses(){
		return $this->oMoodleCourses;
	}
	public function setMoodleCourses($value){
		$this->oMoodleCourses = $value;
	}		
	
	public function getInternalID(){
		return $this->msInternalID;
	}
	public function setInternalID($nValue){
		$this->msInternalID = $nValue;
	}
	
	
			
}

?>
<?php
// -----------------------------------------
// CategoryData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/AbstractCRMObjectImage.php');

class CategoryData extends AbstractCRMObjectImage 
{
	public $msLanguage = "";
	public $msDescription = "";
	public $msShortname = "";
	public $mnOrderAppearence = "";

	public $msPictureFileAC="";
	public $msPictureNameAC="";
	public $mnPictureWidthAC="";
	public $mnPictureHeightAC="";
	public $msUploadAC = "";
	public $msPictureFileDeleteAC = "";
	public $mbPictureFileACExists = "";
	public $mbThumbnailFileACExists = "";
	
	public function getLanguage(){
		return $this->msLanguage;
	}
	public function setLanguage($nValue){
		$this->msLanguage = $nValue;
	}
	
	public function getDescription(){
		return $this->msDescription;
	}
	public function setDescription($nValue){
		$this->msDescription = $nValue;
	}
	
	public function getShortname(){
		return $this->msShortname;
	}
	public function setShortname($nValue){
		$this->msShortname = $nValue;
	}
	
	public function getOrderAppearence(){
		return $this->mnOrderAppearence;
	}
	public function setOrderAppearence($nValue){
		$this->mnOrderAppearence = $nValue;
	}


	public function getPictureFileAC(){
		return $this->msPictureFileAC;
	}
	public function setPictureFileAC($nValue){
		$this->msPictureFileAC = $nValue;
	}
	
	public function getPictureNameAC(){
		return $this->msPictureNameAC;
	}
	public function setPictureNameAC($nValue){
		$this->msPictureNameAC = $nValue;
	}

	public function getPictureWidthAC(){
		return $this->mnPictureWidthAC;
	}
	public function setPictureWidthAC($nValue){
		$this->mnPictureWidthAC = $nValue;
	}
	
	public function getPictureHeightAC(){
		return $this->mnPictureHeightAC;
	}
	public function setPictureHeightAC($nValue){
		$this->mnPictureHeightAC = $nValue;
	}
	
	public function getUploadAC(){
		return $this->msUploadAC;
	}
	public function setUploadAC($nValue){
		$this->msUploadAC = $nValue;
	}
	
	public function getPictureFileDeleteAC(){
		return $this->msPictureFileDeleteAC;
	}
	public function setPictureFileDeleteAC($nValue){
		$this->msPictureFileDeleteAC = $nValue;
	}
	
	public function getPictureFileACExists(){
		return $this->mbPictureFileACExists;
	}
	public function setPictureFileACExists($nValue){
		$this->mbPictureFileACExists = $nValue;
	}
	
	public function getThumbnailFileACExists(){
		return $this->mbThumbnailFileACExists;
	}
	public function setThumbnailFileACExists($nValue){
		$this->mbThumbnailFileACExists = $nValue;
	}
	
	public function getPictureFileOldAC(){
		return $this->msPictureFileOldAC ;
	}
	public function setPictureFileOldAC($nValue){
		$this->msPictureFileOldAC  = $nValue;
	}

	
}

?>
<?php

	if ($_SERVER['REQUEST_METHOD']=="GET") {
		header('Content-Type: application/json');
		$arr = array('ErrorGA' => "Has enviado get, tiene que ser POST");
		echo json_encode($arr);
		return false;
	}

	$usu ="";
	if(!isset($_POST['user_id']) || ($_POST['user_id']=="")) { 
		header('Content-Type: application/json');
		echo json_encode(array('ErrorGA'=>'Falta user_id')); 
		return false;
	} else{
		$usu = $_POST['user_id'];
	}

	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/database/DB_Connection.php");
	
	global $mysqli;
	global $errorLog;

	require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Tutor.php'); 
	require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 


	$obj = new Tutor();
	
	if ($obj->existsUserID($usu) == true ) {
		header('Content-Type: application/json');
		$arr = array('resultado' => "1");
		echo json_encode($arr);	
		return false;
	} else {
		header('Content-Type: application/json');
		$arr = array('resultado' => "0");
		echo json_encode($arr);	
		return false;
	}
	
	
	
  

?>
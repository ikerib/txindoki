<?php
	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/database/DB_Connection.php");
	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/portal/AuthenticationWeb.php");
	
	
	global $mysqli;
	global $errorLog;


    
	//$sql = "SELECT tutor_course_id,tutor_id, course_id, role_moodle FROM ic_tutor_course";
	
	$sql = "SELECT DISTINCT tutor_course_id,tutor_id, ic_tutor_course.course_id, role_moodle, ic_course.course_name
			FROM ic_tutor_course
			LEFT JOIN ic_course
				ON ic_tutor_course.course_id = ic_course.course_moodle_id
			ORDER BY tutor_course_id";
	

	$rs = $mysqli->query($sql);

	while($row = $rs->fetch_array())		{
		$arr[] = $row;
	}
	header('Content-Type: application/json');
	print_r('{"members":'.json_encode($arr).'}');

?>
﻿<?php
// Etiquetas para el área privada en ES

$prefijo="LBL_";
$aux="";

define($prefijo."LNG_SPANISH", "Gaztelania");
define($prefijo."LNG_BASQUE", "Euskara");

define($prefijo."PAG_1", " ");
define($prefijo."PAG_2", "orrialde");

define($prefijo."CAMPUS_GUPREST", "Gureak Akademi");
define($prefijo."AUDIO_REPLAY", "Errepikatu");

define($prefijo."MNU_LARGE", $aux."Handia");
define($prefijo."MNU_LARGE_ALT", $aux."Testu handiagoa");
define($prefijo."MNU_SMALL", $aux."Txikia");
define($prefijo."MNU_SMALL_ALT", $aux."Testu txikiagoa");
define($prefijo."MNU_COLOR", $aux."Kolorea");
define($prefijo."MNU_COLOR_HIGH_ALT", $aux."Kontraste handiko bista");
define($prefijo."MNU_COLOR_NORMAL_ALT", $aux."Bista arrunta");
define($prefijo."MNU_COLOR_VERSION_HIGH_ALT", $aux."Kontraste handiko bertsioa");
define($prefijo."MNU_COLOR_VERSION_NORMAL_ALT", $aux."Bertsio arrunta");

define($prefijo."MNU_HELP", $aux."Laguntza");
define($prefijo."MNU_SOUND", $aux."Soinua");
define($prefijo."MNU_SOUND_OFF_ALT", $aux."Ahotsa");
define($prefijo."MNU_SOUND_ON_ALT", $aux."Ahotsik gabe");

define($prefijo."REQUIRED_FIELD", "$1 arloa derrigorrezkoa da.");

define($prefijo."INI_TXT_01", $aux."Ongietorri");
define($prefijo."INI_TXT_02", $aux."¿Nola sartu Gureak Akademian?<br />Zure aukera hautatu:<br /><b>A</b> Zure izena eta pasahitza idatzi.<br /><b>B</b> zure pendrive-a ordenagailuan sartu.");

define($prefijo."INI_ACC_ACCESS", $aux."Sarbidea");
define($prefijo."INI_ACC_USER", $aux."Erabiltzailea");
define($prefijo."INI_ACC_PASSWORD", $aux."Pasahitza");
define($prefijo."INI_ACC_ENTER", $aux."Sartu");
define($prefijo."INI_ACC_FORGOT_PASSWORD", $aux."Nire pasahitza ahaztu dut");

define($prefijo."INI_OPTION_A", $aux."A Aukera");
define($prefijo."INI_OPTION_B", $aux."B Aukera");
define($prefijo."INI_USB_ACCESS", $aux."Pendrive-ekin sartu");
define($prefijo."INI_LOGO_DFG", $aux."Gipuzkoako Foru Aldundiko Logoa ");
define($prefijo."INI_LOGO_FSE", $aux."Europear Fondo Sozialaren logoa");

define($prefijo."POD_LNG_TXT_01", $aux."Hizkuntza aldatuta.");
define($prefijo."POD_LNG_TXT_02", $aux."Hizkuntza ongi aldatu da.");

define($prefijo."PIE_CREDITOS", $aux."Kredituak");


///// DENTRO DEL ÁREA PRIVADA /////
define($prefijo."ARE_PRI_VOLVER", $aux."Itzuli");
define($prefijo."ARE_PRI_CAMBIAR", $aux."Aldatu");
define($prefijo."ARE_PRI_DESCARGAR", $aux."Deskargatu");
define($prefijo."ARE_PRI_ENLACE", $aux."Lotura");
define($prefijo."ARE_PRI_ESTAS", $aux."Zaude:");
define($prefijo."ARE_PRI_INICIO", $aux."Hasiera");
define($prefijo."ARE_PRI_CURSOS", $aux."Ikastaroa");
define($prefijo."ARE_PRI_JUEGOS", $aux."Jokoak");
define($prefijo."ARE_PRI_BIBLIOTECA", $aux."Liburutegia");
define($prefijo."ARE_PRI_CAFETERIA", $aux."Kafetegia");
define($prefijo."ARE_PRI_NOTICIAS", $aux."Berriak");
define($prefijo."ARE_PRI_TUTORES", $aux."Tutoreak");
define($prefijo."ARE_PRI_COMPRENDER", $aux."Ulertu");
define($prefijo."ARE_PRI_MIS_CURSOS", $aux."Nere ikastaroak");
define($prefijo."ARE_PRI_MIS_CATALOGO", $aux."Katalogoa");
define($prefijo."ARE_PRI_HAY_PAGINAS", $aux."<strong>$1</strong> orrialde daude");
define($prefijo."ARE_PRI_HAY_PAGINA", $aux."<strong>$1</strong> orrialde daude");
define($prefijo."ARE_PRI_ANTERIOR", $aux."Aurrekoa");
define($prefijo."ARE_PRI_SIGUIENTE", $aux."Hurrengoa");
define($prefijo."ARE_PRI_PERFIL", $aux."Profil");
define($prefijo."ARE_PRI_MI_PERFIL", $aux."Nire profila");
define($prefijo."ARE_PRI_GESTION_FORMACION", $aux."Prestakuntza Kudeaketa");
define($prefijo."ARE_PRI_GESTION_RECURSOS", $aux."Baliabideak");
define($prefijo."ARE_PRI_GESTION_BACKEND", $aux."Backend");

define($prefijo."ARE_PRI_MNU_INICIO_TITLE", $aux."Hasierako pantalla.");
define($prefijo."ARE_PRI_MNU_BIBLIOTECA_TITLE", $aux."Dokumentuak aztertzeko.");
define($prefijo."ARE_PRI_MNU_CURSOS_TITLE", $aux."Zure ikastaroetan sartzeko edo beste batzuek aztertzeko.");
define($prefijo."ARE_PRI_MNU_CAFETERIA_TITLE", $aux."Zure kideekin hitzegiteko.");
define($prefijo."ARE_PRI_MNU_JUEGOS_TITLE", $aux."Jolastu eta ondo pasatzeko.");
define($prefijo."ARE_PRI_MNU_NOTICIAS_TITLE", $aux."Gureak Akademi inguruko berriak ikusteko.");
define($prefijo."ARE_PRI_MNU_TUTORES_TITLE", $aux."Arduradunentzako tokia .");

define($prefijo."ARE_PRI_MNU_INICIO", $aux."hasiera");
define($prefijo."ARE_PRI_MNU_BIBLIOTECA", $aux."liburutegia");
define($prefijo."ARE_PRI_MNU_CURSOS", $aux."ikastaroak");
define($prefijo."ARE_PRI_MNU_CAFETERIA", $aux."kafetegia");
define($prefijo."ARE_PRI_MNU_JUEGOS", $aux."jokoak");
define($prefijo."ARE_PRI_MNU_NOTICIAS", $aux."berriak");
define($prefijo."ARE_PRI_MNU_TUTORES", $aux."tutoreak");

define($prefijo."ARE_PRI_MNU_SALIR", $aux."Gureak Akademitik atera");
define($prefijo."ARE_PRI_MNU_DESCONECTAR", $aux."Desconektatu");

define($prefijo."ARE_PRI_INI_NOTICIAS", $aux."berriak");
define($prefijo."ARE_PRI_INI_LEER_NOTICIAS", $aux."berri guztiak irakurri");

define($prefijo."ARE_PRI_PRF_TEXTO_PERFIL", $aux."Hemendik <b>aldatu</b> dezakezu zure <b>pasahitza</b> eta zure <b>profila</b> ren beste datuak.");
define($prefijo."ARE_PRI_PRF_NOMBRE", $aux."Izena");
define($prefijo."ARE_PRI_PRF_APELLIDO1", $aux."1. Abizena");
define($prefijo."ARE_PRI_PRF_APELLIDO2", $aux."2. Abizena");
define($prefijo."ARE_PRI_PRF_CONTACTO", $aux."Emaila");
define($prefijo."ARE_PRI_PRF_CONTR_ANTERIOR", $aux."Aurreko pasahitza");
define($prefijo."ARE_PRI_PRF_CONTR_NUEVA", $aux."Pasahitza berria");
define($prefijo."ARE_PRI_PRF_CONTR_REPETIR", $aux."Pasahitza errepikatu");

define($prefijo."ARE_PRI_PRF_EMAIL_TEXT01", "Posta elektronikoa behar duzu.");
define($prefijo."ARE_PRI_PRF_MSG_CONTR0", "Aurreko pasahitza ez da zuzena.");
define($prefijo."ARE_PRI_PRF_MSG_CONTR1", "Pasahitza berria eta Konfirmazioa berdinak izan behar dira.");
define($prefijo."ARE_PRI_PRF_MSG_CONTR2", "Pasahitza berria eta Konformazioa bete.");

define($prefijo."ARE_PRI_RCO_RECORDAD_CONTRASENA", "Pasahitza gogoratu");
define($prefijo."ARE_PRI_RCO_ESCRIBE", "Zure <b>Posta elektroniko</b> helbidea idatzi, eta jarraian <b>Bidali</b> botoian klikatu.");
define($prefijo."ARE_PRI_RCO_BREVE", "Zure posta pertsonalean pasahitza aldatzeko jarraibideak laister jasoko dituzu.");
define($prefijo."ARE_PRI_RCO_CONTACTENOS", "Posta elektronikoa edo Gureak Akademian sartzeko arazorik baduzu, telefonoz deitu ezazu <b>50109</b>.");
define($prefijo."ARE_PRI_RCO_CORREO", "Posta elektronikoa");
define($prefijo."ARE_PRI_RCO_OBLIGATORIO", "Derrigorrezko arloa ");
define($prefijo."ARE_PRI_RCO_ENVIAR", "Bidali");
define($prefijo."ARE_PRI_RCO_CORREO_VACIO", "Zure posta elektronikoa idatzi.");
define($prefijo."ARE_PRI_RCO_CORREO_INCORRECTO", "Posta elektronikoa ez da zuzena.");
define($prefijo."ARE_PRI_RCO_MENSAJE_ENVIADO", "Mezua bidalita");
define($prefijo."ARE_PRI_RCO_MENSAJE_ENVIADO_INFO", "Zure mezua egoki bidali da .<br />Zure posta pertsonalean Gureak Akademian sarteko jarraibideak laister jasoko dituzu.");

define($prefijo."ARE_PRI_RCO_MENSAJE_ASUNTO", "Gureak Akademiak pasahitza berreskuratzen lagunduko dizu");
define($prefijo."ARE_PRI_RCO_MENSAJE_INSTRUCCIONES", "Kide agurgarria,\nzure pasahitza errekuperatzeko eskaera egin duzu.\n\nZure pasahitza errekuperatzeko eskaera ez badezu egin mezu honi ez kasorik egin, kontrakoa bada hurrengo jarraibideak bete itzazu.\n\n1-Hurrengo link-an klikatu ezazu $1.\n\n2-Azaltzen den orrialdean Pasahitza berria eta konfirmazioa idatzi behar duzu.\n\n3- Bidali klikatu eta mezu bat jasoko duzu zure pasahitza konfirmatzen.\n\n4-Gureak Akademira sartu zaitezke pasahitza berriarekin.\n\n");
define($prefijo."ARE_PRI_RCO_MENSAJE_FIRMA", "Adeitasunez,\nGureak Akademi");
define($prefijo."ARE_PRI_RCO_PROCESO", "Pasahitza errekuperatzeko prozesua");
define($prefijo."ARE_PRI_RCO_PROCESO_1", "Kide agurgarria, idatzi zure pasahitza berria eta konfirmazioa, jarraian Aldatu Klikatu.");

define($prefijo."ARE_PRI_RCO_MENSAJE2_ASUNTO", "Gureak Akademian sartzeko pasahitza berria");
define($prefijo."ARE_PRI_RCO_MENSAJE2_INSTRUCCIONES", "Kide agurgarria,\nse zure pasahitza ongi aldatu duzu.\n\nZure sartzeko datuak:\n\nErabiltzailea: $1\nPasahitza:$2\n\n");
define($prefijo."ARE_PRI_RCO_MENSAJE2_FIRMA", "Adeitasunez,\nGureak Akademi");

define($prefijo."ARE_PRI_AVI_INTENTALO", "Saiatu berriro");
define($prefijo."ARE_PRI_AVI_INTENTALO_MENSAJE", "Erabiltzailea edo pasahitza ez dira zuzenak.");
define($prefijo."ARE_PRI_AVI_ACCESO_INCORRECTO", "Sarbide okerra");
define($prefijo."ARE_PRI_AVI_INTENTALO_CONTACTA", "Zure arduradunarekin harremanetan jarri .");


define($prefijo."ARE_PRI_KRI_CUAL_CONTRASENA", $aux."¿Zein da zure pasahitza?");
define($prefijo."ARE_PRI_KRI_PRIMERO_PINCHA", $aux."Lehenik <b>klikatu zure irudien gainean</b>");
define($prefijo."ARE_PRI_KRI_PROBLEMA_RESPONSABLE", $aux."Arazorik baduzu zure arduraduna abisatu.");
define($prefijo."ARE_PRI_KRI_COCHE", $aux."autoa");
define($prefijo."ARE_PRI_KRI_CAMION", $aux."kamioia");
define($prefijo."ARE_PRI_KRI_AVION", $aux."hegazkina");
define($prefijo."ARE_PRI_KRI_BICI", $aux."bizikleta");
define($prefijo."ARE_PRI_KRI_VELERO", $aux."belaontzia");
define($prefijo."ARE_PRI_KRI_VACIO", $aux."hutsa");
define($prefijo."ARE_PRI_KRI_SANDIA", $aux."sandia");
define($prefijo."ARE_PRI_KRI_PINA", $aux."anana");
define($prefijo."ARE_PRI_KRI_CEREZAS", $aux."gereziak");
define($prefijo."ARE_PRI_KRI_PLATANO", $aux."platanoa");
define($prefijo."ARE_PRI_KRI_UVAS", $aux."mahatsak");
define($prefijo."ARE_PRI_KRI_ELEFANTE", $aux."elefantea");
define($prefijo."ARE_PRI_KRI_JIRAFA", $aux."jirafa");
define($prefijo."ARE_PRI_KRI_TORTUGA", $aux."dortoka");
define($prefijo."ARE_PRI_KRI_GALLO", $aux."oilarra");
define($prefijo."ARE_PRI_KRI_SERPIENTE", $aux."sugea");
define($prefijo."ARE_PRI_KRI_ATENCION", $aux."Arreta");
define($prefijo."ARE_PRI_KRI_RECUERDA", $aux."Gogoratu zure irudien gainean klikatu behar duzula.");

define($prefijo."ARE_PRI_KRI_ERROR", $aux."Akatsa");
define($prefijo."ARE_PRI_KRI_IMAGENES_NO_CORRECTAS", $aux."Irudiak ez dira zuzenak.");
define($prefijo."ARE_PRI_KRI_IMAGENES_NO_CORRECTAS2", $aux."Irudiak<br/> ez dira zuzenak.");
define($prefijo."ARE_PRI_KRI_AVISA_RESPONSABLE", $aux."Zure arduraduna abisatu.");

define($prefijo."ARE_PRI_NOT_NOTICIAS", $aux."Berriak");
define($prefijo."ARE_PRI_NOT_LISTADO_NOTICIAS", $aux."Berri guztiak");

define($prefijo."ARE_PRI_GAM_JUEGOS", $aux."Jokoak");
define($prefijo."ARE_PRI_GAM_JUEGOS_INFO", $aux."Joko motak. <b>Zure aukera hautatu</b>");
define($prefijo."ARE_PRI_GAM_JUEGOS_ACCION", $aux."Ekintza");
define($prefijo."ARE_PRI_GAM_JUEGOS_PENSAR", $aux."Pentsatu");
define($prefijo."ARE_PRI_GAM_JUEGOS_MUCHO", $aux."Asko");
define($prefijo."ARE_PRI_GAM_JUEGOS_LIST", $aux."Joko zerrenda:");
define($prefijo."ARE_PRI_GAM_JUEGOS_TAMANO", $aux."Tamaina");
define($prefijo."ARE_PRI_GAM_JUEGOS_JUGAR", $aux."Jokatu");
define($prefijo."ARE_PRI_GAM_JUEGOS_OBJETIVO", $aux."Deskribapena:");

define($prefijo."ARE_PRI_CAF_CAFETERIA", $aux."Kafetegia");
define($prefijo."ARE_PRI_CAF_CAFETERIA_INFO", $aux."Kafetegia pantailan zaude, hemen zure kideekin harremanetan jarri zaitezke. Egunkaria irakurri. telebista ikusi. Irratia entzun. Eta gauza gehiago ere.");
define($prefijo."ARE_PRI_CAF_CAFETERIA_LISTADO", $aux."Zerrenda:");


define($prefijo."ARE_PRI_CAF_CAFETERIA_INFO_APRENDER", $aux."Nahi duzun lekura joan zaitezke irudian klikatuz. <b>Zure aukera hautatu:</b>");

define($prefijo."ARE_PRI_CRS_MIS_CURSOS", $aux."Nire kurtsoak");
define($prefijo."ARE_PRI_CRS_TUS_CURSOS", $aux."Hauek zure kurtsoak dira:");
define($prefijo."ARE_PRI_CRS_VER_CATALOGO", $aux."Katalogoa ikusi");
define($prefijo."ARE_PRI_CRS_CATALOGO_CURSOS", $aux."Kurtsoen katalogoa");
define($prefijo."ARE_PRI_CRS_TIPOS_CURSOS", $aux."3 kurtso mota dituzu. <b>Zure aukera hautatu::</b>");
define($prefijo."ARE_PRI_CRS_FRM_PRELABORAL", $aux."Lan aurreko Heziketa");
define($prefijo."ARE_PRI_CRS_FRM_PROFESIONAL", $aux."Lanbide Heziketa");
define($prefijo."ARE_PRI_CRS_FRM_EXTRALABORAL", $aux."Lan kanpoko Heziketa");
define($prefijo."ARE_PRI_CRS_LISTADO_CURSOS", $aux."Kurtsoen zerrenda:");
define($prefijo."ARE_PRI_CRS_OBJETIVO", $aux."Helburua");
define($prefijo."ARE_PRI_CRS_PROGRAMA", $aux."Programa");
define($prefijo."ARE_PRI_CRS_DURACION", $aux."Iraupena");
define($prefijo."ARE_PRI_CRS_IDIOMA", $aux."Hizkuntza");
define($prefijo."ARE_PRI_CRS_RESPONSABLE", $aux."Arduraduna");
define($prefijo."ARE_PRI_CRS_TIPO_INSCRIPCION", $aux."Inskripzio mota");

define($prefijo."ARE_PRI_CRS_INSCRIBIRSE", $aux."Apuntatu");
define($prefijo."ARE_PRI_CRS_INSCRIBIRSE_TEXT1", $aux."Hurrengo kurtsoan apuntatu berria zara ");

define($prefijo."ARE_PRI_CRS_NO_HAY_CURSOS", $aux."Honako kategorian ez dira kurtsoak aurkitu oraingoz");

define($prefijo."ARE_PRI_BIB_BIBLIOTECA", $aux."Liburutegia");
define($prefijo."ARE_PRI_BIB_BIBLIOTECA_LIST", $aux."Zerrenda:");
define($prefijo."ARE_PRI_BIB_BIBLIOTECA_INFO", $aux."Liburutegiaren pantailan zaude, nahi duzun lekura joan zaitezke irudian klikatuz. <b>Zure aukera hautatu:</b>");

define($prefijo."ARE_PRI_TUT_TUTORES_OP", $aux."Aukera");
define($prefijo."ARE_PRI_TUT_TUTORES_TUT", $aux."Tutoreak");
define($prefijo."ARE_PRI_TUT_TUTORES_TUT_INFO", $aux."Tutoreen pantailan zaude, nahi duzun baliabidera joan zaitezke irudian klikatuz. <b>Zure aukera hautatu:</b>");
define($prefijo."ARE_PRI_TUT_TUTORES_REP", $aux."Biltegia");
define($prefijo."ARE_PRI_TUT_TUTORES_BUS", $aux."Bilaketa arrunta");
define($prefijo."ARE_PRI_TUT_TUTORES_BUSAV", $aux."Bilaketa aurreratua ");
define($prefijo."ARE_PRI_TUT_TUTORES_BUSCLAVE", $aux."Hitz gakoen bilaketa");
define($prefijo."ARE_PRI_TUT_TUTORES_MIS_RECURSOS", $aux."Nire baliabideak");
define($prefijo."ARE_PRI_TUT_TUTORES_SUB_RECURSOS", $aux."Baliabideak igo");
define($prefijo."ARE_PRI_TUT_TUTORES_SEARCH", $aux."Bilaketaren emaitzak:");
define($prefijo."ARE_PRI_TUT_TUTORES_DET", $aux."Xehetasunak");

define($prefijo."ARE_PRI_TUT_TUTORES_FICHA_1", $aux."<b>Egilea:</b>");
define($prefijo."ARE_PRI_TUT_TUTORES_FICHA_2", $aux."<b>Argitaratze data:</b>");
define($prefijo."ARE_PRI_TUT_TUTORES_FICHA_3", $aux."<b>Baliabidea:</b>");
define($prefijo."ARE_PRI_TUT_TUTORES_FICHA_4", $aux."<b>Baliabidea:</b>");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_1", $aux."Orokorra");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_2", $aux."Azalpena");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_3", $aux."Argitaratzea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_4", $aux."Teknika");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_5", $aux."Hezkuntza erabilera");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_6", $aux."Asmoa");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_0", $aux."Egilea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_1", $aux."Izenburua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_2", $aux."Hizkuntza");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_3", $aux."Formato");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_4", $aux."Kokapena");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_5", $aux."Interaktibitate mota");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_6", $aux."Baliabide mota");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_7", $aux."Baliabide mota");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_8", $aux."Egilea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_9", $aux."Data");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_10", $aux."Arte");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_11", $aux."Erabiltzailearen eginkizuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_12", $aux."Testuingurua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_13", $aux."Zailtasuna");



define($prefijo."ARE_PRI_TUT_TUTORES_ADV_FECHA", $aux."Data");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_SELEC_OPCIONES", $aux."Aukeretako bat hautatu");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_SELEC_IDIOMA", $aux."Hizkuntza aukeratu");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_SELEC_NIVEL", $aux."Maila aukeratu");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_SELEC_VERSION", $aux."Bertsioa aukeratu");


define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_1", $aux."Euskara");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_2", $aux."Gaztelania");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_3", $aux."Ingelera");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_4", $aux."Zeinu mintzaira");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_5", $aux."Besteak");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_6", $aux."Liburua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_7", $aux."Joko serioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_8", $aux."Material desberidina");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_9", $aux."Ikusgaia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_10", $aux."Aktiboa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_11", $aux."Konbinatua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_12", $aux."Irudia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_13", $aux."Bideoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_14", $aux."Audioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_15", $aux."Animazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_16", $aux."Grafikoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_17", $aux."Jarduera");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_18", $aux."Ariketa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_19", $aux."Ebaluazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_20", $aux."Galdeketa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_21", $aux."Textua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_22", $aux."Ikastaroa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_23", $aux."Tutoriala");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_24", $aux."Tailerra");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_25", $aux."Pilula");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_26", $aux."Tutorea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_27", $aux."Ikaskuntza Objektua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_28", $aux."Unitate didaktikoa");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_29", $aux."Bakarkakoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_30", $aux."Taldekoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_31", $aux."MOI");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_32", $aux."MOD");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_33", $aux."Aldea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_34", $aux."Bertaratu");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_35", $aux."Erdi bertaratua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_36", $aux."Hasierakoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_37", $aux."Ertaina");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_38", $aux."Aurreratua");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_0", $aux."Pertsonalak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_1", $aux."Gaitasun fisikoak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_2", $aux."Ikusmena");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_3", $aux."Entzumena");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_4", $aux."Mintamena");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_5", $aux."Gaitasun Motorea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_6", $aux."Motrizitate fina");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_7", $aux."Motrizitate lodia eta indarra");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_8", $aux."Osasun egoera");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_9", $aux."Adimen gaitasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_10", $aux."Arreta");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_11", $aux."Adimen nekea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_12", $aux."Kontzentrazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_13", $aux."Pertzeptzioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_14", $aux."Memoria (sensoriala)");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_15", $aux."Denbora/espazio orientazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_16", $aux."Orientazio espaziala");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_17", $aux."Denboraren orientazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_18", $aux."Erritmoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_19", $aux."Ardura pertsonala");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_20", $aux."Garbitasun eta irudi pertsonala");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_21", $aux."Osasun eta segurtasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_22", $aux."Osasun mantentzea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_23", $aux."Autorregulazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_24", $aux."Emozioen kontrola");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_25", $aux."Diskrezioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_26", $aux."Konfidentzialtasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_27", $aux."Gaitasun akademiko funtzionalak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_28", $aux."Zenbakiak erabili eta erlazionatu");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_29", $aux."Kalkulatu");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_30", $aux."Arazo-konponketa ");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_31", $aux."Konpetentzia Digitala");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_32", $aux."Teknologia berrien inguruko jarrera ");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_33", $aux."Ezaguera digitalak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_34", $aux."Multimedia tratamendua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_35", $aux."Komunitate erabilera");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_36", $aux."Komunitateateriko erlazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_37", $aux."Garraioen erabilera");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_38", $aux."Diru erabilera");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_39", $aux."Etxeko - bizita");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_40", $aux."Enplegu bilaketa ");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_41", $aux."Enplegu bilaketa teknika aktiboak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_42", $aux."Gizarte-Lanekoak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_43", $aux."Konpetentzia sozialak eta interpentsonalak lan arloan");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_44", $aux."Komunikazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_45", $aux."Hizkuntza");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_46", $aux."Informazioaren interpretazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_47", $aux."Informazioaren igorpena");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_48", $aux."Enpatia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_49", $aux."Asertibotasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_50", $aux."Talde lana");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_51", $aux."Lidergoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_52", $aux."Integrazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_53", $aux."Adiskidetasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_54", $aux."Autokontrola");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_55", $aux."Portaerazko jarraibideak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_56", $aux."Arazo-konponketa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_57", $aux."Stressari tolerantzia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_58", $aux."Kritikari tolerantzia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_59", $aux."Eskubideak eta betebeharrak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_60", $aux."Arauen betetzea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_61", $aux."Asistentzia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_62", $aux."Puntualtasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_63", $aux."Lan eta sindikal eskubideak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_64", $aux."Konpetentzi metodologikoak edo oinarrizko jarrerak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_65", $aux."Egokitzea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_66", $aux."Malgutasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_67", $aux."Egokigarritasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_68", $aux."Ikaskuntza");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_69", $aux."Erantzunkizuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_70", $aux."Konpromezua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_71", $aux."Inplikazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_72", $aux."Autokritika");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_73", $aux."Produktibitatea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_74", $aux."Exekuzioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_75", $aux."Erritmoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_76", $aux."Iraunkortasuna");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_77", $aux."Ekimena eta Autonomia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_78", $aux."Erabaki hautaketa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_79", $aux."Analisia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_80", $aux."Planifikazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_81", $aux."Organizazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_82", $aux."Enprendizajea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_83", $aux."Sormena");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_84", $aux."Autozainketa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_85", $aux."Autoestimua eta konfidantza");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_86", $aux."Motibazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_87", $aux."Gogoeta");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_88", $aux."Enpresarekiko posizionamendua");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_89", $aux."Enpresarekiko identifikatzea");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_90", $aux."Irudia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_91", $aux."Bezero Ikusmena");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_92", $aux."Bezero Orientzaioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_93", $aux."Tekniko profesionalak");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_94", $aux."Bai");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_95", $aux."Ez");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_96", $aux."Autozainketa");

define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_1", $aux."Arrakastaz igotako fitxategia! <br>");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_2", $aux."Zerbitzari lokalera fitxategia bidaltzean hutsegitea! <br>");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_3", $aux."Kudeatzaile dokumentalera arrakastaz bidalitako fitxategia! <br>");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_4", $aux."Kudeatzaile dokumentalera fitxategia bidaltzean hutsegitea ! <br>");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_5", $aux."Hutsegitea, fitxategi berbera helburu-ibilbide berdinera ari zara bidaltzen! <br>");

define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_OTRO", $aux."Beste fitxategia igo");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_RECORDATORIO_1", $aux."<strong>NOTA:</strong> Gogoratu izartxoa duten atalak");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_RECORDATORIO_2", $aux."derrigorrez bete behar direla");

define($prefijo."ARE_PRI_TUT_TUTORES_NIVEL", $aux."Agregazioa maila");
define($prefijo."ARE_PRI_TUT_TUTORES_NIVEL_ACCESO", $aux."Atzipen maila");
define($prefijo."ARE_PRI_TUT_TUTORES_VERSION", $aux."Bertsioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO", $aux."Fitxategia");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_MAX", $aux."25MB gehienezko tamaina");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_NOTAS", $aux."Instalazio oharrak");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_ORIENTACION", $aux."Orientazio didaktikoak");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_REL_RECURSO", $aux."Baliabide erlazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_REL_REQ", $aux."Beharrezkoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_REL_TIEMPO_APRENDIZAJE", $aux."Ikaskuntza denbora");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_CONOC", $aux."Aurretiko jakinduria");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_COPY", $aux."Copyright eta beste murrizketak ");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_COPY_1", $aux."Hirugarrenekiko eskubideak");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_COPY_2", $aux."Eskubideak negozioari murriztuta");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_TIPO", $aux."Tipoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_1", $aux."Beharrezkoa");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_2", $aux."Beharrezkoa da");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_3", $aux."Partea du");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_4", $aux."Partea da");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_IDENT", $aux."Identifikazioa");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_DESC", $aux."Azalpena");





?>

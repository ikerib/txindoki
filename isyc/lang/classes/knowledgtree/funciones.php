<?php


	//Includes
	require_once('config.php');
	require_once('RestClient.class.php');
	require_once('xml2array.php');


	//Funciones
	function login(){ 
	global $user_kt, $password_kt, $url_kt;

		$params = array("method" => "login", "username" => $user_kt, "password" => $password_kt);
		$ex = RestClient::post($url_kt, $params);

		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){
			$data = xml2array($ex->getResponse());
			
			if ($data["response"]["status_code"] == 0) {			 
				$_SESSION['isyc_knowledgtree'] = $data["response"]["results"];
				
				return true;	
			}else{ 			 
				$_SESSION['isyc_knowledgtree'] = ""; //Ponemos a vacio la variable de Session
				
				return false;	
			}
		}else{
			echo "��Error!! - Algun dato de la conexi�n con Knowledgtree esta fallando";
		}
		
	}


	function subcarpetas($id_carpeta_padre){
	global $url_kt;
		
		$params= array("method" => "get_folder_contents", "folder_id" => $id_carpeta_padre, "session_id" => $_SESSION['isyc_knowledgtree']);
		$ex = RestClient::post($url_kt,$params);
		
		$directorios[]= array();
		//echo "<br/>***>$id_carpeta_padre------Z>".count($directorios);
		
		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){        
			$xml = $ex->getResponse();
	        $o = simplexml_load_string(trim($xml));			

			if($o->status_code == 0) { //Si existe la ruta y nos devuelve 0 el status_code		
				$tree = $o->results->items;			

				foreach ($tree as $item){
					
					foreach ($item as $carpeta){
						if($carpeta->item_type == "F"){	//Obtengo las carpetas							
							 $tmp = array('id' => "$carpeta->id", 'filename' => "$carpeta->filename");	
							 $directorios[] = $tmp;	
						}
					}
				}	
			}
			//echo "<br/>--->".count($directorios);
		return 	$directorios;
		
		}else{		
			//echo "��Error!! - No se ha podido encontrar las subcarpetas";		
		}	
	}
	
	function obtener_nombre_carpeta($id_carpeta_mostrar){
	global $url_kt;
		
		$params= array("method" => "get_folder_detail", "folder_id" => $id_carpeta_mostrar, "session_id" => $_SESSION['isyc_knowledgtree']);
		$ex = RestClient::post($url_kt,$params);

		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){  
		
			$xml = $ex->getResponse();
	        $o = simplexml_load_string(trim($xml));			

			if($o->status_code == 0) {			
				$folder = $o->results;				
			}
			
			$nombre_carpeta = $folder->folder_name; //obtenemos el nombre de la carpeta
			
			return 	$nombre_carpeta;
		
		}else{		
			//echo "��Error!! - No se ha podido encontrar el nombre de la carpeta solicitada";		
		}	
	}
	
	function link_descarga_documento($id_documento){
	global $url_kt;
	
	$params = array("method" => "download_document", "document_id" => $id_documento, "session_id" => $_SESSION['isyc_knowledgtree']);
	$ex = RestClient::post($url_kt,$params);
		
		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){        
				
			$xml = $ex->getResponse();
		    $o = simplexml_load_string(trim($xml));

			if($o->status_code == 0) {			
				$path_download = $o->results;			
				$path_download = trim(urldecode($path_download));
			}

			return $path_download;	
			
		}else{		
			echo "��Error!! - No se ha podido descargar el documento que solicita";		
		}
	}
	
	
	function obtener_contenido_carpeta($id_carpeta){
	global $url_kt;
	 	
		$archivos = "";
		
		$params= array("method" => "get_folder_contents", "folder_id" => $id_carpeta, "session_id" => $_SESSION['isyc_knowledgtree']);
		$ex = RestClient::post($url_kt,$params);

		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){        
			$xml = $ex->getResponse();
	        $o = simplexml_load_string(trim($xml));			

			if($o->status_code == 0) { //Si existe la ruta y nos devuelve 0 el status_code		
				$tree = $o->results->items;			

				foreach ($tree as $item){

					foreach ($item as $archivo){
					
						if($archivo->item_type == "D"){	//Obtengo los ficheros de la carpeta							
							 $tmp = array('id' => "$archivo->id", 'name' => "$archivo->title", 'filename' => "$archivo->filename", 'created_date' => "$archivo->created_date");				
						}
						$archivos[] = $tmp;	
						
					}
				}	
			}
		
		return 	$archivos;
		
		}else{		
			echo "��Error!! - No se ha podido encontrar archivos";		
		}	
	}
	
	function paginar_array_limitado($v, $l, $p) {
	
		// DEFINIMOS LA CANTIDAD DE P�GINAS 
		$paginas = ceil(count($v) / $l); 
		
		// CONDICION DE INICIO 
		$inicio = ($p-1)*$l; 
	       
		// CONDICION DE FINAL 
		$final = $p*$l; 
		
		// MOSTRAMOS LOS ITEMS RESPECTIVOS 
	      for ($i=$inicio; $i<$final; $i++) { 
	        
			if(isset($v[$i])){ 
	            $valores[] = $v[$i]; 
	        }else{  
	            break;
			}			
	      } 
		
		return $valores;
	}
	
	function obtener_foto_descripcion_archivo($id){
	global $url_kt;
		
		$params= array("method" => "get_folder_contents", "folder_id" => $id, "session_id" => $_SESSION['isyc_knowledgtree']);
		$ex = RestClient::post($url_kt,$params);

		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){        
			$xml = $ex->getResponse();
	        $o = simplexml_load_string(trim($xml));			

			if($o->status_code == 0) { //Si existe la ruta y nos devuelve 0 el status_code		
				$tree = $o->results->items;			

				foreach ($tree as $item){
					foreach ($item as $archivo){
					
						if($archivo->item_type == "D"){	//Obtengo los archivos con su ID, NOMBRE, MIME_TYPE & TAMA�O							
							 $tmp = array('id' => "$archivo->id", 'filename' => "$archivo->filename", 'mime_type' => "$archivo->mime_type", 'filesize' => "$archivo->filesize");				
						}
						$archivos[] = $tmp;	
						
					}
				}	
			}
		
		return 	$archivos;
		
		}else{		
			echo "��Error!! - No se ha podido encontrar archivos, ni fotos.";		
		}	
	}
	

	
	
	function informacion_documento($id_doc){
	global $url_kt;
	
	$params = array("method" => "get_document_detail", "detailstr" => "M",  "document_id" => $id_doc, "session_id" => $_SESSION['isyc_knowledgtree']);
	$ex = RestClient::post($url_kt,$params);
		
		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){        
				
			$xml = $ex->getResponse();
		    $o = simplexml_load_string(trim($xml));
		
			if($o->status_code == 0) { //Si existe la ruta y nos devuelve 0 el status_code		
				//$item = $o->result->item[1]->fields; //info: item[1] == metadata  || item[0]==tag cloud
				$item = $o;	
			}
			
		}else{		
			//echo "��Error!! - No se ha podido descargar el documento que solicita";		
		}
		
		return $item;
	}
	
	
	//Funcion para obtener las extensiones de los ficheros a visualizar o descargar
	function tipoArchivo($elArchivo){		
		$laExtension = strtolower(end(explode('.', $elArchivo)));			
		return $laExtension;
	}
	
	function ByteSize($bytes){
	    $size = $bytes / 1024;
	    if($size < 1024)
	        {
	        $size = number_format($size, 2);
	        $size .= ' KB';
	        } 
	    else 
	        {
	        if($size / 1024 < 1024) 
	            {
	            $size = number_format($size / 1024, 2);
	            $size .= ' MB';
	            } 
	        else if ($size / 1024 / 1024 < 1024)  
	            {
	            $size = number_format($size / 1024 / 1024, 2);
	            $size .= ' GB';
	            } 
	        }
	    return $size;		
    }	
	

	function nube_etiquetas($etiquetas){	

	   //saco los valores m�ximo y minimo de la apariciones de etiquetas
	   $valor_max = max($etiquetas);
	   $valor_min = min($etiquetas);
	   $diferencia = $valor_max - $valor_min;
	   
	   $valor_relativo = "";
	   
	   //ordeno el array
	   ksort($etiquetas);
	   
	   //creo la capa donde se van a mostrar las etiquetas
	   echo '<div class="nube">';
	   echo '<div class="etiquetas">';
	   
	   foreach ($etiquetas as $nombreetiqueta=>$apariciones){
	      //calculo un valor de 0 a 10 para cada etiqueta, porcentualmente seg�n valores m�ximos y m�nimos encontrados
	      $valor_relativo = round((($apariciones - $valor_min) / $diferencia) * 10);
	      //escribo las etiquetas con su estilo dependiendo del valor porcentual
	      echo "<a class='enlace' href='tutores_busq_simple.php?tag=$nombreetiqueta'><span class='etiquetatam$valor_relativo'>";
	      echo $nombreetiqueta;
	      echo "</span></a>";
	   }
	   //meto una capa sin float para que tome todo el alto de las etiquetas
	   echo "<div style='clear:both'></div>";
	   //cierro la nube y las etiquetas
	   echo '</div>';
	   echo '</div>';
	} 		
	
	function search_tag($tag){
	
	global $url_kt;
	$archivos = "";
	
	$query_completed = 'Tag contains "'.$tag.'"';	

	$params= array("method" => "search", "query" => $query_completed, "options" => 10000, "session_id" => $_SESSION['isyc_knowledgtree']);
	$ex = RestClient::post($url_kt,$params);
	
	
		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){ 
		
			$xml = $ex->getResponse();
			$o = simplexml_load_string(trim($xml));	
		
			if($o->status_code == 0) { //Si existe la ruta y nos devuelve 0 el status_code

				$tree = $o->results;			

				foreach ($tree as $item){
					foreach ($item as $archivo){	
						$tmp = array('id' => "$archivo->document_id", 'name' => "$archivo->title", 'filename' => "$archivo->filename", 'mime_type' => "$archivo->mime_type", 'filesize' => "$archivo->filesize");
						$archivos[] = $tmp;							
					}
				}	
			}
		}else{ 
		
			echo "��Error!! - No se ha podido realizar su b�squeda de Tag."; //Lanzamos el error
			
		}	

		return $archivos;
	
	}
	
	function search_title($title){
	
	global $url_kt;
	$archivos = "";
	
	$query_completed = 'DocumentText contains "'.$title.'" OR Title contains "'.$title.'"';	

	$params= array("method" => "search", "query" => $query_completed, "options" => 10000, "session_id" => $_SESSION['isyc_knowledgtree']);
	$ex = RestClient::post($url_kt,$params);
	
	
		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){ 
		
			$xml = $ex->getResponse();
			$o = simplexml_load_string(trim($xml));	
		
			if($o->status_code == 0) { //Si existe la ruta y nos devuelve 0 el status_code

				$tree = $o->results;			

				foreach ($tree as $item){
					foreach ($item as $archivo){	
						$tmp = array('id' => "$archivo->document_id", 'name' => "$archivo->title", 'filename' => "$archivo->filename", 'mime_type' => "$archivo->mime_type", 'filesize' => "$archivo->filesize");
						$archivos[] = $tmp;							
					}
				}	
			}
		}else{ 
		
			echo "��Error!! - No se ha podido realizar su b�squeda de DocumentText & Title."; //Lanzamos el error
			
		}	

		return $archivos;
	
	}
	
	
	function search_advanced($data){
	
	global $url_kt;
	
	$query_completed = $data;
	$archivos = "";

	$params= array("method" => "search", "query" => $query_completed, "options" => 10000, "session_id" => $_SESSION['isyc_knowledgtree']);
	$ex = RestClient::post($url_kt,$params);
	
	
		if ($ex->getResponseCode() == "200" and $ex->getResponseContentType() == "text/xml"){ 
		
			$xml = $ex->getResponse();
			$o = simplexml_load_string(trim($xml));	
		
			if($o->status_code == 0) { //Si existe la ruta y nos devuelve 0 el status_code

				$tree = $o->results;			

				foreach ($tree as $item){

					foreach ($item as $archivo){	
						$tmp = array('id' => "$archivo->document_id", 'name' => "$archivo->title", 'filename' => "$archivo->filename", 'mime_type' => "$archivo->mime_type", 'filesize' => "$archivo->filesize", 'created_date' => "$archivo->created_date");
						$archivos[] = $tmp;							
					}
					
				}	
			}
		}else{ 
		
			echo "��Error!! - No se ha podido realizar su b�squeda por search_advanced."; //Lanzamos el error
			
		}	

		return $archivos;
	
	}
	
	
	function RecortarCadena($text) {
		
		//si la cadena que nos viene es mayor de 50 caracteres la recortamos, sino la conservamos tal c�al

        $chars = 50;

		if(strlen($text) >= $chars){
	        $text = $text." ";
	        $text = substr($text,0,$chars);
	        $text = substr($text,0,strrpos($text,' '));
	        $text = $text."...";
		}

        return $text;

    }

	
	function remplazo_nombres_ficheros($str) {
		$str = ereg_replace("[^a-zA-Z0-9.]",'_',$str);
		return $str;
	} 
	
	function tipo_formato_tutores($digital, $fisico){

	$resp = "";
	
		if($digital == "n/a" && $fisico == "n/a"){
			$resp = "";
		}elseif($digital != "n/a" && $fisico == "n/a"){
			$resp = "Digital / ".$digital;
		}elseif($digital == "n/a" && $fisico != "n/a"){
			$resp = "F�sico / ".$fisico;
		}else{
			$resp = "Digital / ".$digital." Fisico / ".$fisico;
		}
		
		return $resp;	
	
	}
	
	function cotar_cadena_taxonomia($cadena){
		
		$cadena_cortada = "";
		
		$cadena_cortada = substr($cadena, 12);
	
		return $cadena_cortada;
	}
	
?>
<?php
// -----------------------------------------
// Tutor.php
// -----------------------------------------
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Moodle_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/elearning/TutorData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/Functions.php');
require_once($_SERVER['DOCUMENT_ROOT']."/isyc/lang/es/config.php"); 

class Tutor extends TutorData
{
	public function loadData ($id){
	
		global $mysqli;
		global $errorLog;
		$format = new Format();
	  
		try {
			$query_select= sprintf("SELECT ic_tutor.tutor_id, 
										ic_tutor.first_name, 
										ic_tutor.last_name1, 
										ic_tutor.last_name2, 
										ic_tutor.user_id, 
										DATE_FORMAT(ic_tutor.registration_date,'%%Y/%%m/%%d') as registration_date, 
										
										ic_user.language,
										ic_user.e_mail,
										ic_user.password,
										
										ic_tutor.card_id, 
										ic_tutor.phone, 
										ic_tutor.mobile, 
										ic_tutor.city, 
										
										ic_tutor.company_id, 
										ic_tutor.remarks, 
										ic_tutor.active_flag, 
										ic_tutor.timestamp, 
										ic_tutor.modified_by
										
								FROM ic_tutor LEFT JOIN ic_user ON ic_tutor.user_id = ic_user.user_id
												
                          		WHERE ic_tutor.tutor_id = %d ", $id);
						
						$query_select.=sprintf(" AND ic_tutor.company_id = 1 ");

			$errorLog->LogDebug("SELECT: $query_select");
			
			if ($result = $mysqli->query($query_select)){
				
				if ($result->num_rows > 0){
					
					$row = $result->fetch_array();
				
					$this->setID($row["tutor_id"]);
					
					$this->setName($row["first_name"]);
					$this->setPersonLastName1($row["last_name1"]);
					$this->setPersonLastName2($row["last_name2"]);
					
					$this->setUserID($row["user_id"]);
					if ($this->getUserID()!=null && $this->getUserID()!="")
						$this->setIsUser(true);
					else
						$this->setIsUser(false);				
					
					$this->setRegistrationDate($format->formatea_fecha($row["registration_date"]));
					
					$this->setUserLang($row["language"]);
					$this->setUserEmail($row["e_mail"]);
					$this->setUserPass($row["password"]);
				
					$this->setCardID($row["card_id"]);
					$this->setPhone($row["phone"]);
					$this->setMobile($row["mobile"]);
					$this->setBirthPlace($row["city"]); 
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setTimestamp($row["timestamp"]);
					$this->setModifiedBy($row["modified_by"]);
					
					$this->setMoodleCourses($this->loadMoodleCourses());
					
					$result->close();
					
				}else{
					$errorLog->LogError($query_select);
					exit;
				}	

			}
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}			
		
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
	}//end loadData
	
	
	
	public function createNew()
	{
    }//END create_new
	
	public function update($id)
	{
		global $mysqli;
		global $errorLog;
		$format = new Format();
		
		try {
		
			$query_update=sprintf("UPDATE ic_tutor 
								SET first_name = '%s', 
									last_name1 = '%s', 
									last_name2 = '%s', 
									
									registration_date = '%s', 
									
									card_id = '%s',
									phone = '%s',
									mobile = '%s',
									city = '%s',
									
									remarks = '%s', 
									modified_by = '%s',
									timestamp = CURRENT_TIMESTAMP,
									company_id = %d
								WHERE 
									tutor_id = %d",								

									$this->getName(),
									$this->getPersonLastName1(),
									$this->getPersonLastName2(), 
									
									$format->formatea_fecha_bd($this->getRegistrationDate()),
									
									$this->getCardID(),
									$this->getPhone(),
									$this->getMobile(),
									$this->getBirthPlace(),
									
									$this->getRemarks(),	
									$this->getUserID(),
									$this->getCompanyID(),
									$id
    							);
			
			
			$errorLog->LogDebug("UPDATE: $query_update");
			$result = $mysqli->query($query_update);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			//Si viene un cambio de contraseña la modifico.
			if ($_POST["pass_nueva"]!=""){
				$query_update=sprintf("UPDATE ic_user
										SET
											password='%s',
											timestamp=timestamp
										WHERE user_id = '%s'	
									 ",
										md5($_POST["pass_nueva"]),
										$this->getUserID()
									);
				
				$errorLog->LogDebug("UPDATE: $query_update");
				
				$result = $mysqli->query($query_update);
				
				if ($mysqli->error){
					$errorLog->LogError($mysqli->error);
				}
			}
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
	
	}//END UPDATE    

    public function init()
    {
		$error=false;
		$error_txt="";
		$valor = "";
		
		//Param id
		$valor = $_POST['user_id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}
		
		//Param name
		$valor = $_POST['nombre'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_ARE_PRI_PRF_NOMBRE,LBL_REQUIRED_FIELD);
			$this->setName("");
		}
		
		//Param last_name_1
		$valor = $_POST['apell_1'];
		if (trim($valor)!=""){
			$this->setPersonLastName1($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_ARE_PRI_PRF_APELLIDO1,LBL_REQUIRED_FIELD);
			$this->setPersonLastName1("");
		}
		
		//Param last_name_2
		$valor = $_POST['apell_2'];
		if (trim($valor)!=""){
			$this->setPersonLastName2($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_ARE_PRI_PRF_APELLIDO2,LBL_REQUIRED_FIELD);
			$this->setPersonLastName2("");
		}
		
		
		//user_email
		if (isset($_POST["email"]) && $_POST["email"]!=""){
			$this->setUserEmail($_POST["email"]);	
		}else{
			$this->setUserEmail("");	
			$error_txt.="\\n- ".LBL_ARE_PRI_PRF_EMAIL_TEXT01;
		}
		
		
		//Si vienen ambos campos y ambos son iguales, cambiarle la contraseña también.
		//pass_anterior
		if (isset($_POST["pass_anterior"]) && $_POST["pass_anterior"]!=""){
				//Comprobamos que la contraseña es igual a la actual, previa al cambio.
				
				$objTutor = new Tutor();
				$objTutor->loadData($this->getID());
				
				if ($objTutor->getUserPass()==md5($_POST["pass_anterior"])){
					//OK
				}else{
					$error_txt.="\\n- ".LBL_ARE_PRI_PRF_MSG_CONTR0;
				}
		}
		
		//pass_nueva y pass_repetir
		if (isset($_POST["pass_nueva"]) && $_POST["pass_nueva"]!="" && isset($_POST["pass_repetir"]) && $_POST["pass_repetir"]!=""){
			
			if ($_POST["pass_nueva"]==$_POST["pass_repetir"]){
				
				//Todo OK, podemos cambiar la contraseña.
			}else{
				$error_txt.="\\n- ".LBL_ARE_PRI_PRF_MSG_CONTR1; //La nueva contraseña y su confirmación deben ser iguales.
			}
			
			
		}else{
			if (isset($_POST["pass_nueva"]) && ($_POST["pass_nueva"]!="" || $_POST["pass_repetir"]!=""))
				$error_txt.="\\n- ".LBL_ARE_PRI_PRF_MSG_CONTR2; //Debe escribir la nueva contraseña y su confirmación
		}
		
		
		return $error_txt;
    }//END init
	
	
	private function loadMoodleCourses()
	{
    
		global $mysqli;
		global $errorLog;		
		
        $query_select = sprintf("SELECT 
							ic_tutor_course.tutor_course_id,
							ic_tutor_course.course_id,
							ic_tutor_course.role_moodle
						FROM ic_tutor_course 
						WHERE ic_tutor_course.tutor_id = %d 
						ORDER BY tutor_course_id ", $this->getID());
						
		$errorLog->LogDebug("SELECT: $query_select");
        	
        $result = $mysqli->query($query_select);
		return $result;

	}//end loadMoodleCourses
	
	
	//Función que dado el user_id obtiene la ruta para la imagen del perfil en Moodle.
   	public function loadMoodleProfilePhoto($userid) 
	{
		global $mysqliMoodle;
		global $errorLog;
		
		$ruta="";
		
		try {
			$query_select= sprintf("SELECT 
										DISTINCT mdl_user.username, mdl_context.id FROM mdl_user
										INNER JOIN mdl_context ON mdl_user.id = mdl_context.instanceid
										WHERE contextlevel = 30 AND mdl_user.username = '%s' ",  $userid);
		
			$errorLog->LogDebug("SELECT: $query_select");
		
		 if ($result = $mysqliMoodle->query($query_select)){
				
				if ($result->num_rows > 0){
					$row = $result->fetch_array();
						
					$ruta = "/".$row["id"]."/user/icon/f1";
				}
		  }

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}		
	
		return $ruta;
	}//loadMoodleProfilePhoto
	
	
}

?>
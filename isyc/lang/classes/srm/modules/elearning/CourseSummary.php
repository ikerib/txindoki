<?php
// -----------------------------------------
// CourseSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/AbstractCRMObjectSummary.php');

class CourseSummary extends AbstractCRMObjectSummary
{
	
	
   public function CourseSummary()  
   {
   }	

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();
      
      if (null == $sOrderBy)
         return "ic_course.course_name_en";
		 
      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_course.course_desc"; break;
			default: $sOrderBy = "ic_course.course_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END
   
   public function  load()
   {
      
   }//end load
   
   
   public function loadFilter($sType, $sID)
   {
   
		$nCompanyID = $_SESSION["company_id"];
		$sLanguage = $_SESSION["language"];
     
		global $mysqli;
	  
		try
		{

			$sFrom = sprintf(" FROM ic_course LEFT JOIN ic_level ON ic_course.level_id = ic_level.level_id AND ic_level.language='%s'",$sLanguage);
            
			$sWhere = " WHERE ic_course.active_flag = 1 ";
      
			$sOperator = " = ";
			$sCondition = "";

			// Param name
			$sWhere.=$this->getWhereClause("name", "course_name", "ic_course", $this->STRING_TYPE, "", "");

			if ($sType=="GROUP")
			{ 
				$sFrom.=" LEFT JOIN ic_center_course ON ic_course.course_id = ic_center_course.course_id ";
				$sWhere.=" AND ic_center_course.center_id = " . $sID ;
			}else{
				$sWhere.=" AND 1 = 2 " ;
			}
			
			$query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;
	  
			//echo $query_select;
	  
			if ($result = $mysqli->query($query_select)){
				$row = $result->fetch_array();
				$this->init($row[0]);
				$result->close();
			}else{
				$this->init(0);
			}
		 
			$query_select="SELECT ic_course.course_id,
				  COALESCE(ic_course.course_name,'') as course_name,
				  COALESCE(ic_course.course_desc,'') as course_desc,
				  COALESCE(ic_level.level_name,'') as level_name ";
			
			if ($sType=="GROUP")
			{
				//$query_select.=", ic_certification_course.nota_minima";
			}
			
			$query_select.= $sFrom . $sWhere;
			$query_select.= " ORDER BY " . $this->getOrderByColumn() . " " . $this->getOrderHowString() . " ";
			$query_select.= " LIMIT " . $this->getMaxRowsNumber() . " OFFSET " . ($this->getCurrentPage()-1)*$this->getMaxRowsNumber();
		
			//echo $query_select;
	   
			$result = $mysqli->query($query_select);
	   
			return $result;
		
		}
		catch (Exception $ex)
		{
        }
   
   
   }
   
   
   
   public function  loadCoursesNoEnrolled($sLanguage, $categoryid, $studentid, $level, $nLimit, $nPage)
   {
    
     $nCompanyID = 1;
	 $sLanguage = $sLanguage;
     
     global $mysqli;
	 global $errorLog;
	  
      try
      {

      $sFrom = sprintf(" FROM ic_course LEFT JOIN ic_level ON ic_course.level_id = ic_level.level_id AND ic_level.language = '%s'",$sLanguage);
      	
      $sWhere = " WHERE ic_course.active_flag = 1 ";
	  $sWhere.=sprintf(" AND ic_course.validate_flag = 1 ");
	  $sWhere.=sprintf(" AND ic_course.course_category_id  = %d ", $categoryid);
	  $sWhere.=sprintf(" AND ic_course.level_id >= %d ", $level);
	  
	  $sWhere.=sprintf(" AND ic_course.course_id NOT IN (SELECT course_id FROM ic_student_course WHERE student_id= %d) ",$studentid);
      
      $query_select="SELECT ic_course.course_id,
				  COALESCE(ic_course.course_name,'') as course_name,
				  COALESCE(ic_course.course_desc,'') as course_desc,
				  ic_course.level_id,
				  ic_level.level_name";
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY course_name ";
       		
      $errorLog->LogDebug("SELECT: $query_select");
	   
	    if ($nLimit>0)
			$query_select.= " LIMIT " . $nLimit . " OFFSET " . ($nPage-1)*$nLimit;
	   
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
   }//end loadCoursesNoEnrolled
   
   
   
      public function  loadCoursesNoEnrolledTutor($sLanguage, $categoryid, $tutorid, $level, $nLimit, $nPage)
   {
    
     $nCompanyID = 1;
	 $sLanguage = $sLanguage;
     
     global $mysqli;
	 global $errorLog;
	  
      try
      {

      $sFrom = sprintf(" FROM ic_course ");
      	
      $sWhere = " WHERE ic_course.active_flag = 1 ";
	  $sWhere.=sprintf(" AND ic_course.course_category_id  = %d ", $categoryid);
	  
	  $sWhere.=sprintf(" AND ic_course.course_moodle_id NOT IN (SELECT course_id FROM ic_tutor_course WHERE tutor_id= %d) ",$tutorid);
      
      $query_select="SELECT ic_course.course_id,
				  COALESCE(ic_course.course_name,'') as course_name,
				  COALESCE(ic_course.course_desc,'') as course_desc,
				  ic_course.level_id ";
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY course_name ";
       		
      $errorLog->LogDebug("SELECT: $query_select");
	   
	    if ($nLimit>0)
			$query_select.= " LIMIT " . $nLimit . " OFFSET " . ($nPage-1)*$nLimit;
	   
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
   }//end loadCoursesNoEnrolledTutor
   
	
   //Funci�n que devuelve el listado de los cursosn en los que est� matriculado un alumno.	
	
   public function  loadCoursesEnrolled($sLanguage, $studentid, $nLimit, $nPage)
   {
    
     $nCompanyID = 1;
	 $sLanguage = $sLanguage;
     
     global $mysqli;
	  
      try
      {

      $sFrom = sprintf(" FROM ic_course LEFT JOIN ic_level ON ic_course.level_id = ic_level.level_id AND ic_level.language = '%s'",$sLanguage);
      $sFrom.= sprintf(" LEFT JOIN ic_student_course ON ic_course.course_id = ic_student_course.course_id  ");
      	
      $sWhere = " WHERE ic_course.active_flag = 1 ";
	  $sWhere.=sprintf(" AND ic_student_course.student_id = %d ", $studentid);
      
      $query_select="SELECT ic_course.course_id,
				  COALESCE(ic_course.course_name,'') as course_name,
				  COALESCE(ic_course.course_desc,'') as course_desc,
				  ic_course.level_id,
				  ic_level.level_name,
				  ic_course.course_moodle_id ";
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY course_name ";
	   
	   	if ($nLimit>0)
			$query_select.= " LIMIT " . $nLimit . " OFFSET " . ($nPage-1)*$nLimit;
       		
       //echo $query_select;
	   
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
   }//end loadPortaInfo
   
	
	
   //Funci�n que devuelve el listado de los cursos que est� impartiendo el tutor.	
   public function  loadCoursesTutor($sLanguage, $tutorid, $nLimit, $nPage)
   {
    
     $nCompanyID = 1;
	 $sLanguage = $sLanguage;
     
     global $mysqli;
	 global $errorLog;
	  
      try
      {

	   $query_select = sprintf("SELECT 
									ic_tutor_course.tutor_course_id,
									ic_tutor_course.course_id as course_moodle_id,
									ic_tutor_course.role_moodle,
									
									ic_course.course_id,
									ic_course.course_name

								FROM ic_tutor_course LEFT JOIN ic_course ON ic_tutor_course.course_id = ic_course.course_moodle_id
								WHERE ic_tutor_course.tutor_id =%d
									AND ic_course.active_flag = 1 ", $tutorid);
	    $query_select.= " ORDER BY course_id ";	
	   	if ($nLimit>0)
			$query_select.= " LIMIT " . $nLimit . " OFFSET " . ($nPage-1)*$nLimit;
       
	  
       $errorLog->LogDebug("SELECT: $query_select");
	   
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
   }//end loadCoursesTutor
	
	
}

?>
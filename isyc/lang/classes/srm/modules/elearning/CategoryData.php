<?php
// -----------------------------------------
// CategoryData.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/AbstractCRMObjectImage.php');

class CategoryData extends AbstractCRMObjectImage 
{
	public $msLanguage = "";
	public $msDescription = "";
	public $msShortname = "";
	public $mnOrderAppearence = "";
	
	public function getLanguage(){
		return $this->msLanguage;
	}
	public function setLanguage($nValue){
		$this->msLanguage = $nValue;
	}
	
	public function getDescription(){
		return $this->msDescription;
	}
	public function setDescription($nValue){
		$this->msDescription = $nValue;
	}
	
	public function getShortname(){
		return $this->msShortname;
	}
	public function setShortname($nValue){
		$this->msShortname = $nValue;
	}
	
	public function getOrderAppearence(){
		return $this->mnOrderAppearence;
	}
	public function setOrderAppearence($nValue){
		$this->mnOrderAppearence = $nValue;
	}
	
}

?>
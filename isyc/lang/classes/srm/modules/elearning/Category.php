<?php
// -----------------------------------------
// Category.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/elearning/CategoryData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/Functions.php');

class Category extends CategoryData
{
	
	public function loadData ($id){
		
	  global $errorLog;
	  
	  if (null == $id || "new"==$id)
      {
			$this->setID("new");
			$this->setName("");
			$this->setLanguage("");
			$this->setDescription("");	
			$this->setShortname("");
			$this->setOrderAppearence(1);
			
			$this->setPictureFile("");
			$this->setPictureName("");
			$this->setPictureWidth("");
			$this->setPictureHeight("");
			
			$this->setCompanyID(1);
			$this->setRemarks("");
			$this->setActiveFlag("");
			$this->setModifiedBy("");
			$this->setTimestamp("");
			return;
      }
      
      global $mysqli;
	  $format = new Format();	
	  try {
		$query_select= sprintf("SELECT ic_course_category.course_category_id,  
                     	ic_course_category.course_category_name,
                     	ic_course_category.course_category_desc,

						ic_course_category.language,
						
						ic_course_category.course_category_shortname,
						ic_course_category.order_appearance,
						
						ic_course_category.picture_name,
						ic_course_category.picture_file,
						ic_course_category.picture_width,
						ic_course_category.picture_height,
						
						ic_course_category.company_id,
						ic_course_category.remarks,
						ic_course_category.active_flag,
						ic_course_category.modified_by,
						ic_course_category.timestamp
						
                 FROM ic_course_category 
							    
                 WHERE  active_flag=1 AND
						course_category_id = %d", $id);
		
		$errorLog->LogDebug("loadData: $query_select");
		
		 if ($result = $mysqli->query($query_select)){
				
				if ($result->num_rows > 0){
					$row = $result->fetch_array();
				
					$this->setID($row["course_category_id"]);
					$this->setName($row["course_category_name"]);
					$this->setLanguage($row["language"]);
					$this->setDescription($row["course_category_desc"]);
					$this->setShortname($row["course_category_shortname"]);
					$this->setOrderAppearence($row["order_appearance"]);
					
					$this->setPictureName($row["picture_name"]);
					$this->setPictureFile($row["picture_file"]);
					$this->setPictureWidth($row["picture_width"]);
					$this->setPictureHeight($row["picture_height"]);
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setTimestamp($row["timestamp"]);
					
					$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_CATEGORY_URL);
					
					$this->setPictureFileExists(file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFile()));
					$this->setThumbnailFileExists(file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFile()));
					
					$result->close();
				}else{
					$errorLog->LogError("Location: /srm/index.php?error=3. ERROR: ".$query_select);
					header("Location: /srm/index.php?error=3"); 	
					exit;
				}	
				
			}		 
		
	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ".  $e->getMessage());
	  }
	
	}//end loadData
	
   public function createNew()
   {
   }//END create_new
   
   public function update($id)
   {
   }//END UPDATE

   public function delete($id)
   {
   }//END DELETE    

   public function init()
   {
   }//END init
   
}

?>
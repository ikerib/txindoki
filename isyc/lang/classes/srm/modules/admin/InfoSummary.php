<?php
// -----------------------------------------
// InfoSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/admin/InfoSummary.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/AbstractCRMObjectSummary.php');

class InfoSummary extends AbstractCRMObjectSummary
{
	
	
   public function InfoSummary()  
   {
   }	

   public function getOrderByColumn()
   {
      
   }//END
   
   

   public function  load()
   {
   }//end load
   
   
   
   //Funci�n que muestra el listado de las �ltimas o todas las noticias.
   //Si el l�mite es $sLimit=0 entonces mostrar� todas.
   public function  loadLastInfo($sLanguage, $nLimit, $nPage)
   {
    
     $nCompanyID = 1;
	 $sLanguage = $sLanguage;
     
     global $mysqli;
	 global $errorLog;
	  
      try
      {
	     
      $sFrom = " FROM ic_info ";
	
	  $sWhere = " WHERE ic_info.active_flag = 1 ";
	  $sWhere.= " AND ic_info.company_id = $nCompanyID ";
	  $sWhere.= " AND NOW() BETWEEN publication_date AND expiration_date ";
	  
	  
	  
	 if ($sLanguage=='es'){
		  $query_select="SELECT ic_info.info_id,
							COALESCE(ic_info.info_title,'') as info_title,
							COALESCE(ic_info.info_desc,'') as info_desc,
							ic_info.info_date	" ;
	 }else{
		$query_select="SELECT ic_info.info_id,
							COALESCE(ic_info.info_title_eu,'') as info_title,
							COALESCE(ic_info.info_desc_eu,'') as info_desc,
							ic_info.info_date	" ;
	 }
		   
		   
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY info_date DESC, info_id ";
	  
		if ($nLimit>0)
			$query_select.= " LIMIT " . $nLimit . " OFFSET " . ($nPage-1)*$nLimit;
		
	   $errorLog->LogDebug("loadLastInfo: $query_select");
	   
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
   }//end loadLastInfo
   
   
}//END CLASS
?>
<?php

// Clase

//VARIOS TIPOS DE ACCESO.
//1. DESDE UN PENDRIVE PASAN POR GET EL USER Y DEBE SELECCIONAR 3 PICTOGRAMAS (ALUMNOS)
//2. ACCESO NORMAL, PARA TUTORES Y ALUMNOS CON USUARIO Y CONTRASEŅA.
//3. CASO ESPECIAL DE ALUMNOS DE NIVEL 4. LOS CUALES ACCEDEN DIRECTAMENTE AL RECURSO ASOCIADO EN SU PERFIL.

require_once($_SERVER['DOCUMENT_ROOT']."/isyc/classes/database/DB_Connection.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/portal/ErrorLog.php');

class AuthenticationWeb
{
	public $msUsername = "";
	public $msPassword = "";
	public $msAccess = "";
	
	function AuthenticationWeb() 
	{
		if (isset($_POST['username']))
			$this->msUsername = $_POST['username'];	
		if (isset($_POST['password']))			
			$this->msPassword = $_POST['password'];
		if (isset($_POST['access']))			
			$this->msAccess = $_POST['access'];		
	}
	
	public function getPassword(){
		return $this->msPassword;
	}
	public function setPassword($value){
		$this->msPassword = $value;
	}
	
	public function getUsername(){
		return $this->msUsername;
	}
	public function setUsername($value){
		$this->msUsername = $value;
	}	
	
	function validate_user_web()
	{
		global $mysqli;
		global $errorLog;
		
		$is_valid = 0; //ERROR
		$user_type="";
		
		$encodePwd = $this->encodePassword();
		
		//MIRAMOS SI EL USUARIO ES ALUMNO O TUTOR.
		
		$query_select = "SELECT ic_user.user_id
						FROM ic_user
							LEFT JOIN ic_tutor ON ic_user.user_id = ic_tutor.user_id
						WHERE ic_user.user_id = '%s'
							AND ic_tutor.active_flag=1
							AND ic_user.active_flag = 1 ";
							
	  	$query_select = sprintf($query_select, $this->msUsername);
		
		$errorLog->LogDebug("SELECT: $query_select");
		
		if ($result = $mysqli->query($query_select)) //ES TUTOR
		{	
			if ($result->num_rows > 0){
				$user_type="TUTOR";
			}
		}
		$result->close();
		
		
		$query_select = "SELECT ic_user.user_id
						FROM ic_user
							LEFT JOIN ic_student ON ic_user.user_id = ic_student.user_id
						WHERE ic_user.user_id = '%s'
							AND ic_student.active_flag=1
							AND ic_user.active_flag = 1 ";
							
	  	$query_select = sprintf($query_select, $this->msUsername);
		
		$errorLog->LogDebug("SELECT: $query_select");
		
		if ($result = $mysqli->query($query_select)) //ES ALUMNO
		{	
			if ($result->num_rows > 0){
				$user_type="STUDENT";
			}
		}
		$result->close();
		
		if ($user_type=="STUDENT"){
		
			$query_select = "SELECT 
								ic_student.student_id, 
								ic_student.first_name, 
								ic_student.last_name1,
								ic_user.user_id, 
								ic_user.e_mail,
								ic_user.password,
								ic_user.language,
								ic_student.pictograma1,
								ic_student.pictograma2,
								ic_student.pictograma3,
								ic_student.level
							FROM ic_user 
									LEFT JOIN ic_student ON ic_user.user_id = ic_student.user_id
							WHERE ic_user.active_flag = 1 
								AND ic_student.active_flag = 1 
								AND ic_user.user_id='%s'";
								
			$query_select = sprintf($query_select, $this->msUsername);
			
			$errorLog->LogDebug("SELECT: $query_select");
			
			if ($result = $mysqli->query($query_select)) //ES ALUMNO
			{
				$row = $result->fetch_array();		
				
				
				//Acceso por pictograma, sin contraseņa.
				if ($this->msAccess=="pendrive"){
					
					if ($row["pictograma1"]!="" && $row["pictograma2"]!="" && $row["pictograma3"]!="" && $row["pictograma1"]==$_POST["pic1_sel"] && $row["pictograma2"]==$_POST["pic2_sel"] && $row["pictograma3"]==$_POST["pic3_sel"]){
						
						$_SESSION["web_student_id"] = $row["student_id"];
						$_SESSION["web_student_name"] = $row["first_name"]." ".$row["last_name1"];	
						$_SESSION["web_user_id"] = $this->msUsername;
						$_SESSION['web_user_type'] = $user_type;
						$_SESSION["web_student_email"] = $row["e_mail"];
						
						$_SESSION["web_student_language"] = $row["language"];
						
						$is_valid = 10;
					}else{
						$is_valid = 11; //ERROR
					}
					
				}else{
					if ($row["password"]!="" && $encodePwd!="" && $row["password"]==$encodePwd){
						
						$_SESSION["web_student_id"] = $row["student_id"];
						$_SESSION["web_student_name"] = $row["first_name"]." ".$row["last_name1"];	
						$_SESSION["web_user_id"] = $this->msUsername;
						$_SESSION['web_user_type'] = $user_type;
						$_SESSION["web_student_email"] = $row["e_mail"];
						
						$_SESSION["web_student_language"] = $row["language"];	
						
						$is_valid = 20;	
					}else{
						$is_valid = 21;	 //ERROR
					}
						
				
				
				}
				
				$result->close();			
			}
		}else if ($user_type=="TUTOR"){ //ES TUTOR
		
			$errorLog->LogInfo("ES TUTOR");
			
			$query_select = "SELECT 
							ic_tutor.tutor_id, 
							ic_tutor.first_name, 
							ic_tutor.last_name1,
							ic_user.user_id, 
							ic_user.e_mail,
							ic_user.password,
							ic_user.language
						FROM ic_user 
								LEFT JOIN ic_tutor ON ic_user.user_id = ic_tutor.user_id
						WHERE ic_user.active_flag = 1 
							AND ic_tutor.active_flag = 1 
							AND ic_user.user_id='%s'";
							
			$query_select = sprintf($query_select, $this->msUsername);
			
			$errorLog->LogDebug("SELECT: $query_select");
			
			if ($result = $mysqli->query($query_select)) //ES ALUMNO
			{
				$row = $result->fetch_array();
			
				//Acceso por login
				if ($this->msAccess=="login"){
					
					if ($row["password"]!="" && $encodePwd!="" && $row["password"]==$encodePwd){
						
						$_SESSION["web_tutor_id"] = $row["tutor_id"];
						$_SESSION["web_tutor_name"] = $row["first_name"]." ".$row["last_name1"];	
						$_SESSION["web_user_id"] = $this->msUsername;
						$_SESSION['web_user_type'] = $user_type;
						$_SESSION["web_tutor_email"] = $row["e_mail"];
						$_SESSION["web_tutor_language"] = $row["language"];	
						
						$is_valid = 30;	
					}else{
						$is_valid = 31;	 //ERROR
					}
				
				}
				
				$result->close();
			}
			
				
				
		}else{
			$is_valid = 40;	 //ERROR
		
		}

		return $is_valid;

	}
	
	
	function encodePassword()
	{
		return md5($this->msPassword);
	}
	
	function genRandomPassword()
	{
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<8;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}
	
	function accessRegister($description, $user_id, $company_id){
	
		global $mysqli;
		global $errorLog;
		
		if ($company_id == 0)
			$company_id = "NULL";

		$query_insert = sprintf("INSERT INTO ic_log (description, company_id, timestamp, modified_by, remoteIP, remoteHost) VALUES ( 
									'%s',
									%d,
									CURRENT_TIMESTAMP,
									'%s',
									'%s',
									'%s')", 
									$description,
									1,
									$user_id,
									$_SERVER['REMOTE_ADDR'],
									gethostbyaddr($_SERVER['REMOTE_ADDR'])
								);   
		
		$result = $mysqli->query($query_insert);
		if ($mysqli->error){
			$errorLog->LogError($mysqli->error);
		}

	}//accessRegister
	

	
	
	
}

?>
<?php
// Etiquetas para el área privada en ES

$prefijo="LBL_";
$aux="";

define($prefijo."LNG_SPANISH", "Castellano");
define($prefijo."LNG_BASQUE", "Euskara");

define($prefijo."PAG_1", "Hay ");
define($prefijo."PAG_2", " páginas");

define($prefijo."CAMPUS_GUPREST", "Gureak Akademi");
define($prefijo."AUDIO_REPLAY", "Repetir");

define($prefijo."MNU_LARGE", $aux."Grande");
define($prefijo."MNU_LARGE_ALT", $aux."Texto más grande");
define($prefijo."MNU_SMALL", $aux."Pequeña");
define($prefijo."MNU_SMALL_ALT", $aux."Texto más pequeño");
define($prefijo."MNU_COLOR", $aux."Color");
define($prefijo."MNU_COLOR_HIGH_ALT", $aux."Vista alto contraste");
define($prefijo."MNU_COLOR_NORMAL_ALT", $aux."Vista normal");
define($prefijo."MNU_COLOR_VERSION_HIGH_ALT", $aux."Versión de alto contraste");
define($prefijo."MNU_COLOR_VERSION_NORMAL_ALT", $aux."Versión normal");

define($prefijo."MNU_HELP", $aux."Ayuda");
define($prefijo."MNU_SOUND", $aux."Sonido");
define($prefijo."MNU_SOUND_OFF_ALT", $aux."Voz");
define($prefijo."MNU_SOUND_ON_ALT", $aux."Sin voz");

define($prefijo."REQUIRED_FIELD", "El campo $1 es obligatorio.");

define($prefijo."INI_TXT_01", $aux."Bienvenido/a");
define($prefijo."INI_TXT_02", $aux."¿Cómo entrar a Gureak Akademi?<br />Elige tu opción:<br /><b>A</b> Escribe tu nombre y contraseña.<br /><b>B</b> Introduce tu pendrive en el ordenador.");

define($prefijo."INI_ACC_ACCESS", $aux."Acceso");
define($prefijo."INI_ACC_USER", $aux."Usuario");
define($prefijo."INI_ACC_PASSWORD", $aux."Contraseña");
define($prefijo."INI_ACC_ENTER", $aux."Entrar");
define($prefijo."INI_ACC_FORGOT_PASSWORD", $aux."He olvidado mi contraseña");

define($prefijo."INI_OPTION_A", $aux."Opción A");
define($prefijo."INI_OPTION_B", $aux."Opción B");
define($prefijo."INI_USB_ACCESS", $aux."Entrar con pendrive");
define($prefijo."INI_LOGO_DFG", $aux."Logo Diputación Foral de Guipuzcoa");
define($prefijo."INI_LOGO_FSE", $aux."Logo Fondo Social Europeo");

define($prefijo."POD_LNG_TXT_01", $aux."Idioma cambiado.");
define($prefijo."POD_LNG_TXT_02", $aux."El idioma se ha cambiado correctamente.");

define($prefijo."PIE_CREDITOS", $aux."Créditos");


///// DENTRO DEL ÁREA PRIVADA /////
define($prefijo."ARE_PRI_VOLVER", $aux."Volver");
define($prefijo."ARE_PRI_CAMBIAR", $aux."Cambiar");
define($prefijo."ARE_PRI_DESCARGAR", $aux."Descargar");
define($prefijo."ARE_PRI_ENLACE", $aux."Enlace");
define($prefijo."ARE_PRI_ESTAS", $aux."Estás en:");
define($prefijo."ARE_PRI_INICIO", $aux."Inicio");
define($prefijo."ARE_PRI_CURSOS", $aux."Cursos");
define($prefijo."ARE_PRI_JUEGOS", $aux."Juegos");
define($prefijo."ARE_PRI_BIBLIOTECA", $aux."Biblioteca");
define($prefijo."ARE_PRI_CAFETERIA", $aux."Cafetería");
define($prefijo."ARE_PRI_NOTICIAS", $aux."Noticias");
define($prefijo."ARE_PRI_TUTORES", $aux."Tutores");
define($prefijo."ARE_PRI_COMPRENDER", $aux."Comprender");
define($prefijo."ARE_PRI_MIS_CURSOS", $aux."Mis cursos");
define($prefijo."ARE_PRI_MIS_CATALOGO", $aux."Catálogo");
define($prefijo."ARE_PRI_HAY_PAGINAS", $aux."Hay <strong>$1</strong> páginas");
define($prefijo."ARE_PRI_HAY_PAGINA", $aux."Hay <strong>$1</strong> página");
define($prefijo."ARE_PRI_ANTERIOR", $aux."Anterior");
define($prefijo."ARE_PRI_SIGUIENTE", $aux."Siguiente");
define($prefijo."ARE_PRI_PERFIL", $aux."Perfil");
define($prefijo."ARE_PRI_MI_PERFIL", $aux."Mi perfil");
define($prefijo."ARE_PRI_GESTION_FORMACION", $aux."Gestión Formación");
define($prefijo."ARE_PRI_GESTION_RECURSOS", $aux."Recursos");
define($prefijo."ARE_PRI_GESTION_BACKEND", $aux."Backend");

define($prefijo."ARE_PRI_MNU_INICIO_TITLE", $aux."Pantalla de inicio.");
define($prefijo."ARE_PRI_MNU_BIBLIOTECA_TITLE", $aux."Para leer, ver fotos y vídeos.");
define($prefijo."ARE_PRI_MNU_CURSOS_TITLE", $aux."Para entrar a tus cursos o consultar otros.");
define($prefijo."ARE_PRI_MNU_CAFETERIA_TITLE", $aux."Para leer el periodico, escuchar la radio, ver la televisión y contactar con tus compañeros y compañeras.");
define($prefijo."ARE_PRI_MNU_JUEGOS_TITLE", $aux."Para divertirte jugando.");
define($prefijo."ARE_PRI_MNU_NOTICIAS_TITLE", $aux."Para consultar noticias sobre Gureak Akademi.");
define($prefijo."ARE_PRI_MNU_TUTORES_TITLE", $aux."Espacio para los responsables.");

define($prefijo."ARE_PRI_MNU_INICIO", $aux."inicio");
define($prefijo."ARE_PRI_MNU_BIBLIOTECA", $aux."biblioteca");
define($prefijo."ARE_PRI_MNU_CURSOS", $aux."cursos");
define($prefijo."ARE_PRI_MNU_CAFETERIA", $aux."cafetería");
define($prefijo."ARE_PRI_MNU_JUEGOS", $aux."juegos");
define($prefijo."ARE_PRI_MNU_NOTICIAS", $aux."noticias");
define($prefijo."ARE_PRI_MNU_TUTORES", $aux."tutores");

define($prefijo."ARE_PRI_MNU_SALIR", $aux."Salir de Gureak Akademi");
define($prefijo."ARE_PRI_MNU_DESCONECTAR", $aux."Desconectar");

define($prefijo."ARE_PRI_INI_NOTICIAS", $aux."noticias");
define($prefijo."ARE_PRI_INI_LEER_NOTICIAS", $aux."leer todas las noticias");

define($prefijo."ARE_PRI_PRF_TEXTO_PERFIL", $aux."Desde aquí podrás <b>cambiar</b> tu <b>contraseña</b> y otros datos de tu <b>perfil</b>.");
define($prefijo."ARE_PRI_PRF_NOMBRE", $aux."Nombre");
define($prefijo."ARE_PRI_PRF_APELLIDO1", $aux."1er Apellido");
define($prefijo."ARE_PRI_PRF_APELLIDO2", $aux."2o Apellido");
define($prefijo."ARE_PRI_PRF_CONTACTO", $aux."Mail de contacto");
define($prefijo."ARE_PRI_PRF_CONTR_ANTERIOR", $aux."Contraseña anterior");
define($prefijo."ARE_PRI_PRF_CONTR_NUEVA", $aux."Nueva contraseña");
define($prefijo."ARE_PRI_PRF_CONTR_REPETIR", $aux."Repetir contraseña");

define($prefijo."ARE_PRI_PRF_EMAIL_TEXT01", "Debes tener un correo electrónico.");
define($prefijo."ARE_PRI_PRF_MSG_CONTR0", "La contraseña anterior no es correcta.");
define($prefijo."ARE_PRI_PRF_MSG_CONTR1", "La Nueva contraseña y su Confirmación deben ser iguales.");
define($prefijo."ARE_PRI_PRF_MSG_CONTR2", "Rellena la Nueva contraseña y su Confirmación.");

define($prefijo."ARE_PRI_RCO_RECORDAD_CONTRASENA", "Recordar contraseña");
define($prefijo."ARE_PRI_RCO_ESCRIBE", "Escribe tu dirección de <b>correo electrónico</b>, y a continuación haz click en el botón <b>Enviar</b>.");
define($prefijo."ARE_PRI_RCO_BREVE", "Pronto recibirás en tu correo personal las instrucciones para cambiar tu contraseña.");
define($prefijo."ARE_PRI_RCO_CONTACTENOS", "Si no tienes correo electrónico o tienes algún otro problema para acceder a Gureak Akademi, contacta con nosotros en el <b>943 000 795</b> o por email en: <b>infoakademi@grupogureak.com</b>.");
define($prefijo."ARE_PRI_RCO_CORREO", "Correo electrónico");
define($prefijo."ARE_PRI_RCO_OBLIGATORIO", "campo obligatorio");
define($prefijo."ARE_PRI_RCO_ENVIAR", "Enviar");
define($prefijo."ARE_PRI_RCO_CORREO_VACIO", "Escribe tu correo electrónico.");
define($prefijo."ARE_PRI_RCO_CORREO_INCORRECTO", "El correo electrónico no es correcto.");
define($prefijo."ARE_PRI_RCO_MENSAJE_ENVIADO", "Mensaje enviado a ");
define($prefijo."ARE_PRI_RCO_MENSAJE_ENVIADO_INFO", "Tu mensaje se ha enviado correctamente.<br />En breve recibirás en tu correo personal instrucciones para acceder a Gureak Akademi.");

define($prefijo."ARE_PRI_RCO_MENSAJE_ASUNTO", "Gureak Akademi te ayuda a recuperar la contraseña");
define($prefijo."ARE_PRI_RCO_MENSAJE_INSTRUCCIONES", "Estimado compañero/a,\nhas solicitado recuperar tu contraseña.\n\nSi no solicitaste recuperar tu contraseña no hagas caso de este mensaje, en caso contrario sigue las siguientes instrucciones.\n\n1-Haz click sobre el siguiente link $1.\n\n2-Aparecerá una página donde deberás escribir la nueva contraseña y su confirmación.\n\n3- Haz click en enviar y recibirás un correo confirmando tu nueva contraseña.\n\n4- Ya podrás acceder a Gureak Akademi con tu nueva contraseña.\n\n");
define($prefijo."ARE_PRI_RCO_MENSAJE_FIRMA", "Atentamente,\nGureak Akademi");
define($prefijo."ARE_PRI_RCO_PROCESO", "Proceso de recuperación de la contraseña");
define($prefijo."ARE_PRI_RCO_PROCESO_1", "Estimado compañero/a, escribe tu nueva contraseña y su confirmación, seguidamente haz click en Cambiar.");

//rmartin 11-may-2011: continuar aquí.
define($prefijo."ARE_PRI_RCO_MENSAJE2_ASUNTO", "Gureak Akademi nueva contraseña de acceso");
define($prefijo."ARE_PRI_RCO_MENSAJE2_INSTRUCCIONES", "Estimado compañero/a,\nse has cambiado correctamente tu contraseña.\n\nTus datos de acceso son:\n\nUsuario: $1\nContraseña:$2\n\n");
define($prefijo."ARE_PRI_RCO_MENSAJE2_FIRMA", "Atentamente,\nGureak Akademi");

define($prefijo."ARE_PRI_AVI_INTENTALO", "Inténtalo de nuevo");
define($prefijo."ARE_PRI_AVI_INTENTALO_MENSAJE", "El usuario o la contraseña no son correctos.");
define($prefijo."ARE_PRI_AVI_ACCESO_INCORRECTO", "Acceso incorrecto");
define($prefijo."ARE_PRI_AVI_INTENTALO_CONTACTA", "Contacta con tu responsable.");


define($prefijo."ARE_PRI_KRI_CUAL_CONTRASENA", $aux."¿Cuál es tu contraseña?");
define($prefijo."ARE_PRI_KRI_PRIMERO_PINCHA", $aux."Primero, <b>haz click sobre tus imágenes</b>");
define($prefijo."ARE_PRI_KRI_PROBLEMA_RESPONSABLE", $aux."Si tienes algún problema, avisa a tu responsable.");
define($prefijo."ARE_PRI_KRI_COCHE", $aux."coche");
define($prefijo."ARE_PRI_KRI_CAMION", $aux."camión");
define($prefijo."ARE_PRI_KRI_AVION", $aux."avión");
define($prefijo."ARE_PRI_KRI_BICI", $aux."bici");
define($prefijo."ARE_PRI_KRI_VELERO", $aux."velero");
define($prefijo."ARE_PRI_KRI_VACIO", $aux."vacío");
define($prefijo."ARE_PRI_KRI_SANDIA", $aux."sandía");
define($prefijo."ARE_PRI_KRI_PINA", $aux."piña");
define($prefijo."ARE_PRI_KRI_CEREZAS", $aux."cerezas");
define($prefijo."ARE_PRI_KRI_PLATANO", $aux."plátano");
define($prefijo."ARE_PRI_KRI_UVAS", $aux."uvas");
define($prefijo."ARE_PRI_KRI_ELEFANTE", $aux."elefante");
define($prefijo."ARE_PRI_KRI_JIRAFA", $aux."jirafa");
define($prefijo."ARE_PRI_KRI_TORTUGA", $aux."tortuga");
define($prefijo."ARE_PRI_KRI_GALLO", $aux."gallo");
define($prefijo."ARE_PRI_KRI_SERPIENTE", $aux."serpiente");
define($prefijo."ARE_PRI_KRI_ATENCION", $aux."Atención");
define($prefijo."ARE_PRI_KRI_RECUERDA", $aux."Recuerda que tienes que hacer click sobre tus imágenes.");

define($prefijo."ARE_PRI_KRI_ERROR", $aux."Error");
define($prefijo."ARE_PRI_KRI_IMAGENES_NO_CORRECTAS", $aux."Las imágenes no son correctas.");
define($prefijo."ARE_PRI_KRI_IMAGENES_NO_CORRECTAS2", $aux."Las imágenes<br/> no son correctas.");
define($prefijo."ARE_PRI_KRI_AVISA_RESPONSABLE", $aux."Avisa a tu responsable.");

define($prefijo."ARE_PRI_NOT_NOTICIAS", $aux."Noticias");
define($prefijo."ARE_PRI_NOT_LISTADO_NOTICIAS", $aux."Todas las noticias");

define($prefijo."ARE_PRI_GAM_JUEGOS", $aux."Juegos");
define($prefijo."ARE_PRI_GAM_JUEGOS_INFO", $aux."Estás en la pantalla Juegos. Aquí podrás divertirte jugando.<br />Puedes acceder a cada uno de los sitios haciendo click en la imagen que te interese. <b>Elige tu opción:</b>");
define($prefijo."ARE_PRI_GAM_JUEGOS_ACCION", $aux."Acción");
define($prefijo."ARE_PRI_GAM_JUEGOS_PENSAR", $aux."Pensar");
define($prefijo."ARE_PRI_GAM_JUEGOS_MUCHO", $aux."Mucho");
define($prefijo."ARE_PRI_GAM_JUEGOS_LIST", $aux."Listado de juegos:");
define($prefijo."ARE_PRI_GAM_JUEGOS_TAMANO", $aux."Tamaño");
define($prefijo."ARE_PRI_GAM_JUEGOS_JUGAR", $aux."Jugar");
define($prefijo."ARE_PRI_GAM_JUEGOS_OBJETIVO", $aux."Descripción:");

define($prefijo."ARE_PRI_CAF_CAFETERIA", $aux."Cafetería");
define($prefijo."ARE_PRI_CAF_CAFETERIA_INFO", $aux."Estás en la pantalla Cafetería. Aquí podrás contactar con tus compañeros y compañeras. Leer el periodico. Ver la televisión. Escuchar la radio. Y muchas cosas más.");
define($prefijo."ARE_PRI_CAF_CAFETERIA_LISTADO", $aux."Listado:");

define($prefijo."ARE_PRI_CAF_CAFETERIA_INFO_APRENDER", $aux."Puedes acceder a cada uno de los sitios haciendo click en la imagen que te interese. <b>Elige tu opción:");

define($prefijo."ARE_PRI_CRS_MIS_CURSOS", $aux."Mis Cursos");
define($prefijo."ARE_PRI_CRS_TUS_CURSOS", $aux."Estos son tus cursos:");
define($prefijo."ARE_PRI_CRS_VER_CATALOGO", $aux."ver catálogo");
define($prefijo."ARE_PRI_CRS_CATALOGO_CURSOS", $aux."Catálogo de cursos");
define($prefijo."ARE_PRI_CRS_TIPOS_CURSOS", $aux."Tienes 3 tipos de cursos. <b>Elige tu opción:</b>");
define($prefijo."ARE_PRI_CRS_FRM_PRELABORAL", $aux."Formación Prelaboral");
define($prefijo."ARE_PRI_CRS_FRM_PROFESIONAL", $aux."Formación Profesional");
define($prefijo."ARE_PRI_CRS_FRM_EXTRALABORAL", $aux."Formación Extralaboral");
define($prefijo."ARE_PRI_CRS_LISTADO_CURSOS", $aux."Listado de cursos:");
define($prefijo."ARE_PRI_CRS_OBJETIVO", $aux."Objetivo");
define($prefijo."ARE_PRI_CRS_PROGRAMA", $aux."Programa");
define($prefijo."ARE_PRI_CRS_DURACION", $aux."Duración");
define($prefijo."ARE_PRI_CRS_IDIOMA", $aux."Idioma");
define($prefijo."ARE_PRI_CRS_RESPONSABLE", $aux."Responsable");
define($prefijo."ARE_PRI_CRS_TIPO_INSCRIPCION", $aux."Tipo de inscripción");

define($prefijo."ARE_PRI_CRS_INSCRIBIRSE", $aux."Apuntarse");
define($prefijo."ARE_PRI_CRS_INSCRIBIRSE_TEXT1", $aux."Acabas de apuntarte en el curso de ");

define($prefijo."ARE_PRI_CRS_NO_HAY_CURSOS", $aux."Actualmente no se han encontrado cursos en esta categoría");

define($prefijo."ARE_PRI_BIB_BIBLIOTECA", $aux."Biblioteca");
define($prefijo."ARE_PRI_BIB_BIBLIOTECA_LIST", $aux."Listado:");
define($prefijo."ARE_PRI_BIB_BIBLIOTECA_INFO", $aux."Estás en la pantalla Biblioteca. Aquí podrás leer libros. Ver fotos y vídeos. También podrás entrar en páginas interesantes.<br />Puedes acceder a cada uno de los sitios haciendo click en la imagen que te interese. <b>Elige tu opción:</b>");

define($prefijo."ARE_PRI_TUT_TUTORES_OP", $aux."Opción");
define($prefijo."ARE_PRI_TUT_TUTORES_TUT", $aux."Tutores");
define($prefijo."ARE_PRI_TUT_TUTORES_TUT_INFO", $aux."Estás en la pantalla tutores, puedes acceder a cada uno de los recursos haciendo click sobre la imagen que te interese. <b>Elige tu opción:</b>");
define($prefijo."ARE_PRI_TUT_TUTORES_REP", $aux."Repositorio");
define($prefijo."ARE_PRI_TUT_TUTORES_BUS", $aux."Búsqueda Simple");
define($prefijo."ARE_PRI_TUT_TUTORES_BUSAV", $aux."Búsqueda Avanzada");
define($prefijo."ARE_PRI_TUT_TUTORES_BUSCLAVE", $aux."Búsqueda Palabras Clave");
define($prefijo."ARE_PRI_TUT_TUTORES_MIS_RECURSOS", $aux."Mis Recursos");
define($prefijo."ARE_PRI_TUT_TUTORES_SUB_RECURSOS", $aux."Subir Recursos");
define($prefijo."ARE_PRI_TUT_TUTORES_SEARCH", $aux."Resultados de la busqueda:");
define($prefijo."ARE_PRI_TUT_TUTORES_DET", $aux."Detalles");

define($prefijo."ARE_PRI_TUT_TUTORES_FICHA_1", $aux."<b>Creador:</b>");
define($prefijo."ARE_PRI_TUT_TUTORES_FICHA_2", $aux."<b>Fecha de publicación:</b>");
define($prefijo."ARE_PRI_TUT_TUTORES_FICHA_3", $aux."<b>Recurso:</b>");
define($prefijo."ARE_PRI_TUT_TUTORES_FICHA_4", $aux."<b>Contenido:</b>");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_1", $aux."General");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_2", $aux."Descripción");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_3", $aux."Publicación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_4", $aux."Técnica");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_5", $aux."Uso educativo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_6", $aux."Propósito");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_0", $aux."Creador");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_1", $aux."Título");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_2", $aux."Idioma");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_3", $aux."Formato");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_4", $aux."Localización");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_5", $aux."Tipo de interactividad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_6", $aux."Tipo de recurso");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_7", $aux."Tipo de contenido");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_8", $aux."Creador");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_9", $aux."Fecha de");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_10", $aux."hasta");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_11", $aux."Rol de usuario");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_12", $aux."Contexto");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_TXT_13", $aux."Dificultad");



define($prefijo."ARE_PRI_TUT_TUTORES_ADV_FECHA", $aux."Fecha");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_SELEC_OPCIONES", $aux."Selecciona una de las opciones");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_SELEC_IDIOMA", $aux."Selecciona un Idioma");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_SELEC_NIVEL", $aux."Selecciona un Nivel");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_SELEC_VERSION", $aux."Selecciona una Versión");


define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_1", $aux."Euskera");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_2", $aux."Castellano");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_3", $aux."Inglés");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_4", $aux."Lenguaje signos");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_5", $aux."Otros");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_6", $aux."Libro");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_7", $aux."Juego serio");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_8", $aux."Material diverso");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_9", $aux."Expositiva");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_10", $aux."Activa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_11", $aux."Combinada");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_12", $aux."Imagen");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_13", $aux."Vídeo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_14", $aux."Audio");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_15", $aux."Animación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_16", $aux."Gráfico");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_17", $aux."Actividad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_18", $aux."Ejercicio");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_19", $aux."Evaluación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_20", $aux."Cuestionario");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_21", $aux."Texto");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_22", $aux."Curso");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_23", $aux."Tutorial");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_24", $aux."Taller");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_25", $aux."Píldora");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_26", $aux."Tutor");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_27", $aux."Objeto de aprendizaje");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_28", $aux."Unidad didáctica");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_29", $aux."Individual");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_30", $aux."Grupal");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_31", $aux."MOI");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_32", $aux."MOD");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_33", $aux."Distancia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_34", $aux."Presencial");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_35", $aux."Semipresencial");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_36", $aux."Inicial");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_37", $aux."Medio");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_VALORES_38", $aux."Avanzado");

define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_0", $aux."Personales");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_1", $aux."Capacidades físicas");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_2", $aux."Visión");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_3", $aux."Audición");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_4", $aux."Habla");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_5", $aux."Capacidad motora");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_6", $aux."Motricidad fina");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_7", $aux."Motricidad gruesa y fuerza");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_8", $aux."Estado de salud");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_9", $aux."Capacidades intelectuales");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_10", $aux."Atención");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_11", $aux."Fatiga intelectual");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_12", $aux."Concentración");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_13", $aux."Percepción");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_14", $aux."Memoria (sensorial)");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_15", $aux."Orientación espacio/temporal");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_16", $aux."Orientación espacial");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_17", $aux."Orientación temporal");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_18", $aux."Ritmo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_19", $aux."Cuidado personal");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_20", $aux."Higiene y apariencia personal");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_21", $aux."Salud y seguridad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_22", $aux."Mantenimiento de la salud");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_23", $aux."Autorregulación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_24", $aux."Control emocional");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_25", $aux."Discreción");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_26", $aux."Confidencialidad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_27", $aux."Habilidades académicas funcionales");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_28", $aux."Utilizar y relacionar números");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_29", $aux."Cálculo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_30", $aux."Resolución de problemas");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_31", $aux."Competencia Digital");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_32", $aux."Actitudes generales ante las NTIC");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_33", $aux."Conocimientos digitales");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_34", $aux."Tratamiento multimedia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_35", $aux."Utilización de la comunidad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_36", $aux."Relación con la comunidad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_37", $aux."Utilización del transporte");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_38", $aux."Uso de dinero");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_39", $aux."Vida en el hogar");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_40", $aux."Búsqueda de empleo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_41", $aux."Técnicas activas de búsqueda de empleo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_42", $aux."Socio-laborales");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_43", $aux."Competencias sociales e interpersonales en el ámbito laboral");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_44", $aux."Comunicación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_45", $aux."Lenguaje");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_46", $aux."Interpretación información");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_47", $aux."Transmisión de información");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_48", $aux."Empatía");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_49", $aux."Asertividad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_50", $aux."Trabajar en equipo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_51", $aux."Liderazgo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_52", $aux."Integración");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_53", $aux."Compañerismo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_54", $aux."Autocontrol");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_55", $aux."Pautas comportamiento");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_56", $aux."Resolución de problemas");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_57", $aux."Tolerancia al stress");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_58", $aux."Tolerancia a la crítica");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_59", $aux."Derechos y Obligaciones");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_60", $aux."Cumplimiento normas");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_61", $aux."Asistencia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_62", $aux."Puntualidad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_63", $aux."Derecho del trabajo y sindical");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_64", $aux."Competencias metodológicas o actitudes básicas");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_65", $aux."Adaptación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_66", $aux."Flexibilidad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_67", $aux."Adaptabilidad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_68", $aux."Aprendizaje");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_69", $aux."Responsabilidad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_70", $aux."Compromiso");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_71", $aux."Implicación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_72", $aux."Autocrítica");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_73", $aux."Productividad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_74", $aux."Ejecución");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_75", $aux."Ritmo");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_76", $aux."Perseverancia");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_77", $aux."Iniciativa y Autonomía");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_78", $aux."Toma de decisiones");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_79", $aux."Análisis");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_80", $aux."Planificación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_81", $aux."Organización");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_82", $aux."Emprendizaje");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_83", $aux."Creatividad");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_84", $aux."Apariencia y Autocuidado");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_85", $aux."Autoestima y Confianza");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_86", $aux."Motivación");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_87", $aux."Reflexión");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_88", $aux."Posicionamiento en la empresa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_89", $aux."Identificación con la empresa");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_90", $aux."Imagen");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_91", $aux."Visión cliente");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_92", $aux."Orientación al cliente");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_93", $aux."Técnico profesionales");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_94", $aux."Si");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_95", $aux."No");
define($prefijo."ARE_PRI_TUT_TUTORES_ADV_OPCIONES_LISTA_VALORES_96", $aux."Apariencia y Autocuidado");

define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_1", $aux."¡Fichero subido con éxito! <br>");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_2", $aux."¡Error en el envió del fichero al servidor local! <br>");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_3", $aux."¡Fichero enviado con éxito al gestor documental! <br>");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_4", $aux."¡Fallo al enviar el fichero al gestor documental! <br>");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_5", $aux."¡Fallo esta subiendo el mismo fichero en la misma ruta de destino! <br>");

define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_OTRO", $aux."Subir otro fichero");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_RECORDATORIO_1", $aux."<strong>NOTA:</strong> Recuerda que los campos señalados con un asterisco");
define($prefijo."ARE_PRI_TUT_TUTORES_UPLOAD_RECORDATORIO_2", $aux."son obligatorios de rellenar");

define($prefijo."ARE_PRI_TUT_TUTORES_NIVEL", $aux."Nivel de Agregación");
define($prefijo."ARE_PRI_TUT_TUTORES_NIVEL_ACCESO", $aux."Nivel de Acceso");
define($prefijo."ARE_PRI_TUT_TUTORES_VERSION", $aux."Versión");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO", $aux."Archivo");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_MAX", $aux."25MB Tamaño máximo");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_NOTAS", $aux."Notas de Instalación");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_ORIENTACION", $aux."Orientaciones didácticas");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_REL_RECURSO", $aux."Relación Recurso");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_REL_REQ", $aux."Requerimiento");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_REL_TIEMPO_APRENDIZAJE", $aux."Tiempo de aprendizaje");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_CONOC", $aux."Conocimiento previo");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_COPY", $aux."Copyright y otras restricciones");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_COPY_1", $aux."Derechos de terceros");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_COPY_2", $aux."Derechos reservados por negocio");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_TIPO", $aux."Tipo");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_1", $aux."Requiere");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_2", $aux."Es requerido por");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_3", $aux."Tiene parte");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_4", $aux."Es parte");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_IDENT", $aux."Identificación");
define($prefijo."ARE_PRI_TUT_TUTORES_ARCHIVO_RELREC_DESC", $aux."Descripción");

define($prefijo."ARE_PRI_LOCALIZACION", $aux."<b>Localización: </b>");

?>
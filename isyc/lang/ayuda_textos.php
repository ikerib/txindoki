<?php
// Tratando de poner un poco de orden.

$texto_inicio_acceso_es =
"<p>Estás en la pantalla de entrada a Gureak Akademi.</p>
<p>Puedes entrar de dos formas. Si sabes tu nombre y contraseña escríbelos en el recuadro azul.
Si tienes tu pendrive mételo en el ordenador como se indica en el recuadro verde.</p>";
$texto_inicio_acceso_eu = "<p>Gureak Akademira sartzeko pantailan zaude.</p>
<p>Bi modutara sar zaitezke. Zure izena eta pasahitza ezagutzen badituzu, idatz itzazu koadro urdinean.
Zure pendrivea baduzu, sar ezazu ordenagailuan koadro berdean adierazten den bezala.</p>";

$texto_inicio_kriptograma_es =
"<p>Ahora tienes que recordar tus 3 imágenes.</p>
<p>Haz click sobre 1 de las imágenes en naranja (medios de transporte).</p>
<p>Haz click sobre 1 de las imágenes en azul (frutas).</p>
<p>Haz click sobre 1 de las imágenes en verde (verde).</p>";
$texto_inicio_kriptograma_eu = "<p>Orain zure 3 irudiak gogoratu behar dituzu.</p>
<p>Klikatu laranja kolorez dauden irudietako batean (garraiobideak).</p>
<p>Klikatu urdin kolorez dauden irudietako batean (fruituak).</p>
<p>Klikatu berde kolorez dauden irudietako batean (verde).</p>";

$texto_inicio_campus_es =
"<p>Estás en la pantalla de inicio de Gureak Akademi.</p>
<p>Para saber que hay dentro de cada edificio, pon el ratón encima.</p>
<p>Si quieres entrar, haz click sobre el edificio.</p>";
$texto_inicio_campus_eu = "<p>Gureak Akademira sartzeko pantailan zaude.</p>
<p>Eraikuntza bakoitzaren barruan dagoena ezagutzeko jarri sagua gainean.</p>
<p>Sartu nahi baduzu klikatu eraikuntzaren gainean.</p>";

$texto_inicio_perfil_es =
"<p>Para cambiar tus datos, vuelve a escribirlos.</p>
<p>Para cambiar la contraseña, escribe la antigua contraseña y dos veces la nueva.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong>.</p>";
$texto_inicio_perfil_eu = "<p>Zure datuak aldatzeko idatz itzazu berriro.</p>
<p>Pasahitza aldatzeko, idatzi lehenik zaharra eta ondoren berria 2 aldiz.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong>.</p>";

$texto_cursos_es =
"<p>Estas en la pantalla de cursos. Haz click sobre el lápiz para entrar en tus cursos.
Haz click sobre el cuaderno para ver todos los cursos de Gureak Akademi.</p>";
$texto_cursos_eu = "<p>Ikastaroen pantailan zaude. Klikatu arkatzaren gainean zure ikastaroetan sartzeko.
Kuadernoaren gainean klikatu Gureak Akademiko ikastaro guztiak ikusteko.</p>";

$texto_cursos_miscursos_es ="<p>Estás en la pantalla de tus cursos.</p>
<p>Para entrar haz click sobre un curso. En cada curso tienes una barra.
El color verde te dice lo que has hecho del curso. Y el color blanco te dice lo que te falta.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_cursos_miscursos_eu = "<p>Zure ikastaroen pantailan zaude.</p>
<p>Sartzeko klikatu ikastaro baten gainean. Ikastaro bakoitzean barra bat duzu.
Kolore berdeak ikastaroan eginda duzuna adierazten dizu. Eta kolore zuriak falta zaizuna adierazten dizu.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_cursos_catalogo_es =
"<p>Estas en el catálogo. Haz click en una imagen para ver los cursos.</p>
<p><strong>Formación prelaboral:</strong> Prepararte para trabajar.</p>
<p><strong>Formación profesional:</strong> Mejorar en tu trabajo.</p>
<p><strong>Formación extralaboral:</strong> Aprender otras cosas.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_cursos_catalogo_eu = "<p>Katalogoan zaude. Klikatu irudi baten gainean ikastaroak ikusteko.</p>
<p><strong>Laneratze prestakuntza:</strong> Lan egiteko prestatuko zaitu.</p>
<p><strong>Lanbide-Heziketa:</strong> Zure lanean hobetu.</p>
<p><strong>Lanaz kanpoko prestakuntza:</strong> Beste gauza batzuek ikasi.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_cursos_cat_es =
"<p>Haz click en un curso para ver más información.</p>
<p>Las medallas te dicen si el curso es fácil o difícil:</p>
<p><strong>1 medalla:</strong> Fácil.</p>
<p><strong>2 medallas:</strong> Medio.</p>
<p><strong>3 medallas:</strong> Difícil.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_cursos_cat_eu = "<p>Klikatu ikastaro baten gainean informazio gehiago ikusteko.</p>
<p>Dominek estan dizute ikastaroa erraza edo zaila den:</p>
<p><strong>domina 1:</strong> Erraza.</p>
<p><strong>2 domina:</strong> Ertaina.</p>
<p><strong>3 domina:</strong> Zaila.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_cursos_ficha_inscripcion_es =
'<p>Para apuntarte al curso haz click sobre el botón "Inscribirse".</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>';
$texto_cursos_ficha_inscripcion_eu = "<p>Ikastaroan izena emateko klikatu 'Izena eman' botoiaren gainean.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_biblioteca_es =
"<p>Para leer libros, haz click en <strong>Documentos</strong></p>
<p>Para ver imágenes, haz click en <strong>Fotos</strong></p>
<p>Para ver y escuchar películas, haz click en <strong>Vídeos</strong></p>
<p>Para entrar en páginas interesantes, haz click en <strong>Webs</strong></p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_biblioteca_eu =
"<p>Liburuak irakurtzeko, klikatu <strong>Dokumentuak</strong></p>
<p>Imajinak ikusteko klikatu <strong>Argazkiak</strong></p>
<p>Filmak entzun eta ikusteko klikatu <strong>Bideoaks</strong></p>
<p>Orrialde interesgarrietan sartzeko klikatu <strong>Web-ak</strong></p>
<p>Aurreko pantailara joateko klikatu botoiaren gainean <strong>Itzuli</strong></p>";

$texto_biblioteca_29_es =
"<p>Haz click en una imagen para entrar.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_biblioteca_29_eu =
"<p>Sartzeko klikatu irudi baten gainean.</p>
<p>Aurreko pantailara joateko klikatu botoiaren gainean <strong>Itzuli</strong></p>";

$texto_biblioteca_docs_listado_es =
"<p>Haz click encima de una opción para ver sus datos.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_biblioteca_docs_listado_eu =
"<p>Klikatu aukera baten gainea datuak ikusteko.</p>
<p>Aurreko pantailara joateko klikatu botoiaren gainean <strong>Itzuli</strong></p>";

$texto_biblioteca_ficha_es =
"<p>Para descargar el archivo, haz click encima del botón Descargar.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_biblioteca_ficha_eu = "<p>Artxiboa deskargatzeko klikatu Deskargatu botoiaren gainean.</p>
<p>Aurreko pantailara joateko klikatu botoiaren gainean <strong>Itzuli</strong></p>";

$texto_cafeteria_es =
"<p>Para ver la televisión, haz click en <strong>TV</strong></p>
<p>Para escuchar la radio, haz click en <strong>Radio</strong></p>
<p>Para hablar con tus compañeros y compañeras, haz click en <strong>Redes Sociales</strong></p>
<p>Para leer las últimas noticias, haz click en <strong>Actualidad</strong></p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_cafeteria_eu = "<p>Telebista ikusteko klikatu <strong>TB</strong></p>
<p>Irratia entzuteko klikatu <strong>Irratia</strong></p>
<p>Zure lankideekin hitz egiteko klikatu <strong>Sare sozialak</strong></p>
<p>Azken berriak irakurtzeko klikatu <strong>Aktualitatea</strong></p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_cafeteria_listado_es =
"<p>Haz click en una opción para ver más información.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_cafeteria_listado_eu = "<p>Klikatu aukera baten gainean informazioa gehiago ikusteko.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_cafeteria_ficha_es =
"<p>Para descargar el archivo, haz click encima del botón Descargar.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_cafeteria_ficha_eu = "<p>Artxiboa deskargatzeko klikatu Deskargatu botoiaren gainean.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_juegos_es =
"<p>Para divertirte con juegos de pensar, haz click en el <strong>interrogante</strong>.</p>
<p>Para divertirte con juegos de acción, haz click en el <strong>corredor</strong>.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_juegos_eu = "<p>Pentsatzeko jokoekin ondo pasatzeko klikatu <strong>galdera ikurra</strong>.</p>
<p>Akzioko jokoekin ondo pasatzeko klikatu <strong>korrikalaria</strong>.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_juegos_listado_es =
"<p>Haz click en el juego que quieres jugar.</p>
<p>En cada pantalla se ven tres juegos. Si quieres ver más juegos, haz click en los números que están entre las flechas.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_juegos_listado_eu = "<p>Klikatu jolastu nahi duzun jokoaren gainean.</p>
<p>Pantaila bakoitzean hiru joko ikusten dira. Gehiago ikusi nahi badituzu klikatu gezien arteko zenbakietan.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_juegos_ficha_es =
"<p>Para descargar el archivo, haz click en el botón Descargar.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_juegos_ficha_eu = "<p>Artxiboa deskargatzeko klikatu Deskargatu botoiaren gainean.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_noticias_es =
"<p>Estas en la pantalla de noticias. Haz click en una noticia para leerla.</p>
<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_noticias_eu = "<p>Artxiboa deskargatzeko klikatu Deskargatu botoiaren gainean.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_noticias_ficha_es =
"<p>Para ir a la pantalla anterior, haz click en el botón <strong>Volver</strong></p>";
$texto_noticias_ficha_eu = "<p>Berrien pantailan zaude. Klikatu berri baten gainean irakurtzeko.</p>
<p>Aurreko pantailara joateko klikatu <strong>Itzuli</strong></p>";

$texto_tutores_es =
"<p>Estas en la pantalla de tutores, puedes acceder a cada uno de los recursos pulsando sobre la imagen que te interese.</p>";
$texto_tutores_eu = "<p>Tutoreen pantailan zaude.Errekurtso bakoitzera sar zaitezke interesatzen zaizun irudian klikatuz.</p>";

$texto_tutores_recursos_es =
"<p>Utiliza la opción A para realizar búsquedas simples o avanzadas.
La Opción B, te permite ver tus recursos subidos a la plataforma.
La Opción C, te permite subir nuevos recursos.</p>";
$texto_tutores_recursos_eu = "<p>Erabili A aukera bilaketa errazak edo aurreratuak egiteko.
B aukerak plataformara igotako zure errekurtsoak ikusten utziko dizu.
C aukerak errekurtso berriak igotzen utziko dizu.</p>";

$texto_tutores_avanzada_es =
"<p>Rellena los campos de búsqueda y haz click sobre Buscar.</p>";
$texto_tutores_avanzada_eu = "<p>Bete itzazu bilaketa eremuak eta klikatu Bilatu.</p>";

$texto_tutores_mis_rec_es =
"<p>Estos son los recursos que has subido a la plataforma. Haz click sobre un recurso para ver sus datos.</p>";
$texto_tutores_mis_rec_eu = "<p>Hauek dira plataformara igo dituzun errekurtsoak. Klikatu errekurtso baten gainean datuak ikusteko.</p>";

$texto_tutores_ficha_es =
"<p>Para descargar este recurso, haz click sobre el botón Descargar.</p>";
$texto_tutores_ficha_eu = "<p>Errekurtso hau deskargatzeko klikatu Deskargatu botoiaren gainean.</p>";

$texto_tutores_subir_es =
"<p>Rellena los campos para subir un recurso. Recuerda que debes seleccionar un archivo para subir y que los campos marcados con * son obligatorios.</p>";
$texto_tutores_subir_eu = "<p>Bete eremuak errekurtso bat igotzeko. Gogoratu artxibo bat aukeratu behar duzula igotzeko eta * duten eremuak derrigorrezkoak direla.</p>";

<?php session_start(); ?>
<?php
	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/database/DB_Connection.php");
	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/portal/AuthenticationWeb.php");
	
	//VARIOS TIPOS DE ACCESO.
	//1. DESDE UN PENDRIVE PASAN POR GET EL USER Y DEBE SELECCIONAR 3 PICTOGRAMAS (ALUMNOS)
	//2. ACCESO NORMAL, PARA TUTORES Y ALUMNOS CON USUARIO Y CONTRASEŅA.
	//3. ALUMNOS DE NIVEL 4. LOS CUALES ACCEDEN DIRECTAMENTE AL CURSO ASOCIADO EN SU PERFIL.
	
	$host  = $_SERVER['HTTP_HOST'];
	$oAuthentication = new AuthenticationWeb();
	
	if ($oAuthentication->msAccess=="pendrive"){
	
		$error_code=$oAuthentication->validate_user_web();
		
		if ($error_code==10){
		
			$oAuthentication->accessRegister("Login PRIVATE PIC OK", $oAuthentication->msUsername,1);
			header("Location: /inicio_campus.php");
			
		}else if ($error_code==11){
			$oAuthentication->accessRegister("Login PRIVATE PIC ERROR", $oAuthentication->msUsername,1);
			
			unset($_SESSION['web_student_id']);
			unset($_SESSION['web_student_name']);
			unset($_SESSION['web_user_id']);
			unset($_SESSION['web_user_type']);
			unset($_SESSION['web_student_email']);
			unset($_SESSION['web_student_language']);
			
			unset($_SESSION["isyc_user_krip"]);
			$_SESSION['isyc_errores']=$_SESSION['isyc_errores']+1;
			header("Location: /inicio_acceso.php?user=".$oAuthentication->msUsername."&error=$error_code");
		}else{
			
			$oAuthentication->accessRegister("Login PRIVATE PIC ERROR 2", $oAuthentication->msUsername,1);
			
			unset($_SESSION['web_student_id']);
			unset($_SESSION['web_student_name']);
			unset($_SESSION['web_user_id']);
			unset($_SESSION['web_user_type']);
			unset($_SESSION['web_student_email']);
			unset($_SESSION['web_student_language']);
			
			unset($_SESSION["isyc_user_krip"]);
			$_SESSION['isyc_errores']=$_SESSION['isyc_errores']+1;
			header("Location: /inicio_acceso.php?error=$error_code");
		}
		
	}else if ($oAuthentication->msAccess=="login"){ //ACCESO POR LOGIN
		$error_code=$oAuthentication->validate_user_web();
		
		if ($error_code==20){ //ALUMNO
			
			$oAuthentication->accessRegister("Login PRIVATE LOGIN OK", $oAuthentication->msUsername,1);
			header("Location: /inicio_campus.php");
			
		}else if ($error_code==21){
			$oAuthentication->accessRegister("Login PRIVATE LOGIN ERROR", $oAuthentication->msUsername,1);
			
			unset($_SESSION['web_student_id']);
			unset($_SESSION['web_student_name']);
			unset($_SESSION['web_user_id']);
			unset($_SESSION['web_user_type']);
			unset($_SESSION['web_student_email']);
			unset($_SESSION['web_student_language']);
			
			unset($_SESSION["isyc_user_krip"]);
			$_SESSION['isyc_errores']=$_SESSION['isyc_errores']+1;
			header("Location: /inicio_acceso.php?error=$error_code");
			
		}else if ($error_code==30){ //TUTOR
		
			$oAuthentication->accessRegister("Login PRIVATE LOGIN OK", $oAuthentication->msUsername,1);
			header("Location: /inicio_campus.php");
			
		}else if ($error_code==31){
			$oAuthentication->accessRegister("Login PRIVATE LOGIN ERROR", $oAuthentication->msUsername,1);
			
			unset($_SESSION['web_tutor_id']);
			unset($_SESSION['web_tutor_name']);
			unset($_SESSION['web_user_id']);
			unset($_SESSION['web_user_type']);
			unset($_SESSION['web_tutor_email']);
			unset($_SESSION['web_tutor_language']);
			
			unset($_SESSION["isyc_user_krip"]);
			$_SESSION['isyc_errores']=$_SESSION['isyc_errores']+1;
			header("Location: /inicio_acceso.php?error=$error_code");
			
		}else{
			
			$oAuthentication->accessRegister("Login PRIVATE LOGIN ERROR 2", $oAuthentication->msUsername,1);
			
			unset($_SESSION['web_tutor_id']);
			unset($_SESSION['web_tutor_name']);
			unset($_SESSION['web_user_id']);
			unset($_SESSION['web_user_type']);
			unset($_SESSION['web_tutor_email']);
			unset($_SESSION['web_tutor_language']);
			
			unset($_SESSION["isyc_user_krip"]);
			
			$_SESSION['isyc_errores']=$_SESSION['isyc_errores']+1;
			header("Location: /inicio_acceso.php?error=$error_code");
		}
	
	}
	
 	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/database/DB_Close.php");
 	
 ?> 
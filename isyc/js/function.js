function accessMoodle(course_id){

	if (course_id!=0){
		document.moodle.CID.value=course_id;
		document.moodle.submit();
	}
}

function accessTutorMoodle(){

	document.moodle.submit();
}

function accessTutorSrm(){

	document.srm.submit();
}


function searchTitle(){

	document.busqueda_simple.submit();

}

function searchAdv(){

	var str = "";
    var frm_target=document.busqueda_avanzada.destino;

    for(var i = 0; i < frm_target.length; i++)
    {
      if (i > 0)
         str = str + ", ";
      str = str + frm_target.options[i].value;
    }

    document.busqueda_avanzada.assign_options.value=str;

	document.busqueda_avanzada.submit();

}


function checkKey(){
	if (window.event.keyCode == 13)
	{
		validarLogin();
	}
}



function validarLogin(){

	var user = document.login_form.username.value;

		if(user == "admin" || user == "therediaa" || user == "jlekaroz" || user == "jokinlar1" || user == "therediam" || user == "adminm" || user == "adminm1" || user == "adminm2" || user == "adminm3" || user == "adminm4" || user == "adminm5"){
			document.forms.login_form.action = "http://moodle.gureak-akademi.local/login/index.php";
			document.forms.login_form.submit();
		}else{
			document.forms.login_form.action = "./isyc/check_login.php";
			document.forms.login_form.submit();
		}

}


function paginar(pagina){

	document.busqueda_simple.pag.value=pagina;
	document.busqueda_simple.submit();

}



function ver_filtros(){

	div = document.getElementById('d_resultados');
	div.style.display = 'none';

	div = document.getElementById('d_todos_filtros');
	div.style.display = '';

}



function validarEmail(valor) {

  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(valor)){
   return (true)
  } else {
   alert(mesg_INCORRECT_EMAIL);
   return (false);
  }
}

function checkProfile(){

	if (checkProfile2()){
		document.data_frm.send_form.value="send";
		document.data_frm.submit();
	}

}

function checkProfile2(){

		if (document.data_frm.email.value==""){

		}else if (!validarEmail(document.data_frm.email.value)){
			return false;
		}

		return true;

}


function checkEmail(){
	if (checkEmail2()){
		document.form2.submit();
	}

}

function checkEmail2(){

		if (document.form2.email.value==""){
			alert(mesg_EMPTY_EMAIL);
			return false;
		}else if (!validarEmail(document.form2.email.value)){
			return false;
		}

		return true;

}

function checkPassword(){

	if (checkPassword2()){
		document.data_frm.send_form.value="send";
		document.data_frm.submit();
	}

}

function checkPassword2(){

		if (document.data_frm.pass_nueva.value=="" || document.data_frm.pass_repetir.value==""){
			alert(mesg_EMPTY_PASS);
			return false;
		}else if (document.data_frm.pass_nueva.value!=document.data_frm.pass_repetir.value){
			alert(mesg_DISTINCT_PASS);
			return false;
		}

		return true;

}



function doAdd(borrar_origen)
{
   var origen=document.forms[0].from;
   var destino=document.forms[0].to;

   for(var i = 0; i < origen.length; i++)
   {
      if ((origen.options[i] != null) && (origen.options[i].selected))
      {
         if(borrar_origen == false)
         {
            var encontrado = false;
            for(var j = 0; j < destino.length; j++)
            {
               if (origen.options[i].value==destino.options[j].value)
               {
                  encontrado = true;
                  break;
               }
            }

            if (!encontrado)
            {
               destino.options[destino.length] = new Option(origen.options[i].text,origen.options[i].value);
               origen.options[i].selected=false;
            }
         }
         else
         {
            destino.options[destino.length] = new Option(origen.options[i].text,origen.options[i].value);
            // origen.options[i]=null;
         }
      }
   }

   if(true == borrar_origen)
   {
      for(var i = (origen.options.length-1); i >= 0; i--)
      {
         if ((origen.options[i] != null) && (origen.options[i].selected))
         {
            origen.options[i]=null;
         }
      }
   }

   return(true);
}  // doAdd

function doRemove(annadir_origen)
{
   var origen=document.forms[0].from;
   var destino=document.forms[0].to;

   for(var i = (destino.options.length-1); i >= 0; i--)
   {
      if ((destino.options[i] != null) && (destino.options[i].selected == true))
      {
         if(annadir_origen == true)
         {
            origen.options[origen.length] = new Option(destino.options[i].text,destino.options[i].value);
         }

         destino.options[i] = null;
      }
   }

   return(true);
}  // doRemove

function uploadFiles(){

	var str = "";
    var frm_target=document.subir_recursos.destino;

    for(var i = 0; i < frm_target.length; i++)
    {
      if (i > 0)
         str = str + ", ";
      str = str + frm_target.options[i].value;
    }

    document.subir_recursos.assign_options.value=str;


	resp = valida_formulario_subida();

	if(resp){ //Si hay respuesta true, procesamos el formulario
		//Verficamos que el titulo nos viene sin acentos, si lo trae los sutitumos
		limpia = Acentos(document.subir_recursos.titulo_rec.value);
		document.subir_recursos.titulo_rec.value = limpia;
		$("#tapa").css("display", "block");
		$("#cargando").css("display", "block");
		document.subir_recursos.submit(); //enviamos el formulario
	}

}


function vacio(q) { //busca caracteres que no sean espacio en blanco en una cadena

	for ( i = 0; i < q.length; i++ ) {
			if ( q.charAt(i) != " " ) {
					return true
			}
	}
	return false
}


function valida_formulario_subida() { //valida que el campo no este vacio y no tenga solo espacios en blanco

	if(vacio(document.subir_recursos.titulo_rec.value) == false && vacio(document.subir_recursos.archivo.value) == false && vacio(document.subir_recursos.acceso_rec.value) == false) {
			alert("-- Recuerde introducir un Titulo, un Nivel de acceso y  un Archivo --");
			return false;
	}else if(vacio(document.subir_recursos.titulo_rec.value) == true &&  vacio(document.subir_recursos.archivo.value) == false && vacio(document.subir_recursos.acceso_rec.value) == false) {
			alert("-- Recuerde elegir un Archivo y un Nivel de Acceso --");
			return false;
	}else if(vacio(document.subir_recursos.titulo_rec.value) == false &&  vacio(document.subir_recursos.archivo.value) == true && vacio(document.subir_recursos.acceso_rec.value) == false) {
			alert("-- Recuerde introducir un Titulo y un Nivel de Acceso --");
			return false;
	}else if(vacio(document.subir_recursos.titulo_rec.value) == false &&  vacio(document.subir_recursos.archivo.value) == true && vacio(document.subir_recursos.acceso_rec.value) == true) {
			alert("-- Recuerde introducir un Titulo --");
			return false;
	}else if(vacio(document.subir_recursos.titulo_rec.value) == true &&  vacio(document.subir_recursos.archivo.value) == false && vacio(document.subir_recursos.acceso_rec.value) == true) {
			alert("-- Recuerde introducir un Archivo --");
			return false;
	}else if(vacio(document.subir_recursos.titulo_rec.value) == true &&  vacio(document.subir_recursos.archivo.value) == true && vacio(document.subir_recursos.acceso_rec.value) == false) {
			alert("-- Recuerde introducir un Nivel de Acceso --");
			return false;
	}else{
		return true;
	}
}

function Acentos(Text)
{
	var cadena= "";
	var codigo= "";
	for (var j = 0; j < Text.length; j++)
	{
		var Char=Text.charCodeAt(j);
		switch(Char)
		{
			case 225:
			cadena+="a";
			break;
			case 233:
			cadena+="e";
			break;
			case 237:
			cadena+="i";
			break;
			case 243:
			cadena+="o";
			break;
			case 250:
			cadena+="u";
			break;
			case 193:
			cadena+="A";
			break;
			case 201:
			cadena+="E";
			break;
			case 205:
			cadena+="I";
			break;
			case 211:
			cadena+="O";
			break;
			case 218:
			cadena+="U";
			break;
			case 241:
			cadena+="n";
			break;
			case 209:
			cadena+="n";
			break;
		default:
			cadena+=Text.charAt(j);
		break;
		}
			codigo+="_"+Text.charCodeAt(j);
	}
	return cadena;
}
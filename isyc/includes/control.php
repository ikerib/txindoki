<?php
		
	if (!isset($_SESSION['tak_view'])) { session_start(); }	

	$host  = $_SERVER['HTTP_HOST'];
	if (!isset($_SESSION['web_user_id']) ){  //Hay session?
		header("Location: http://".$host."/inicio_acceso.php?error=1&NO_HAY_SESSION"); 	
		exit();
	}
	
	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/database/DB_Connection.php");	
	require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/portal/ErrorLog.php');
	require_once($_SERVER['DOCUMENT_ROOT']."/config_app.php"); 
	
	
	if ($_SESSION['web_user_type']=="STUDENT"){
		require_once($_SERVER['DOCUMENT_ROOT']."/isyc/classes/srm/modules/elearning/Student.php");
		
		$objUser = new Student();
		$objUser->loadData( $_SESSION["web_student_id"], "PORTAL","" );
		
		//$_SESSION['tak_view'] = "N";
		//$_SESSION['tak_size'] = "0";
		//$_SESSION['tak_audio'] = "1";
		//$_SESSION['isyc_idioma'] = "es";

		if ($objUser->getHighContrast()=="1"){
			$_SESSION['tak_view'] = "AC";
		}else if ($objUser->getHighContrast()=="2"){
			$_SESSION['tak_view'] = "N";
		}
		
		if ($objUser->getLocution()=="1"){
			$_SESSION['tak_audio'] = "1";
		}else if ($objUser->getLocution()=="0"){
			$_SESSION['tak_audio'] = "0";
		}
		
		if (strtoupper($objUser->getUserLang())==strtoupper("ES")){
			$_SESSION['isyc_idioma'] = "es";
		}else if (strtoupper($objUser->getUserLang())==strtoupper("EU")){
			$_SESSION['isyc_idioma'] = "eu";
		}
	}else if ($_SESSION['web_user_type']=="TUTOR"){
		require_once($_SERVER['DOCUMENT_ROOT']."/isyc/classes/srm/modules/elearning/Tutor.php"); 
		
		$objUser = new Tutor();
		$objUser->loadData( $_SESSION["web_tutor_id"]);
		
		//$_SESSION['tak_view'] = "N";
		//$_SESSION['tak_size'] = "0";
		//$_SESSION['tak_audio'] = "1";
		//$_SESSION['isyc_idioma'] = "es";
	}
	
	
	require_once($_SERVER['DOCUMENT_ROOT']."/isyc/lang/".$_SESSION['isyc_idioma']."/lbl_private.php");
	
	
?>
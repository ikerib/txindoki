<?php
// -----------------------------------------
// Student.php
// -----------------------------------------
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Moodle_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/elearning/StudentData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/Functions.php');
require_once($_SERVER['DOCUMENT_ROOT']."/isyc/lang/es/config.php"); 


class Student extends StudentData
{
	public $saveUserID = false;
	
	public function getSaveUserID(){
		return $this->saveUserID;
	}
					
	public function setSaveUserID($value){
		$this->saveUserID = $value;
	}
	
	public function loadData ($id){
	
		global $mysqli;
		global $errorLog;
		$format = new Format();
print_r("<br /> HEMEN 1<br/>");
		try {
			$query_select= sprintf("SELECT ic_student.student_id, 
										ic_student.first_name, 
										ic_student.last_name1, 
										ic_student.last_name2, 
										ic_student.user_id, 
										DATE_FORMAT(ic_student.registration_date,'%%Y/%%m/%%d') as registration_date, 
										
										ic_user.language,
										ic_user.e_mail,
										ic_user.password,
										
										ic_student.card_id, 
										ic_student.phone, 
										ic_student.mobile, 
										ic_student.city, 
										
										ic_student.pictograma1, 
										ic_student.pictograma2, 
										ic_student.pictograma3, 
										
										ic_student.level,
										ic_student.course_particular_id,
										   
										ic_student.locution,
										ic_student.high_contrast,
										
										
										ic_student.picture_name,
										ic_student.picture_file,
										ic_student.picture_width,
										ic_student.picture_height,
																				
										ic_student.company_id, 
										ic_student.remarks, 
										ic_student.active_flag, 
										ic_student.timestamp, 
										ic_student.modified_by
										
								FROM ic_student LEFT JOIN ic_user ON ic_student.user_id = ic_user.user_id
												
                          		WHERE ic_student.student_id = %d ", $id);
						
						$query_select.=sprintf(" AND ic_student.company_id = %d ", 1);
print_r($query_select);
/* 			$errorLog->LogDebug("SELECT: $query_select"); */
			
			if ($result = $mysqli->query($query_select)){
print_r("<br /> HEMEN 2<br/>");
				if ($result->num_rows > 0){
				print_r("<br /> HEMEN 3<br/>");
					print_r("<br /> HEMEN <br/>");
					$row = $result->fetch_array();
				
					$this->setID($row["student_id"]);
					
					$this->setName($row["first_name"]);
					$this->setPersonLastName1($row["last_name1"]);
					$this->setPersonLastName2($row["last_name2"]);
					
					$this->setUserID($row["user_id"]);
					if ($this->getUserID()!=null && $this->getUserID()!="")
						$this->setIsUser(true);
					else
						$this->setIsUser(false);				
					
					$this->setRegistrationDate($format->formatea_fecha($row["registration_date"]));
					
					$this->setUserLang($row["language"]);
					$this->setUserEmail($row["e_mail"]);
					$this->setUserPass($row["password"]);
				
					$this->setCardID($row["card_id"]);
					$this->setPhone($row["phone"]);
					$this->setMobile($row["mobile"]);
					$this->setBirthPlace($row["city"]); 
					
					$this->setPictogram1($row["pictograma1"]); 
					$this->setPictogram2($row["pictograma2"]); 
					$this->setPictogram3($row["pictograma3"]); 
					
					$this->setLevel($row["level"]); 
					$this->setCourseParticularID($row["course_particular_id"]); 
					
					$this->setLocution($row["locution"]); 
					$this->setHighContrast($row["high_contrast"]); 
					
					$this->setPictureName($row["picture_name"]);
					$this->setPictureFile($row["picture_file"]);
					$this->setPictureWidth($row["picture_width"]);
					$this->setPictureHeight($row["picture_height"]);
					
					$ruta_upload = str_replace("$1",$this->getCompanyID(),CFG_PICTURE_STUDENT_URL);
					
					$this->setPictureFileExists(file_exists($ruta_upload.$this->getID() . "_" . $this->getPictureFile()));
					$this->setThumbnailFileExists(file_exists($ruta_upload.$this->getID() . "_th_" . $this->getPictureFile()));
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setTimestamp($row["timestamp"]);
					$this->setModifiedBy($row["modified_by"]);
					
					$this->setAssignedCourses($this->loadCourses());
					//$this->setNoAssignedCourses($this->loadPossibleCourses());
					
					$result->close();
					
					$this->setSaveUserID(true);
				}else{
					print_r($query_select);
/* 					$errorLog->LogError($query_select); */
					exit;
				}	

			}
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}			
		
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
	}//end loadData
	
	
	public function loadDataByUser ($user_id){
	
		global $mysqli;
		global $errorLog;
		$format = new Format();
	  
		try {
			$query_select= sprintf("SELECT ic_student.student_id, 
										ic_student.first_name, 
										ic_student.last_name1, 
										ic_student.last_name2, 
										ic_student.user_id, 
										DATE_FORMAT(ic_student.registration_date,'%%Y/%%m/%%d') as registration_date, 
										
										ic_user.language,
										ic_user.e_mail,
										ic_user.password,
										
										ic_student.card_id, 
										ic_student.phone, 
										ic_student.mobile, 
										ic_student.city, 
										
										ic_student.pictograma1, 
										ic_student.pictograma2, 
										ic_student.pictograma3, 
										
										ic_student.level,
										ic_student.course_particular_id,
										   
										ic_student.locution,
										ic_student.high_contrast,
																				
										ic_student.company_id, 
										ic_student.remarks, 
										ic_student.active_flag, 
										ic_student.timestamp, 
										ic_student.modified_by
										
								FROM ic_student LEFT JOIN ic_user ON ic_student.user_id = ic_user.user_id
												
                          		WHERE ic_student.user_id = '%s' ", $user_id);
						
						$query_select.=sprintf(" AND ic_student.company_id = %d ", 1);

			$errorLog->LogDebug("SELECT: $query_select");
			
			if ($result = $mysqli->query($query_select)){
				
				if ($result->num_rows > 0){
					
					$row = $result->fetch_array();
				
					$this->setID($row["student_id"]);
					
					$this->setName($row["first_name"]);
					$this->setPersonLastName1($row["last_name1"]);
					$this->setPersonLastName2($row["last_name2"]);
					
					$this->setUserID($row["user_id"]);
					if ($this->getUserID()!=null && $this->getUserID()!="")
						$this->setIsUser(true);
					else
						$this->setIsUser(false);				
					
					$this->setRegistrationDate($format->formatea_fecha($row["registration_date"]));
					
					$this->setUserLang($row["language"]);
					$this->setUserEmail($row["e_mail"]);
					$this->setUserPass($row["password"]);
				
					$this->setCardID($row["card_id"]);
					$this->setPhone($row["phone"]);
					$this->setMobile($row["mobile"]);
					$this->setBirthPlace($row["city"]); 
					
					$this->setPictogram1($row["pictograma1"]); 
					$this->setPictogram2($row["pictograma2"]); 
					$this->setPictogram3($row["pictograma3"]); 
					
					$this->setLevel($row["level"]); 
					$this->setCourseParticularID($row["course_particular_id"]); 
					
					$this->setLocution($row["locution"]); 
					$this->setHighContrast($row["high_contrast"]); 
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setTimestamp($row["timestamp"]);
					$this->setModifiedBy($row["modified_by"]);
					
					$this->setAssignedCourses($this->loadCourses());
					//$this->setNoAssignedCourses($this->loadPossibleCourses());
					
					$result->close();
					
					$this->setSaveUserID(true);
				}else{
					$errorLog->LogError($query_select);
					exit;
				}	

			}
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}			
		
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
	}//end loadData
	
	
	public function loadDataByEmail ($email){
	
		global $mysqli;
		global $errorLog;
		$format = new Format();
	  
		try {
			$query_select= sprintf("SELECT ic_student.student_id, 
										ic_student.first_name, 
										ic_student.last_name1, 
										ic_student.last_name2, 
										ic_student.user_id, 
										DATE_FORMAT(ic_student.registration_date,'%%Y/%%m/%%d') as registration_date, 
										
										ic_user.language,
										ic_user.e_mail,
										
										ic_student.card_id, 
										ic_student.phone, 
										ic_student.mobile, 
										ic_student.city, 
										
										ic_student.pictograma1, 
										ic_student.pictograma2, 
										ic_student.pictograma3, 
										
										ic_student.level,
										ic_student.course_particular_id,
										   
										ic_student.locution,
										ic_student.high_contrast,
																				
										ic_student.company_id, 
										ic_student.remarks, 
										ic_student.active_flag, 
										ic_student.timestamp, 
										ic_student.modified_by
										
								FROM ic_student LEFT JOIN ic_user ON ic_student.user_id = ic_user.user_id
												
                          		WHERE ic_user.e_mail = '%s' ", $email);
						
						$query_select.=sprintf(" AND ic_student.company_id = %d ", 1);

			$errorLog->LogDebug("SELECT: $query_select");
			
			if ($result = $mysqli->query($query_select)){
				
				if ($result->num_rows > 0){
					
					$row = $result->fetch_array();
				
					$this->setID($row["student_id"]);
					
					$this->setName($row["first_name"]);
					$this->setPersonLastName1($row["last_name1"]);
					$this->setPersonLastName2($row["last_name2"]);
					
					$this->setUserID($row["user_id"]);
					if ($this->getUserID()!=null && $this->getUserID()!="")
						$this->setIsUser(true);
					else
						$this->setIsUser(false);				
					
					$this->setRegistrationDate($format->formatea_fecha($row["registration_date"]));
					
					$this->setUserLang($row["language"]);
					$this->setUserEmail($row["e_mail"]);
				
					$this->setCardID($row["card_id"]);
					$this->setPhone($row["phone"]);
					$this->setMobile($row["mobile"]);
					$this->setBirthPlace($row["city"]); 
					
					$this->setPictogram1($row["pictograma1"]); 
					$this->setPictogram2($row["pictograma2"]); 
					$this->setPictogram3($row["pictograma3"]); 
					
					$this->setLevel($row["level"]); 
					$this->setCourseParticularID($row["course_particular_id"]); 
					
					$this->setLocution($row["locution"]); 
					$this->setHighContrast($row["high_contrast"]); 
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setTimestamp($row["timestamp"]);
					$this->setModifiedBy($row["modified_by"]);
					
					$this->setAssignedCourses($this->loadCourses());
					//$this->setNoAssignedCourses($this->loadPossibleCourses());
					
					$result->close();
					
					$this->setSaveUserID(true);
				}	
			}
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}			
		
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
	}//end loadDataByEmail
	
	
	public function createNew()
	{
	}//END create_new
   
	
	
	public function update($id)
	{
		global $mysqli;
		global $errorLog;
		$format = new Format();
		$error = "";
		
		try {
		
			$query_update=sprintf("UPDATE ic_student 
								SET first_name = '%s', 
									last_name1 = '%s', 
									last_name2 = '%s', 
									
									registration_date = '%s', 
									
									card_id = '%s',
									phone = '%s',
									mobile = '%s',
									city = '%s',
									
									pictograma1 = %d,
									pictograma2 = %d,
									pictograma3 = %d,
					
									level = %d,
									course_particular_id = %d,
									locution = %d,
									high_contrast = %d,
									
									remarks = '%s', 
									modified_by = '%s',
									timestamp = CURRENT_TIMESTAMP,
									company_id = %d
								WHERE 
									student_id = %d",								

									$this->getName(),
									$this->getPersonLastName1(),
									$this->getPersonLastName2(), 
									
									$format->formatea_fecha_bd($this->getRegistrationDate()),
									
									$this->getCardID(),
									$this->getPhone(),
									$this->getMobile(),
									$this->getBirthPlace(),
									
									$this->getPictogram1(),
									$this->getPictogram2(),
									$this->getPictogram3(), 
									
									$this->getLevel(),
									$this->getCourseParticularID(),
									$this->getLocution(),
									$this->getHighContrast(),
									
									$this->getRemarks(),	
									$this->getUserID(),
									$this->getCompanyID(),
									$id
    							);
			
			
			$errorLog->LogDebug("UPDATE: $query_update");
			$result = $mysqli->query($query_update);
			
			if ($mysqli->error){
				$errorLog->LogError($mysqli->error);
			}
			
			//Si viene un cambio de contraseña la modifico.
			if ($_POST["pass_nueva"]!=""){
				$query_update=sprintf("UPDATE ic_user
										SET
											password='%s',
											timestamp=timestamp
										WHERE user_id = '%s'	
									 ",
										md5($_POST["pass_nueva"]),
										$this->getUserID()
									);
				
				$errorLog->LogDebug("UPDATE: $query_update");
				
				$result = $mysqli->query($query_update);
				
				if ($mysqli->error){
					$errorLog->LogError($mysqli->error);
				}
			}
			
			
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
		
		return $error;
	}//END UPDATE

	public function delete($id)
	{
			  
	}//END DELETE

	
   public function init()
   {
		$error=false;
		$error_txt="";
		$valor = "";
		//Param id
		$valor = $_POST['user_id'];
		if (trim($valor)!=""){
			$this->setID($valor);
		}
		
		//Param name
		$valor = $_POST['nombre'];
		if (trim($valor)!=""){
			$this->setName($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_ARE_PRI_PRF_NOMBRE,LBL_REQUIRED_FIELD);
			$this->setName("");
		}
		
		//Param last_name_1
		$valor = $_POST['apell_1'];
		if (trim($valor)!=""){
			$this->setPersonLastName1($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_ARE_PRI_PRF_APELLIDO1,LBL_REQUIRED_FIELD);
			$this->setPersonLastName1("");
		}
		
		//Param last_name_2
		$valor = $_POST['apell_2'];
		if (trim($valor)!=""){
			$this->setPersonLastName2($valor);
		}else{
			$error=true;
			$error_txt.="\\n- ".str_replace("$1",LBL_ARE_PRI_PRF_APELLIDO2,LBL_REQUIRED_FIELD);
			$this->setPersonLastName2("");
		}
		
		
		//user_email
		if (isset($_POST["email"]) && $_POST["email"]!=""){
			$this->setUserEmail($_POST["email"]);	
		}else{
			$this->setUserEmail("");	
			$error_txt.="\\n- ".LBL_ARE_PRI_PRF_EMAIL_TEXT01;
		}
		
		
		//Si vienen ambos campos y ambos son iguales, cambiarle la contraseña también.
		//pass_anterior
		if (isset($_POST["pass_anterior"]) && $_POST["pass_anterior"]!=""){
				//Comprobamos que la contraseña es igual a la actual, previa al cambio.
				
				$objStudent = new Student();
				$objStudent->loadData($this->getID());
				
				if ($objStudent->getUserPass()==md5($_POST["pass_anterior"])){
					//OK
				}else{
					$error_txt.="\\n- ".LBL_ARE_PRI_PRF_MSG_CONTR0;
				}
		}
		
		//pass_nueva y pass_repetir
		if (isset($_POST["pass_nueva"]) && $_POST["pass_nueva"]!="" && isset($_POST["pass_repetir"]) && $_POST["pass_repetir"]!=""){
			
			if ($_POST["pass_nueva"]==$_POST["pass_repetir"]){
				
				//Todo OK, podemos cambiar la contraseña.
			}else{
				$error_txt.="\\n- ".LBL_ARE_PRI_PRF_MSG_CONTR1; //La nueva contraseña y su confirmación deben ser iguales.
			}
			
			
		}else{
			if (isset($_POST["pass_nueva"]) && ($_POST["pass_nueva"]!="" || $_POST["pass_repetir"]!=""))
				$error_txt.="\\n- ".LBL_ARE_PRI_PRF_MSG_CONTR2; //Debe escribir la nueva contraseña y su confirmación
		}
		
		
		return $error_txt;
   }//END init

	private function loadCourses()
	{
    
		global $mysqli;
		global $errorLog;
		
         $query_select = "SELECT ic_course.course_id, ".
                        "       ic_course.course_name ".
                        "  FROM ic_course, ic_student_course ".
                        " WHERE ic_course.course_id = ic_student_course.course_id ".
                        "   AND ic_student_course.student_id = ".$this->getID().
                        "   AND ic_course.active_flag = 1 ".
                        "   AND ic_course.company_id = 1";
		
		$errorLog->LogDebug("SELECT: $query_select");
        $result = $mysqli->query($query_select);
		return $result;

	}//loadRoles
   
   
   
   public function exists($user_id)
	{
		global $mysqli;
		global $errorLog;
		
		$bResult = false;
		
		try {	
			$query_select = sprintf("SELECT user_id FROM ic_student WHERE user_id='%s' AND active_flag=1", $user_id);
			
			$errorLog->LogDebug("SELECT: $query_select");
			
			if ($result = $mysqli->query($query_select))
			{
				if ($result->num_rows>0)
					$bResult = true;
					
				$result->close();
			}	
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}	
		return $bResult;
    }
	
	
	
	public function saveCourse($courseid, $studentid) 
	{
		global $mysqli;
		global $errorLog;
		
		try {
			
			$query_select = sprintf("SELECT * FROM ic_student_course WHERE student_id=%d AND course_id=%d", $studentid, $courseid);
			
			$errorLog->LogDebug("SELECT: $query_select");
			
			if ($result = $mysqli->query($query_select))
			{
				if ($result->num_rows==0)
				{
		
					$query_insert = "INSERT INTO ic_student_course (course_id, student_id) ".
								" VALUES ( ".$courseid.", ".$studentid.") ";
								
					$result2 = $mysqli->query($query_insert);
					
					$errorLog->LogDebug("INSERT: $query_insert");
				}
			}

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}		
	
	}
   
   
   
   
    private function existsUserID($sUserID)
    {	
		global $mysqli;
		global $errorLog;
	
        $bExists = false;

        if (!isset($sUserID))
            return false;
		
		$query_select = "";
		$query_select = sprintf("SELECT * FROM ic_user WHERE UPPER(ic_user.user_id) = UPPER('%s')", $sUserID);
		
		$errorLog->LogDebug("SELECT: $query_select");
		
		if ($result = $mysqli->query($query_select)){
		
			if ($result->num_rows>0){
				$bExists = true;
			}
			$result->close();
		}

        return $bExists;
    }
   
   
   
   
   //Función que envía un correo con instrucciones para que el Alumno pueda recuperar su contraseña.
	
	public function recoveryPassword($email)
	{
		global $mysqli;
		global $errorLog;
		
		$this->loadDataByEmail($email);
		
		$user_id = $this->getUserID();
		$language = $this->getUserLang();
		$user_email = $this->getUserEmail();
		
		
		try {
		
			//Si existe le enviamos un correo con un enlace.
			
			if ($this->existsUserID($user_id)){
					
				//ENVIO POR CORREO DEL USUARIO.
				$mail = new Mail();
				
				$to=$user_email;
				$from=CFG_EMAIL_FROM_DEFAULT;
				$subject=LBL_ARE_PRI_RCO_MENSAJE_ASUNTO;
				
				
				$contents=str_replace("$1",CFG_RECOVERY_URL."?access=".md5($user_id.$email),LBL_ARE_PRI_RCO_MENSAJE_INSTRUCCIONES); 
				//$contents.=str_replace("$2",CFG_RECOVERY_URL,$contents); 
				$contents.=LBL_ARE_PRI_RCO_MENSAJE_FIRMA;
				
				if ($subject!="" && $to!="" && $from!=""){
					$ok = $mail->send_mail($to, $from, $subject, $contents , "", "");
				}
			}
			
		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}
	  
		return true;
	}//END recoveryPassword
   
   
   
   
   public function loadUserEmail($access)
    {	
		global $mysqli;
		global $errorLog;
	
        $user_id="";

        $query_select = "";
		$query_select = sprintf("SELECT user_id FROM ic_user WHERE md5(concat(user_id,e_mail)) = '%s'", $access);
		
		$errorLog->LogDebug("SELECT: $query_select");
		
		if ($result = $mysqli->query($query_select)){
			
			if ($result->num_rows>0){
				$row = $result->fetch_array();
				$user_id=$row["user_id"];
			}
			$result->close();
		}
		
		

        return $user_id;
    }
	
	
    public function updatePassword($user_id)
    {	
		global $mysqli;
		global $errorLog;
	
        $query_update=sprintf("UPDATE ic_user
										SET
											password='%s',
											timestamp=timestamp
										WHERE user_id = '%s'	
									 ",
										md5($_POST["pass_nueva"]),
										$user_id
									);
				
		$errorLog->LogDebug("UPDATE: $query_update");

		$result = $mysqli->query($query_update);

		if ($mysqli->error){
			$errorLog->LogError($mysqli->error);
		}
		
		$this->loadDataByUser($user_id);
		
		//ENVIO POR CORREO DEL USUARIO.
		$mail = new Mail();
		
		$to=$this->getUserEmail();
		$from=CFG_EMAIL_FROM_DEFAULT;
		$subject=LBL_ARE_PRI_RCO_MENSAJE2_ASUNTO;
		
		
		$contents=str_replace("$2",$_POST["pass_nueva"],(str_replace("$1",$user_id,LBL_ARE_PRI_RCO_MENSAJE2_INSTRUCCIONES))); 
		//$contents.=str_replace("$2",$_POST["pass_nueva"],$contents); 
		$contents.=LBL_ARE_PRI_RCO_MENSAJE2_FIRMA;
		
		if ($subject!="" && $to!="" && $from!=""){
			$ok = $mail->send_mail($to, $from, $subject, $contents , "", "");
		}
		
		
    }
   
   
  	//Función que dado el user_id obtiene la ruta para la imagen del perfil en Moodle.
   	public function loadMoodleProfilePhoto($userid) 
	{
		global $mysqliMoodle;
		global $errorLog;
		
		$ruta="";
		
		try {
			$query_select= sprintf("SELECT 
										DISTINCT mdl_user.username, mdl_context.id FROM mdl_user
										INNER JOIN mdl_context ON mdl_user.id = mdl_context.instanceid
										WHERE contextlevel = 30 AND mdl_user.username = '%s' ",  $userid);
		
			$errorLog->LogDebug("SELECT: $query_select");
		
		 if ($result = $mysqliMoodle->query($query_select)){
				
				if ($result->num_rows > 0){
					$row = $result->fetch_array();
						
					$ruta = "/".$row["id"]."/user/icon/f1";
				}
		  }

		} catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
		}		
	
		return $ruta;
	}//loadMoodleProfilePhoto 
   
   
   
	
}

?>
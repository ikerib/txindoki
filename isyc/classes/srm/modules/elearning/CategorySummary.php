<?php
// -----------------------------------------
// CategorySummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/AbstractCRMObjectSummary.php');

class CategorySummary extends AbstractCRMObjectSummary
{
	
	
   public function CategorySummary()  
   {
   }	

   public function getOrderByColumn()
   {
      $sOrderBy = $this->getOrderBy();
      
      if (null == $sOrderBy)
         return "ic_course_category.course_category_name";
		 
      try {
         switch( $sOrderBy) {
            case 2:  $sOrderBy = "ic_course_category.course_category_desc"; break;
			case 3:  $sOrderBy = "ic_course_category.course_category_shortname"; break;
			case 4:  $sOrderBy = "ic_course_category.order_appearance"; break;
			case 5:  $sOrderBy = "ic_course_category.language"; break;
			default: $sOrderBy = "ic_course_category.course_category_name";
         }
      } catch (Exception $ex) {
         return $sOrderBy;
      }
      return $sOrderBy;
   }//END
   
   public function  load()
   {
    
    
   }//end load
   
   public function loadFilter($sType, $sID)
   {
   
   }
   
   
   public function loadInfoPortal($sLanguage)
   {
   
	 $nCompanyID = 1;
	 $sLanguage = $sLanguage;
     
     global $mysqli;
	  
      try
      {

      $sFrom = " FROM ic_course_category ";
            
      $sWhere = sprintf(" WHERE ic_course_category.active_flag = 1 ");
      $sWhere.= sprintf(" AND ic_course_category.language = '%s' ",$sLanguage);
	  
      $sOperator = " = ";
      $sCondition = "";

	  // Param name
	  $sWhere.=$this->getWhereClause("name", "course_category_name", "ic_course_category", $this->STRING_TYPE, "", "");

	  // Param description
	  $sWhere.=$this->getWhereClause("description", "course_category_desc", "ic_course_category", $this->STRING_TYPE, "", "");

	  $query_select = "SELECT COUNT(*) " . $sFrom . $sWhere;
	  
	  if ($result = $mysqli->query($query_select)){
			$row = $result->fetch_array();
			$this->init($row[0]);
			$result->close();
	  }else{
			$this->init(0);
	  }
		 
       $query_select="SELECT ic_course_category.course_category_id,
				  COALESCE(ic_course_category.course_category_name,'') as course_category_name,
				  COALESCE(ic_course_category.course_category_desc,'') as course_category_desc,
				  COALESCE(ic_course_category.course_category_shortname,'') as course_category_shortname,
				  ic_course_category.language,
				  ic_course_category.order_appearance,
				  ic_course_category.picture_file,
				  ic_course_category.picture_file_ac ";
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY order_appearance";
       
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
		
   
   
   }
   
   
   
  
	
	
}

?>
<?php
// -----------------------------------------
// StudentSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/AbstractCRMObjectSummary.php');

class StudentSummary extends AbstractCRMObjectSummary
{
	
	
   public function StudentSummary()  
   {
   }	

   public function getOrderByColumn()
   {
     
   }//END
   
   
   
   
   /**
    * Crea una lista con los objetos que responden a las
    * restricciones establecidas en la petición.
    *
    * @param  oReq     petición
    * @throws          isyc.website.CRMException
    *                  si se produce algún error al obtener los datos.
    * @throws          NullPointerException
    *                  si la petición especificada es un valor <CODE>null</CODE>.
    * @since           CRM 1.0.0
    */
	public function  load()
	{
    
		
	  
      
   }//end load
   
   
   
   
   
   
   public function  loadFilter($sType, $sID)
   {
   }//end loadFilter
   
   
   
   //FUNCIÓN QUE DADO UN CURSO MOODLE, DEVUELVE EL LISTADO DE TODOS LOS ALUMNOS.
   
    public function  loadStudentByCourse($sCourse)
	{
    
		$nCompanyID = 1;
		
		global $mysqli;
	  
		try
		{
			 
			$query_select="SELECT ic_student.student_id,
						ic_student.first_name,
						ic_student.last_name1,
						ic_student.user_id ";
			
			$sFrom = " FROM ic_student ";
			$sFrom.= " LEFT JOIN ic_student_course ON ic_student.student_id = ic_student_course.student_id ";
				
			$sWhere = " WHERE ic_student.active_flag = 1 ";
			$sWhere.= " AND ic_student.company_id = ".$nCompanyID;
			
			
			$sWhere.= sprintf(" AND ic_student_course.course_id IN (Select course_id from ic_course where course_moodle_id='%s') ",$sCourse);
						
			$query_select.= $sFrom . $sWhere;
			$query_select.= " ORDER BY first_name ";
			
			//echo $query_select;
		   
			$result = $mysqli->query($query_select);
		   
			return $result;
		
		}
		catch (Exception $ex)
		{
           
		}
      
	  
      
   }//end loadStudentByCourse
	
	
}

?>
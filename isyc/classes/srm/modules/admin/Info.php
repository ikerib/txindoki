<?php
// -----------------------------------------
// Info.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/admin/InfoData.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/Functions.php');

require_once($_SERVER['DOCUMENT_ROOT']."/isyc/lang/es/config.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/admin/Attachment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/admin/Picture.php');


class Info extends InfoData
{
	
   public function loadData ($id, $sLanguage, $admin=false){
	
	$nCompanyID = 1;
	$sLanguage = $sLanguage;
	  
      global $mysqli;
	  global $errorLog;
	  $format = new Format();	
	  
	  try {
		$query_select= sprintf("SELECT ic_info.info_id, 
								ic_info.info_title,
								ic_info.info_title_eu,
								ic_info.info_desc,
								ic_info.info_desc_eu,
								ic_info.company_id,
								ic_info.info_type_id,
								ic_info.info_date,
								ic_info.contents,
								ic_info.contents_eu,
								ic_info.link,
								ic_info.link_name,
								
								ic_info.info_source,
								ic_info.publication_date,
								ic_info.expiration_date,
								ic_info.info_extra,
								
								ic_info.remarks,
								ic_info.active_flag,
								ic_info.modified_by,
								ic_info.timestamp
                     FROM ic_info LEFT OUTER JOIN ic_info_type
                            ON ic_info_type.info_type_id=ic_info.info_type_id
                     WHERE ic_info.info_id = %d ",
					 
					 $id);
		
		$query_select.= sprintf(" AND ic_info.company_id = %d ", $nCompanyID);
		 
		if (!$admin) //Los administradores podr�n verlo desde cualquier lugar, aunque est� fuera de fecha.
			$query_select.= " AND NOW() BETWEEN publication_date AND expiration_date ";
		
		 $errorLog->LogDebug("SELECT: $query_select");
		
		 if ($result = $mysqli->query($query_select)){
				
					$row = $result->fetch_array();
				
					$this->setID($row["info_id"]);
					$this->setName($sLanguage=="es"?$row["info_title"]:$row["info_title_eu"]);
					$this->setDescription($sLanguage=="es"?$row["info_desc"]:$row["info_desc_eu"]);
					$this->setInfoTypeID($row["info_type_id"]);
					$this->setDate($format->formatea_fecha($row["info_date"]));
					$this->setContents($sLanguage=="es"?$row["contents"]:$row["contents_eu"]);
					$this->setLink($row["link"]);
					$this->setLinkName($row["link_name"]);
					
					$this->setInfoSource($row["info_source"]);
					$this->setPublicationDate($format->formatea_fecha($row["publication_date"]));
					$this->setExpirationDate($format->formatea_fecha($row["expiration_date"]));
					$this->setInfoExtra($row["info_extra"]);
					
					$this->setCompanyID($row["company_id"]);
					$this->setRemarks($row["remarks"]);
					$this->setActiveFlag($row["active_flag"]);
					$this->setModifiedBy($row["modified_by"]);
					$this->setTimestamp($row["timestamp"]);
					
					$result->close();
				
			}		 
			
	  } catch (Exception $e) {
			$errorLog->LogFatal("Caught exception: ". $e->getMessage());
	  }
		
	}//end loadData
   
   

   public function createNew()
   {
	  
   }//END create_new
   

   
   
   public function update($id)
   {
	  
   }//END UPDATE
   
   
   
   public function delete($id)
   {
			  
   }//END DELETE
   
 

   public function init()
   {
	
   }//END init
   




}//END CLASS
?>
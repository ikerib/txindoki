<?php
// -----------------------------------------
// PictureSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/admin/PictureSummary.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/AbstractCRMObjectSummary.php');

class PictureSummary extends AbstractCRMObjectSummary
{
	
   public function PictureSummary()  
   {
   }	

   public function getOrderByColumn()
   {
     
   }//END
   

    public function  load()
   {
   }//end load
   
   
   
   public function  loadPicture($sType, $sID, $sLanguage)
   {
    
     $nCompanyID = 1;
	 $sLanguage = $sLanguage;
     
     global $mysqli;
	  
      try
      {
		 
      $sFrom = " FROM ic_picture  ";
	  $sFrom.= " , ica_object_type ";
	        
      $sWhere = " WHERE ic_picture.active_flag = 1 ";
	  $sWhere.= " AND ica_object_type.object_type_id = ic_picture.object_type_id ";
	  $sWhere.= sprintf(" AND ica_object_type.language ='%s' ",$sLanguage);
	  
      $sWhere.= " AND ic_picture.company_id = $nCompanyID ";
	  
	  if ($sType=="INFORMATION")
      {
			$sWhere.="   AND ic_picture.object_id = " . $sID ;
			$sWhere.="   AND ic_picture.object_type_id = 22";
      }else{
			$sWhere.="   AND 1 = 2 " ;
	  }
	 
       $query_select="SELECT ic_picture.picture_id,
							COALESCE(ic_picture.picture_name,'') as picture_name,
							COALESCE(ic_picture.picture_desc,'') as picture_desc,
							COALESCE(ic_picture.picture_order,0) as picture_order,
							ic_picture.picture_file,
							ic_picture.picture_width,
							ic_picture.picture_height,
							ic_picture.object_name,
							ic_picture.object_type_id,
							ica_object_type.icon,
							ic_picture.company_id" ;
	   
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY picture_id";
       
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
	  
      
   }//end load

   
}
?>
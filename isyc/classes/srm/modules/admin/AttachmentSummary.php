<?php
// -----------------------------------------
// AttachmentSummary.php
// -----------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/database/DB_Connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/admin/AttachmentSummary.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/isyc/classes/srm/modules/AbstractCRMObjectSummary.php');

class AttachmentSummary extends AbstractCRMObjectSummary
{
	
   public function AttachmentSummary()  
   {
   }	

   public function getOrderByColumn()
   {
   }//END
   
   public function  load()
   {
   }//end load
   
    public function  loadAttachment($sType, $sID, $sLanguage)
    {
    
     $nCompanyID = 1;
	 $sLanguage = $sLanguage;
     
     global $mysqli;
	 global $errorLog;
	  
      try
      {
		 
      $sFrom = " FROM  ic_attachment LEFT JOIN ica_object_type ON ic_attachment.object_type_id = ica_object_type.object_type_id  ";
	        
      $sWhere = " WHERE ic_attachment.active_flag = 1 ";
	  $sWhere.= sprintf(" AND ica_object_type.language = '%s' ",$sLanguage);
	  
	  $sWhere.= " AND ic_attachment.company_id = $nCompanyID ";
       
	  if ($sType=="INFORMATION")
      {		
			$sWhere.="   AND ic_attachment.object_id = " . $sID ;
			$sWhere.="   AND ic_attachment.object_type_id = 22";
      }else{
			$sWhere.="   AND 1 = 2 " ;
	  }
		
	   
       $query_select="SELECT ic_attachment.attachment_id,
							 COALESCE(ic_attachment.attachment_name,'') as attachment_name,
							 COALESCE(ic_attachment.attachment_file,'') as attachment_file,
							 COALESCE(ic_attachment.attachment_desc,'') as attachment_desc,
							 COALESCE(ica_object_type.object_type_name,'') as object_type_name,
							 COALESCE(ic_attachment.object_name,'') as object_name,
							 ic_attachment.company_id " ;
	   
       $query_select.= $sFrom . $sWhere;
       $query_select.= " ORDER BY attachment_id";
       
       $errorLog->LogDebug("SELECT: $query_select");
	   
       $result = $mysqli->query($query_select);
	   
	   return $result;
		
      }
      catch (Exception $ex)
      {
           
      }
      
   }//end loadFilter
   
  

}
?>
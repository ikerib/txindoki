<?php
// Etiquetas EN para el tutor

$prefijo="LBL_";

define($prefijo."USER_LOGIN", "User ID");
define($prefijo."USER_EMAIL", "Email User");
define($prefijo."USER_LANGUAGE", "User Language");
define($prefijo."REGISTRATION_DATE", "Registration date");
define($prefijo."USER_DATA", "User Data");

define($prefijo."USER_ID", "User");
define($prefijo."OLD_PASSWORD", "Current password");
define($prefijo."NEW_PASSWORD", "New password");
define($prefijo."CONFIRM_PASSWORD", "Confirm password");
define($prefijo."CONFIRM_NEW_PASSWORD", "Confirm New Password");

define($prefijo."CARD_ID", "NIF");
define($prefijo."CITY", "City");
define($prefijo."MOBILE", "Mobile");
define($prefijo."PHONE", "Phone");

define($prefijo."MOODLE_COURSES", "Moodle courses");
define($prefijo."MOODLE_COURSE", "Moodle course");

$prefijoMsg = "MSG_";
define($prefijoMsg."USER_DATA_INFO1","This tutor has not create a user yet.");
define($prefijoMsg."USER_IDENTIFICATOR","This is one of the identifier options that we propose to you. You can choose another option if you prefer.<br/><b>Are you sure, the user ID generated contains alphanumeric characters in lowercase and without spaces.</b>");

define($prefijoMsg."USER_DATA_INFO2", "Si quiere cambiarle la constraseña (el usuario la recibirá en el correo configurado).");

define($prefijoMsg."USER_SIGNATURE","\n\nSincerely,\nGureak ");

define($prefijoMsg."NEW_USER_TEXT_01", "Dear Tutor,\n\nThe login data to access the platform is:");

define($prefijoMsg."EMAIL_TEXT01", "Debe tener un correo electrónico.");
define($prefijoMsg."USERDATA_TEXT01", "La Nueva contraseña y su Confirmación deben ser iguales.");
define($prefijoMsg."USERDATA_TEXT02", "Debe rellenar la Nueva contraseña y su Confirmación.");
define($prefijoMsg."USERDATA_TEXT03", "Ya existe un usuario con este nombre.");
define($prefijoMsg."USERDATA_TEXT04", "Debe elegir el nombre de usuario.");

define($prefijoMsg."SUBJECT_USER_PASSWORD", "Gureak - Access Password");

?>
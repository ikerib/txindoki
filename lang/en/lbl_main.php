<?php
// Etiquetas EN para la pantalla de login

$prefijo="LBL_";

define($prefijo."HOME", "HOME");
define($prefijo."DATE_ENROLLMENT", "Registration date");
define($prefijo."CERTIFICATION_COURSE", "Certification / Course");
define($prefijo."INTERNAL_ID4", "Internal code");
define($prefijo."USER_LOGIN", "User");

define($prefijo."ENROLLMENT_PENDING", "ENROLLMENT STILL TO BE VALIDATED");
define($prefijo."STUDENT_PENDING", "STUDENTS STILL TO BE VALIDATED");

?>
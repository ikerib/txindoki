<?php
// Etiquetas EN para la Información

$prefijo="LBL_";

define($prefijo."INFO_TYPE", "Type information");
define($prefijo."DATE", "Date");
define($prefijo."LINK_NAME", "Link name");
define($prefijo."LINK", "Link");
define($prefijo."CONTENTS", "Content");
define($prefijo."INFORMATION", "Information Content");

define($prefijo."INFO_SOURCE", "Source");
define($prefijo."PUBLICATION_DATE_INFO", "Date range in which the info is visible");
define($prefijo."PUBLICATION_DATE", "Publication Date");
define($prefijo."EXPIRATION_DATE", "Expiration Date");
define($prefijo."INFO_EXTRA", "Video inlaid");

$prefijoMsg = "MSG_";
define($prefijoMsg."INFO_TEXT_01", "The title of the information is mandatory in both languages.");
define($prefijoMsg."INFO_TEXT_02", "You must select a date for the information.");


?>
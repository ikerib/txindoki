<?php
// Etiquetas EN para el empleado

$prefijo="LBL_";

define($prefijo. "INTERNAL_ID4", "Internal ID");
define($prefijo. "NATIONAL_ID_TYPE", "Document type");
define($prefijo. "NATIONAL_ID", "Document ID");
define($prefijo. "SEX", "Gender");
define($prefijo. "FEMALE", "Female");
define($prefijo. "MALE", "Male");
define($prefijo. "USER_LOGIN", "Username");
define($prefijo. "USER_EMAIL", "Email");
define($prefijo. "USER_LANGUAGE", "User Language");
define($prefijo. "DATE_BIRTH", "Birth Date");
define($prefijo. "PLACE_BIRTH", "Birthplace");
define($prefijo. "PROVINCE", "Province ");
define($prefijo. "COUNTRY_BIRTH", "Country of birth");
define($prefijo. "NACIONALITY", "Nationality");
define($prefijo. "MATERNAL_LANG", "Language");
define($prefijo. "DATE_ON", "Creation date");
define($prefijo. "DATE_OFF", "Delition date");
define($prefijo. "USER_DATA", "Ecampus Data");

define($prefijo. "DEPARTMENT", "Department");

define($prefijo. "NEW_PASSWORD", "New password");
define($prefijo. "CONFIRM_PASSWORD", "Confirm password");

define($prefijo. "ROLE_ADMIN", "ADMIN");
define($prefijo. "ROLE_EMPLOYEE", "EMPLOYEE");

$prefijoMsg = "MSG_";
define($prefijoMsg."USER_DATA_INFO1","You must fill in the username and password.");
define($prefijoMsg."USER_DATA_INFO2","You can change the password for user access");

define($prefijoMsg."NEW_EMPLOYEE_SUBJECT", "Gureak - Employee ");
define($prefijoMsg."NEW_EMPLOYEE_TEXT_01", "Dear Employee,\n\nThe data to access the platform is: ");

define($prefijoMsg."SELECT_FILE","You must select a file.");
define($prefijoMsg."INFO_SIZE", "Remember that the maximum size is $1 kb. Allowed type of files: csv or txt. ");

define($prefijoMsg."ERROR_TEXT_01","You must assign this to a Employee Center.");
define($prefijoMsg."ERROR_TEXT_02","The field name is required.");
define($prefijoMsg."ERROR_TEXT_03","You must choose another user ID. There is already a user with that ID.");
define($prefijoMsg."ERROR_TEXT_04","\\nTo create the user fill out the fields: \\nUser ID, \\nEmail User and \\nLanguage of the user.");

define($prefijoMsg."ERROR_TEXT_05", "You should choose a user ID for the Employee.");
define($prefijoMsg."ERROR_TEXT_06", "You must choose a role for the Employee.");

define($prefijoMsg."EMAIL_TEXT01", "You must have an e-mail.");

define($prefijoMsg."USERDATA_TEXT01", "The New password and confirmation must be equal.");
define($prefijoMsg."USERDATA_TEXT02", "Must complete the New password and confirmation.");
define($prefijoMsg."USERDATA_TEXT03", "User already exists with this name. ");
define($prefijoMsg."USERDATA_TEXT04", "You must choose your username. ");

define($prefijoMsg."USER_IDENTIFICATOR","This is one of the identifier options that we propose to you. You can choose another option if you prefer.<br/><b>Are you sure, the user ID generated contains alphanumeric characters in lowercase and without spaces.</b>");
define($prefijoMsg."SUBJECT_USER_PASSWORD", "Gureak Usa - Access Password");
define($prefijoMsg."NEW_USER_TEXT_01", "Dear employee,\n\nThe login data to access the platform is:");
define($prefijoMsg."USER_SIGNATURE","\n\nSincerely,\nGureak");

?>
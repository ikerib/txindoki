<?php
// Etiquetas ES para los Header Tags

$prefijo="MSG_";

define($prefijo."WARNING", "Warning");
define($prefijo."UNSAVED_FORWARD", "You have not saved the latest changes. \\nYou should save before continuing. \\n\\nClick OK to continue without saving or click Cancel.");
define($prefijo."UNSAVED_BACKWARD", "You have not saved your changes. Click 'OK' to continue (the changes will be lost) or click 'Cancel' to stop. ");
define($prefijo."CONFIRM_DELETION", "Click 'OK' if you really want to delete the data in the current record or press' Cancel 'to stop.");
define($prefijo."DATE_END_INVALID", "End date can not be previous to the start date.");
define($prefijo."INVALID_DECIMAL_NUMBER", "The number entered is not a valid decimal value.");
define($prefijo."INVALID_NUMBER", "You must insert a numerical value:");

?>
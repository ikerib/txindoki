<?php
// Etiquetas ES para la Imágen

$prefijo="LBL_";

define($prefijo."IMAGE_NAME", "Nombre de la imágen");
define($prefijo."NOTE", "Nota");
define($prefijo."ORDER", "Orden");
define($prefijo."NEW_PICTURE", "Añadir nueva imágen");

$prefijoMsg="MSG_";
define($prefijoMsg."INFO_SIZE", "Recuerde que los tamaños máximos son de $1 pixels de ancho y $2 pixels de altura y el ratio de la imagen para su correcta visualización debería de ser de 4:3 (cuatro tercios). Subir imágenes: GIF, JPEG o PNG.");
define($prefijoMsg."INFO_ORIGINAL", "Está viendo la imágen reducida. Si desea ver la original pulse sobre ella.");
define($prefijoMsg."INFO_SELECT", "Tiene que seleccionar una imágen.");
define($prefijoMsg."INFO_BIG_SIZE", "La imágen que intenta subir es demasiado grande, sus medidas son: ancho($1) x alto($2) pixels de altura. Debe ser como máximo de: ancho($3) x alto($4).");
define($prefijoMsg."INFO_LARGE", "La imágen que intenta subir es demasiado grande $1 bytes. Debe ser como máximo de $2 bytes.");

?>
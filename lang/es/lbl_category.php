<?php
// Etiquetas ES para las categorías.

$prefijo="LBL_";

define($prefijo."CATEGORY", "Categoría");
define($prefijo."DESC", "Descripción");
define($prefijo."SHORTNAME", "Nombre corto");
define($prefijo."ORDER_APPEARANCE", "Orden");

define($prefijo."ICON", "Icono");
define($prefijo."ICON_AC", "Icono en Alto Contraste");
define($prefijo."NOTE", "Nota");
define($prefijo."NEW_PICTURE", "Añadir nueva imágen");

$prefijoMsg="MSG_";
define($prefijoMsg."INFO_SIZE", "Recuerde que los tamaños máximos son de $1 pixels de ancho y $2 pixels de altura. <br/>Se creará un icono de $3 pixels de ancho x $4 pixels de altura, visible en el portal. Subir iconos: GIF, JPEG o PNG.");
define($prefijoMsg."INFO_ORIGINAL", "Está viendo el icono. Si desea ver la imagen original pulse sobre ella.");
define($prefijoMsg."INFO_SELECT", "Tiene que seleccionar una imágen para convertirla en icono de este curso.");
define($prefijoMsg."INFO_BIG_SIZE", "La imágen que intenta subir es demasiado grande, sus medidas son: ancho($1) x alto($2) pixels de altura. Debe ser como máximo de: ancho($3) x alto($4).");
define($prefijoMsg."INFO_LARGE", "La imágen que intenta subir es demasiado grande $1 bytes. Debe ser como máximo de $2 bytes.");


define($prefijoMsg."INFO_SIZE_AC", "Recuerde que los tamaños máximos son de $1 pixels de ancho y $2 pixels de altura. <br/>Se creará un icono de $3 pixels de ancho x $4 pixels de altura, visible en el portal. Subir iconos: GIF, JPEG o PNG.");
define($prefijoMsg."INFO_ORIGINAL_AC", "Está viendo el icono. Si desea ver la imagen original pulse sobre ella.");
define($prefijoMsg."INFO_SELECT_AC", "Tiene que seleccionar una imágen para convertirla en icono de este curso.");
define($prefijoMsg."INFO_BIG_SIZE_AC", "La imágen que intenta subir es demasiado grande, sus medidas son: ancho($1) x alto($2) pixels de altura. Debe ser como máximo de: ancho($3) x alto($4).");
define($prefijoMsg."INFO_LARGE_AC", "La imágen que intenta subir es demasiado grande $1 bytes. Debe ser como máximo de $2 bytes.");




?>
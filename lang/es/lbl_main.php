<?php
// Etiquetas ES para la pantalla de login

$prefijo="LBL_";

define($prefijo."PORTADA", "PORTADA");
define($prefijo."DATE_ENROLLMENT", "Fecha de matriculación");
define($prefijo."CERTIFICATION_COURSE", "Certificación/Curso");
define($prefijo."INTERNAL_ID4", "Código interno");
define($prefijo."USER_LOGIN", "Usuario");

define($prefijo."ENROLLMENT_PENDING", "MATRÍCULAS PENDIENTES DE VALIDAR");
define($prefijo."STUDENT_PENDING", "ALUMNOS PENDIENTES DE VALIDAR");

?>
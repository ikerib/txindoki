<?php
// Etiquetas ES para el tutor

$prefijo="LBL_";

define($prefijo."USER_LOGIN", "Identificador de usuario");
define($prefijo."USER_EMAIL", "Correo electrónico del usuario");
define($prefijo."USER_LANGUAGE", "Idioma del usuario");
define($prefijo."REGISTRATION_DATE", "Fecha de registro");
define($prefijo."USER_DATA", "Datos del usuario");

define($prefijo."USER_ID", "Usuario");
define($prefijo."OLD_PASSWORD", "Contraseña actual");
define($prefijo."NEW_PASSWORD", "Nueva contraseña");
define($prefijo."CONFIRM_PASSWORD", "Confirmar contraseña");
define($prefijo."CONFIRM_NEW_PASSWORD", "Confirmar nueva contraseña");

define($prefijo."CARD_ID", "NIF");
define($prefijo."CITY", "Ciudad");
define($prefijo."MOBILE", "Movil");
define($prefijo."PHONE", "Teléfono");

define($prefijo."MOODLE_COURSES", "Cursos Moodle");
define($prefijo."MOODLE_COURSE", "Curso Moodle");

define($prefijo."PHOTO", "Fotografía del tutor, extraida de su perfil en Moodle");

$prefijoMsg="MSG_";
define($prefijoMsg."USER_DATA_INFO1", "Este tutor todavía no tiene usuario creado para poder acceder.");
define($prefijoMsg."USER_IDENTIFICATOR", "<b>Asegurese que el Identificador de usuario generado contiene carácteres alfanuméricos en minúsculas y sin espacios.</b>");

define($prefijoMsg."USER_DATA_INFO2", "Si quiere cambiarle la constraseña (el usuario la recibirá en el correo configurado).");

define($prefijoMsg."USER_SIGNATURE", "Atentamente,\nGureak");

define($prefijoMsg."NEW_USER_TEXT_01", "Estimado Tutor,\n\nLos datos para acceder a la plataforma son los siguientes:");

define($prefijoMsg."EMAIL_TEXT01", "Debe tener un correo electrónico.");
define($prefijoMsg."USERDATA_TEXT01", "La Nueva contraseña y su Confirmación deben ser iguales.");
define($prefijoMsg."USERDATA_TEXT02", "Debe rellenar la Nueva contraseña y su Confirmación.");
define($prefijoMsg."USERDATA_TEXT03", "Ya existe un usuario con este nombre.");
define($prefijoMsg."USERDATA_TEXT04", "Debe elegir el nombre de usuario.");

define($prefijoMsg."SUBJECT_USER_PASSWORD", "Gureak - Access Password");

?>
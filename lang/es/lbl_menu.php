<?php
// Etiquetas ES para el menú

$prefijo="LBL_";

define($prefijo."MNU_OPEN", "Abrir");
define($prefijo."MNU_NEW", "Nuevo");
define($prefijo."-MNU_UTILITIES", "Utilidades");
define($prefijo."MANAGEMENT", "Gestión");
define($prefijo."-CUSTOMERS", "Clientes");
define($prefijo."-SALES", "Ventas");
define($prefijo."-MARKETING", "Marketing");
define($prefijo."-HHRR", "HHRR");
define($prefijo."-HELPDESK", "Helpdesk");
define($prefijo."-FIELDSERVICE", "Fieldservice");
define($prefijo."-SUPPORT", "Soporte");
define($prefijo."-EDM", "EDM");
define($prefijo."-WORKFLOW", "Workflow");
define($prefijo."-SURVEYS", "Encuestas");
define($prefijo."-ELEARNING", "Administrar centro");
define($prefijo."-MNU_CHANNELS", "Datos personales");
define($prefijo."-MNU_REPORTS", "Informes");

define($prefijo."ELEARNING", "e-Learning");
?>
<?php
// Etiquetas ES para la Información

$prefijo="LBL_";

define($prefijo."INFO_TYPE", "Tipo de información");
define($prefijo."DATE", "Fecha");
define($prefijo."LINK_NAME", "Nombre del enlace");
define($prefijo."LINK", "Enlace");
define($prefijo."CONTENTS", "Contenido");
define($prefijo."INFORMATION", "Contenido de la información");

define($prefijo."INFO_SOURCE", "Fuente");
define($prefijo."PUBLICATION_DATE_INFO", "Rango de fechas en las que la noticia es visible");
define($prefijo."PUBLICATION_DATE", "Fecha de publicación");
define($prefijo."EXPIRATION_DATE", "Fecha de caducidad");
define($prefijo."INFO_EXTRA", "Vídeo incrustrado");


$prefijoMsg="MSG_";
define($prefijoMsg."INFO_TEXT_01", "El título de la información es obligatoria en ambos idiomas.");
define($prefijoMsg."INFO_TEXT_02", "Debe seleccionar una fecha para la información.");
define($prefijoMsg."INFO_TEXT_03", "<strong>El contenido de la información no debe exceder de 6 o 7 líneas para la correcta visualización de la noticia en el portal.</strong>");
define($prefijoMsg."INFO_TEXT_04", "<strong>El video de la información no debe exceder de 300 x 250 pixels para la correcta visualización de la noticia en el portal.</strong>");
define($prefijoMsg."INFO_TEXT_05", "<strong>Recuerde que si incrusta un vídeo, este será reproducido en lugar de la imagen.</strong>");

?>
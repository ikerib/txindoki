<?php
// Etiquetas ES genéricas para los buscadores

$prefijo="LBL_";

define($prefijo."LOC_ACTION", "Realizar búsqueda");
define($prefijo."LOC_SUM_TITLE1", "Resultados de la búsqueda");
define($prefijo."LOC_SUM_TITLE2", "registros disponibles");
define($prefijo."LOC_SUM_TITLE3", "de");
define($prefijo."LOC_TITLE", "Buscador");

?>
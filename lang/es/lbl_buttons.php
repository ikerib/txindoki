<?php
// Etiquetas ES para los botones

$prefijo="LBL_";

define($prefijo."ALT_NEW1", "Crear nuevo");
define($prefijo."ALT_OPEN", "Abrir");
define($prefijo."ALT_SAVE", "Guardar cambios");
define($prefijo."ALT_DELETE", "Borrar");
define($prefijo."ALT_CLOSE", "Cerrar este formulario");

define($prefijo."ALT_IMPORTING_DATA", "Importar datos");


?>
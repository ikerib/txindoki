<?php
	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/database/DB_Connection.php");
	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/portal/AuthenticationWeb.php");
	
	
	global $mysqli;
	global $errorLog;


	$sql = "SELECT ic_course.course_id, 
					COALESCE(ic_course.course_name,'') as course_name, 
					COALESCE(ic_course.course_desc,'') as course_desc, 
					ic_course.level_id, 
					ic_level.level_name, 
					ic_course.course_moodle_id 
			FROM ic_course 
			LEFT JOIN ic_level 
				ON ic_course.level_id = ic_level.level_id 
			AND ic_level.language = 'es' 
			WHERE ic_course.active_flag = 1 ORDER BY ic_course.course_name ASC";

	$rs = $mysqli->query($sql);

	while($row = $rs->fetch_array())		{
		$arr[] = $row;
	}
	header('Content-Type: application/json');
	print_r('{"members":'.json_encode($arr).'}');

?>
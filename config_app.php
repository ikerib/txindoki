<?php

$path_contents_secure_absolute="/home/srmgureak/contents/";
$ruta_debug="/home/gitek/logs/";
$nombre_log="log_portal.txt";
$nivel_debug=1; //DEBUG=1 INFO=2 WARN=3 ERROR=4 FATAL=5 OFF=6

$prefijo="CFG_";

//PICTURE
define($prefijo."PICTURE_URL", $path_contents_secure_absolute."company$1/images/picture/");
define($prefijo."PICTURE_WIDTH", "260");	//px
define($prefijo."PICTURE_HEIGHT", "260");	//px
define($prefijo."TH_PICTURE_WIDTH", "90");	//px
define($prefijo."TH_PICTURE_HEIGHT", "65");//px
define($prefijo."SIZE_BYTES", "102400");
define($prefijo."SIZE_KB", "100");

//COURSE
define($prefijo."PICTURE_COURSE_URL", $path_contents_secure_absolute."company$1/images/course/");
define($prefijo."PICTURE_COURSE_WIDTH", "300");	//px
define($prefijo."PICTURE_COURSE_HEIGHT", "300");	//px
define($prefijo."TH_PICTURE_COURSE_WIDTH", "110");	//px
define($prefijo."TH_PICTURE_COURSE_HEIGHT", "110");//px
define($prefijo."SIZE_COURSE_BYTES", "102400");
define($prefijo."SIZE_COURSE_KB", "100");

//CATEGORY
define($prefijo."PICTURE_CATEGORY_URL", $path_contents_secure_absolute."company$1/images/category/");
define($prefijo."PICTURE_CATEGORY_WIDTH", "300");	//px
define($prefijo."PICTURE_CATEGORY_HEIGHT", "300");	//px
define($prefijo."TH_PICTURE_CATEGORY_WIDTH", "192");	//px
define($prefijo."TH_PICTURE_CATEGORY_HEIGHT", "188");//px
define($prefijo."SIZE_CATEGORY_BYTES", "102400");
define($prefijo."SIZE_CATEGORY_KB", "100");

define($prefijo."PICTURE_CATEGORY_URL_AC", $path_contents_secure_absolute."company$1/images/category/ac/");
define($prefijo."PICTURE_CATEGORY_WIDTH_AC", "200");	//px
define($prefijo."PICTURE_CATEGORY_HEIGHT_AC", "400");	//px
define($prefijo."TH_PICTURE_CATEGORY_WIDTH_AC", "192");	//px
define($prefijo."TH_PICTURE_CATEGORY_HEIGHT_AC", "378");//px
define($prefijo."SIZE_CATEGORY_BYTES_AC", "102400");
define($prefijo."SIZE_CATEGORY_KB_AC", "100");

//ATTACHMENT
define($prefijo."ATTACHMENT_URL", $path_contents_secure_absolute."company$1/files/attachment/");
define($prefijo."ATTACHMENT_SIZE_BYTES", "3072000");
define($prefijo."ATTACHMENT_SIZE_KB", "3000");
define($prefijo."MAX_UPLOAD_FILE_SIZE", "5242880");
define($prefijo."MAX_UPLOAD_FILE_SIZE_MB", "5");

//STUDENT
define($prefijo."PICTURE_STUDENT_URL", $path_contents_secure_absolute."company$1/images/student/");
define($prefijo."PICTURE_STUDENT_WIDTH", "300");	//px
define($prefijo."PICTURE_STUDENT_HEIGHT", "300");	//px
define($prefijo."TH_PICTURE_STUDENT_WIDTH", "100");	//px
define($prefijo."TH_PICTURE_STUDENT_HEIGHT", "100");//px
define($prefijo."SIZE_STUDENT_BYTES", "102400");
define($prefijo."SIZE_STUDENT_KB", "100");

//MISCELLANEOUS
define($prefijo."MAX_NUM_ROWS", "3");	//Número máximo de registros en un locator.
define($prefijo."EMAIL_FROM_DEFAULT", "no_reply@guprest.net");	//Email usado para enviar correo desde el from
define($prefijo."EMAIL_SMTP", "correo.sarenet.es");	//Smtp
define($prefijo."EMAIL_SMTP_USER", "guprest@ext.grupogureak.com");	//Smtp usuario
define($prefijo."EMAIL_SMTP_PASS", "gi10.pr_st"); //Smtp contraseña

define($prefijo."MOODLE_URL", "http://moodle.gureak-akademi.local/login/index.php");	//Ruta de acceso al Moodle.
define($prefijo."SRM_URL", "http://backend.gureak-akademi.local/check_login.php");	//Ruta de acceso al Srm.

define($prefijo."RECOVERY_URL", "http://www.gureak-akademi.local/recuperar_contrasena.php");	//Ruta para recuperar la contraseña.
define($prefijo."MOODLE_URL_GET_IMAGE", "http://moodle.gureak-akademi.local/pluginfile.php");	//Ruta de acceso al Moodle.

//CONFIGURACIÓN DEL KNOWLEDGTREE
define($prefijo."CAFETERIA_ID", "18");	//ID carpeta "Cafeteria"  en KT
define($prefijo."JUEGOS_ID", "23");	//ID carpeta "Juegos"  en KT
define($prefijo."BIBLIOTECA_ID", "27");	//ID carpeta "Biblioteca"  en KT

?>
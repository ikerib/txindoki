<?php

if ($_SERVER['REQUEST_METHOD']=="GET") {
	header('Content-Type: application/json');
	$arr = array('ErrorGA' => "Has enviado get, tiene que ser POST");
	echo json_encode($arr);
	return false;
}

$user_name ="";
if(!isset($_POST['user_name']) || ($_POST['user_name']=="")) { 
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_name')); 
	return false;
} else{
	$user_name = $_POST['user_name'];
}

$first_name ="";
if(!isset($_POST['first_name']) || ($_POST['first_name']=="")) { 
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta first_name')); 
	return false;
} else{
	$first_name = $_POST['first_name'];
}

$last_name_1 ="";
if(!isset($_POST['last_name_1']) || ($_POST['last_name_1']=="")) { 
	header('Content-Type: application/json'); 
	echo json_encode(array('ErrorGA'=>'Falta last_name_1')); 
	return false;
} else {
	$last_name_1 =$_POST['last_name_1'];
}

$last_name_2 =""; // PUEDE SER FALSE
if(!isset($_POST['last_name_2'])) { 
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta last_name_2')); 
	return false;
*/
	$last_name_2 ="";
} else {
	$last_name_2 =$_POST['last_name_2'];
}

$username ="";
if(!isset($_POST['user_name']) || ($_POST['user_name']=="")) { 
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_name')); 
	return false;
} else {
	$username = $_POST['user_name'];
}

$email ="";
if(!isset($_POST['user_email']) || ($_POST['user_email']=="")) {  
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_email')); 
	return false;
} else {
	$email = $_POST['user_email'];
}

$lang ="es"; // Por defecto es
if(!isset($_POST['user_lang']) || ($_POST['user_lang']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_lang')); 
	return false;
*/
	$lang="es";
} else {
	$lang = $_POST['user_lang'];
}

$npass ="";
if(isset($_POST['npass'])) {  
	$npass = $_POST['npass'];
	$cpass = $npass;
}

$card_id =""; // NAN
if(!isset($_POST['card_id'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta card_id')); 
	return false;
*/
	$card_id = "";
} else {
	$card_id = $_POST['card_id'];
}

$telf ="";
if(!isset($_POST['phone'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta phone')); 
	return false;
*/
	$telf="";
} else {
	$telf = $_POST['phone'];
}

$telf2 ="";
if(!isset($_POST['mobile'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta mobile')); 
	return false;
*/
	$telf2="";
} else {
	$telf2 = $_POST['mobile'];
}

$city ="";
if(!isset($_POST['city']) || ($_POST['city']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta city')); 
	return false;
*/
	$city ="";
} else {
	$city = $_POST['city'];
}

/***

if (isset($action) && is_numeric($id) ) {
			
			$resultado = $obj->init();
			
			if ($resultado==""){ //NO hay error.
				$error = $obj->update($id);
				
				if ($error==""){
					//MENSAJE...
					$_SESSION["aviso_tipo"] = "success";
					$_SESSION["aviso_mensaje"] = MSG_INFO_CHANGED_CORRECTLY;
				}else{
					$_SESSION["aviso_tipo"] = "warning";  
					$_SESSION["aviso_mensaje"] = $error;
				}
			}else{//hay error redirecciono y muestro error.
				
				//echo $resultado;
				$_SESSION["aviso_tipo"] = "warning";  
				$_SESSION["aviso_mensaje"] = $resultado; 
			}
				
			session_write_close();
			Header("Location: detail.php?id=$

***/



require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Tutor.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 

	if (isset($_POST["id"]) && $_POST["id"]!="") {
		$id=$_POST["id"];
	}
		

	$obj = new Tutor();
	$obj->loadData($id);
	
	
	$datuak['id']=$id;
	$datuak['user_id']=$id;
	$datuak['first_name']=$first_name;
	$datuak['last_name_1'] = $last_name_1;
	$datuak["last_name_2"] = $last_name_2;
	$datuak["registration_date"] = date(CFG_FORMATO_FECHA);
	$datuak["user_valido"] = "NO EXISTE";
	
	$datuak["user_name"] =  $user_name;
	$datuak["user_email"] = $email;
	$datuak["user_lang"] = $lang; // 'eu'
	$datuak["npass"]=$npass;
	$datuak["cpass"]=$npass;
/* 	$datuak["level"] = '1'; */
/* 	$datuak["course_particular_id"]='0'; */
	$datuak["card_id"]=$card_id; // NAN
	$datuak["phone"]=$telf;
	$datuak["mobile"]=$telf2;
	$datuak["city"]=$city;
/* 	$datuak["internal_id"]=$internal_id; */
	$datuak['bupload_image']="no";
	$datuak['picture_file_old']="";
	$datuak["company_id"]="1";
	$datuak["remarks"]="";
	
	
	
	$resultado = $obj->init($datuak);

	if ($resultado==""){ //NO hay error.

		$error = $obj->update($id);
				
		if ($error==""){
			header('Content-Type: application/json');
			$arr = array('resultado' => "1");
			echo json_encode($arr);
			
		}else{
			header('Content-Type: application/json');
			$arr = array('resultado' => "0");
			echo json_encode($arr);	
			return false;
		}
			
	}else{//hay error redirecciono y muestro error.
				
		header('Content-Type: application/json');
			$arr = array('resultado' => "0");
			echo json_encode($arr);	
			return false;
	}
	
	

?>
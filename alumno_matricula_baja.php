<?php

	if ($_SERVER['REQUEST_METHOD']=="GET") {
		header('Content-Type: application/json');
		$arr = array('ErrorGA' => "Has enviado get, tiene que ser POST");
		echo json_encode($arr);
		return false;
	}

	$usu ="";
	if(!isset($_POST['user_id']) || ($_POST['user_id']=="")) { 
		header('Content-Type: application/json');
		echo json_encode(array('ErrorGA'=>'Falta user_id')); 
		return false;
	} else{
		$usu = $_POST['user_id'];
	}

	$curso_id ="";
	if(!isset($_POST['curso_id']) || ($_POST['curso_id']=="")) { 
		header('Content-Type: application/json');
		echo json_encode(array('ErrorGA'=>'Falta curso_id')); 
		return false;
	} else{
		$curso_id = $_POST['curso_id'];
	}


	include($_SERVER['DOCUMENT_ROOT']."/isyc/classes/database/DB_Connection.php");
	
	global $mysqli;
	global $errorLog;

	
	$curso_id = $_POST['curso_id'];
	$usuid = "";


	// 1-. Buscamos el usuario para obtener su ID y lo guardamos en $usuid
	$sql = "SELECT * FROM ic_student WHERE user_id = '" . $usu . "'";
	
	$result = $mysqli->query($sql);
	

	if ( $result->num_rows == 0 ) {
		header('Content-Type: application/json');
		$arr = array('ErrorGA' => "El user_id NO EXISTE");
		echo json_encode($arr);	
		return false;
	} else {
		while ($row = $result->fetch_row()) {
        	$usuid = $row[0];
        }
        $result->close();
	}
	
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Student.php'); 
	require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 

	$_SESSION["user_id"] = $usuid;
	$obj = new Student();
	$obj->loadData($usuid);
	
	if ($obj->comprobarAltaCurso($curso_id,$usuid) == true ) {
		$obj->bajaCurso($curso_id,$usuid); 
	} else {
		header('Content-Type: application/json');
		$arr = array('resultado' => "0");
		echo json_encode($arr);	
		return false;	
	}
	
	header('Content-Type: application/json');
	$arr = array('resultado' => "1");
	echo json_encode($arr);	
	return false;
	
  

?>
<?php

if ($_SERVER['REQUEST_METHOD']=="GET") {
	header('Content-Type: application/json');
	$arr = array('ErrorGA' => "Has enviado get, tiene que ser POST");
	echo json_encode($arr);
	return false;
}

$izena ="";
if(!isset($_POST['name']) || ($_POST['name']=="")) { 
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta nombre')); 
	return false;
} else{
	$izena = $_POST['name'];
}

$last_name_1 ="";
if(!isset($_POST['last_name_1']) || ($_POST['last_name_1']=="")) { 
	header('Content-Type: application/json'); 
	echo json_encode(array('ErrorGA'=>'Falta last_name_1')); 
	return false;
} else {
	$last_name_1 =$_POST['last_name_1'];
}

$last_name_2 =""; // PUEDE SER FALSE
if(!isset($_POST['last_name_2'])) { 
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta last_name_2')); 
	return false;
*/
	$last_name_2 ="";
} else {
	$last_name_2 =$_POST['last_name_2'];
}

$username ="";
if(!isset($_POST['user_name']) || ($_POST['user_name']=="")) { 
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_name')); 
	return false;
} else {
	$username = $_POST['user_name'];
}

$email ="";
if(!isset($_POST['user_email']) || ($_POST['user_email']=="")) {  
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_email')); 
	return false;
} else {
	$email = $_POST['user_email'];
}

$lang ="es"; // Por defecto es
if(!isset($_POST['user_lang']) || ($_POST['user_lang']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta user_lang')); 
	return false;
*/
	$lang="es";
} else {
	$lang = $_POST['user_lang'];
}

$npass ="";
if(isset($_POST['npass'])) {  
	$npass = $_POST['npass'];
	$cpass = $npass;
}

$level ="";
if(!isset($_POST['level']) || ($_POST['level']=="")) {  
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta level')); 
	return false;
} else {
	$level = $_POST['level'];
}

$pictograma1 ="";
if(!isset($_POST['pictograma1'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta pictograma1')); 
	return false;
*/
	$pictograma1="";
} else {
	$pictograma1 = $_POST['pictograma1'];
}

$pictograma2 ="";
if(!isset($_POST['pictograma2'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta pictograma2')); 
	return false;
*/
	$pictograma2 ="";
} else {
	$pictograma2 = $_POST['pictograma2'];
}

$pictograma3 ="";
if(!isset($_POST['pictograma3'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta pictograma3')); 
	return false;
*/
	$pictograma3 = "";
} else {
	$pictograma3 = $_POST['pictograma3'];
}

$locution ="1";
if(!isset($_POST['locution'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta locution')); 
	return false;
*/
	$locution = "1";
} else {
	$locution = $_POST['locution'];
}

$highcontrast ="2"; // 2 => NO
if(!isset($_POST['high_contrast'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta high_contrast')); 
	return false;
*/
	$highcontrast="2";
} else {
	$highcontrast = $_POST['high_contrast'];
}

$card_id =""; // NAN
if(!isset($_POST['card_id'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta card_id')); 
	return false;
*/
	$card_id = "";
} else {
	$card_id = $_POST['card_id'];
}

$telf ="";
if(!isset($_POST['phone'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta phone')); 
	return false;
*/
	$telf="";
} else {
	$telf = $_POST['phone'];
}

$telf2 ="";
if(!isset($_POST['mobile'])) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta mobile')); 
	return false;
*/
	$telf2="";
} else {
	$telf2 = $_POST['mobile'];
}

$city ="";
if(!isset($_POST['city']) || ($_POST['city']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta city')); 
	return false;
*/
	$city ="";
} else {
	$city = $_POST['city'];
}

$internal_id ="";
if(!isset($_POST['internal_id']) || ($_POST['internal_id']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta internal_id')); 
	return false;
*/
	$internal_id ="";
} else {
	$internal_id = $_POST['internal_id'];
}

$empresa ="";
if(!isset($_POST['empresa']) || ($_POST['empresa']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta empresa')); 
	return false;
*/
	$empresa ="";
} else {
	$$empresa = $_POST['empresa'];
}

$seccion ="";
if(!isset($_POST['seccion']) || ($_POST['seccion']=="")) {  
/*
	header('Content-Type: application/json');
	echo json_encode(array('ErrorGA'=>'Falta seccion')); 
	return false;
*/
	$seccion ="";
} else {
	$seccion = $_POST['seccion'];
}


/***

if (isset($action) && is_numeric($id) ) {
			
			$resultado = $obj->init();
			
			if ($resultado==""){ //NO hay .
				$error = $obj->update($id);
				
				if ($error==""){
					//MENSAJE...
					$_SESSION["aviso_tipo"] = "success";
					$_SESSION["aviso_mensaje"] = MSG_INFO_CHANGED_CORRECTLY;
				}else{
					$_SESSION["aviso_tipo"] = "warning";  
					$_SESSION["aviso_mensaje"] = $error;
				}
			}else{//hay error redirecciono y muestro error.
				
				//echo $resultado;
				$_SESSION["aviso_tipo"] = "warning";  
				$_SESSION["aviso_mensaje"] = $resultado; 
			}
				
			session_write_close();
			Header("Location: detail.php?id=$

***/



require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/modules/elearning/Student.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/srm/Functions.php'); 

	if (isset($_POST["id"]) && $_POST["id"]!="") {
		$id=$_POST["id"];
	}
		

	$obj = new Student();
	$obj->loadData($id);
	
	
	$datuak['id']=$id;
	$datuak['user_id']=$id;
	$datuak['name']=$izena;
	$datuak['last_name_1'] = $last_name_1;
	$datuak["last_name_2"] = $last_name_2;
	$datuak["registration_date"] = date(CFG_FORMATO_FECHA);
	$datuak["user_valido"] = "NO EXISTE";
	
	$datuak["user_name"] =  $username;
	$datuak["user_email"] = $email;
	$datuak["user_lang"] = $lang; // 'eu'
	$datuak["npass"]=$npass;
	$datuak["cpass"]=$npass;
	$datuak["level"] = $level;
	$datuak["course_particular_id"]='0';
	$datuak['pictograma1'] = $pictograma1;
	$datuak['pictograma2'] = $pictograma2;
	$datuak['pictograma3'] = $pictograma3;
	$datuak["locution"] = $locution; // Si
	$datuak["high_contrast"] = $highcontrast; // 2=No
	$datuak["card_id"]=$card_id; // NAN
	$datuak["phone"]=$telf;
	$datuak["mobile"]=$telf2;
	$datuak["city"]=$city;
	$datuak["internal_id"]=$internal_id;
	$datuak["empresa"]="";
	$datuak["seccion"]="";
	$datuak['bupload_image']="no";
	$datuak['picture_file_old']="";
	$datuak["company_id"]="1";
	$datuak["remarks"]="";

	$resultado = $obj->init($datuak);

	if ($resultado==""){ //NO hay error.

		$error = $obj->update($id);
				
		if ($error==""){
			header('Content-Type: application/json');
			$arr = array('resultado' => "1");
			echo json_encode($arr);
			
		}else{
			header('Content-Type: application/json');
			$arr = array('resultado' => "0");
			echo json_encode($arr);	
			return false;
		}
			
	}else{//hay error redirecciono y muestro error.
				
		header('Content-Type: application/json');
			$arr = array('resultado' => "0");
			echo json_encode($arr);	
			return false;
	}
	

?>